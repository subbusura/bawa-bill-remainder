<?php

use yii\db\Migration;

/**
 * Class m180701_092409_add_orders_arrivel_tbl
 */
class m180701_092409_add_orders_arrivel_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{%orders_arrivel}}', [
    			'id' => $this->bigPrimaryKey(),
    			'order_id'=>$this->bigInteger(),
    			'line_items'=>$this->text()
    	
    	], $tableOptions);
    	

    	$this->createTable('{{%sms_setting}}', [
    			'id' => $this->bigPrimaryKey(),
    			'sms_type'=>$this->string(),
    			'content'=>$this->text()
    	], $tableOptions);
    	
    	
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropTable('orders_arrivel');
    	$this->dropTable('sms_setting');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180701_092409_add_orders_arrivel_tbl cannot be reverted.\n";

        return false;
    }
    */
}
