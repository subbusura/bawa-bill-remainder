<?php

use yii\db\Migration;

/**
 * Class m180802_014214_sales_rep_tbl_added
 */
class m180802_014214_sales_rep_tbl_added extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	
    	$this->createTable('{{%salesman}}', [
    			'id' => $this->bigPrimaryKey(),
    			'name'=>$this->string()->notNull()->unique(),
    	], $tableOptions);
    	
    	
    	$this->addColumn('orders', 'salesman_id', $this->integer()->null()->after('user_id'));
    	$this->addColumn('orders', 'remarks', $this->text()->null()->after('order_status'));
    	$this->addColumn('orders', 'image_one', $this->text()->null()->after('remarks'));
    	$this->addColumn('orders', 'image_two', $this->text()->null()->after('image_one'));
    	
    	
    		
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
          $this->dropTable('salesman');
          
          $this->dropColumn('orders', 'salesman_id');
          $this->dropColumn('orders', 'remarks');
          $this->dropColumn('orders', 'image_one');
          $this->dropColumn('orders', 'image_two');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180802_014214_sales_rep_tbl_added cannot be reverted.\n";

        return false;
    }
    */
}
