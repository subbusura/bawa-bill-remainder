<?php

use yii\db\Migration;

/**
 * Class m180708_105927_add_tbl_sms
 */
class m180708_105927_add_tbl_sms extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('{{%sms_account}}', [
    			'id' => $this->bigPrimaryKey(),
    			'name'=>$this->string()->unique(),
    			'url'=>$this->text(),
    			'sender_id'=>$this->bigInteger(),
    			'username'=>$this->text(),
    			'password'=>$this->string(),
    			'sms_type'=>$this->string(),
    			'is_default'=>$this->integer(),
    			'created_at'=>$this->integer(),
    			'updated_at'=>$this->integer()
    			 
    	], $tableOptions);
    	
    	$this->createTable('{{%sms_transaction}}', [
    			'id' => $this->bigPrimaryKey(),
    			'account_id'=>$this->bigInteger(),
    			'sender_id'=>$this->string(),
    			'sender_type'=>$this->string(),
    			'mobile_number'=>$this->string(),
    			'sms_content'=>$this->text(),
    			'reference_id'=>$this->string(),
    			'created_at'=>$this->integer(),
    			'updated_at'=>$this->integer()
    	
    	], $tableOptions);
    	
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
    	$this->dropTable('sms_account');
    	$this->dropTable('sms_transaction');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180708_105927_add_tbl_sms cannot be reverted.\n";

        return false;
    }
    */
}
