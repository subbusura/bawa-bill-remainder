<?php

use yii\db\Migration;

/**
 * Class m180629_155435_init
 */
class m180629_155435_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    	$tableOptions = null;
    	if ($this->db->driverName === 'mysql') {
    		// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    
    	$this->createTable('{{%admin_users}}', [
    			'id' => $this->primaryKey(),
    			'username'=>$this->string(100)->notNull(),
    			'auth_key'=>$this->string(255)->null(),
    			'password'=>$this->string(255)->notNull(),
    			'firstname'=>$this->string()->null(),
    			'lastname'=>$this->string()->null(),
    			'email'=>$this->string(100)->null(),
    			'mobile_number'=>$this->string(15)->null(),
    			'status'=>$this->integer()->defaultValue(0)->null(),
    			'created_at'=>$this->integer()->null(),
    			'updated_at'=>$this->integer()->null()
    			 
    	], $tableOptions);
    	
    	$this->createTable('{{%products}}', [
    			'id' => $this->bigPrimaryKey(),
    			'product_code'=>$this->string()->notNull()->unique(),
    			'product_name'=>$this->string()->notNull(),
    			'DivisionCode'=>$this->string()->null(),
    	], $tableOptions);
    	
    	$this->createTable('{{%suppliers}}', [
    			'id' => $this->bigPrimaryKey(),
    			'supplier_code'=>$this->string()->notNull()->unique(),
    			'supplier_name'=>$this->string()->notNull(), 
    	], $tableOptions);
    	
    	$this->createTable('{{%orders}}', [
    			'id' => $this->bigPrimaryKey(),
    			'name'=>$this->string()->null(),
    			'mobile_number'=>$this->string(15)->notNull(),
    			'advance_payment'=>$this->decimal(12,2),
    			'image_path'=>$this->text(),
    			'user_id'=>$this->integer(),
    			'order_status'=>$this->integer(),
    			'created_at'=>$this->integer(),
    			'updated_at'=>$this->integer()
    	], $tableOptions);
    	
    	$this->createTable('{{%orders_items}}', [
    			'id' => $this->bigPrimaryKey(),
    			'order_id'=>$this->bigInteger()->notNull(),
    			'product_code'=>$this->string(100)->notNull(),
    			'product_name'=>$this->string(100)->null(),
    			'supplier_code'=>$this->string(100)->null(),
    			'supplier_name'=>$this->string(100)->null(),
    			'qty'=>$this->integer(),
    			'arrival_status'=>$this->integer(),
    			'arrival_datetime'=>$this->integer()
    	], $tableOptions);
    	
    	
    	
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('admin_users');
    	$this->dropTable('products');
    	$this->dropTable('suppliers');
    	$this->dropTable('orders');
    	$this->dropTable('orders_items');
    	
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180629_155435_init cannot be reverted.\n";

        return false;
    }
    */
}
