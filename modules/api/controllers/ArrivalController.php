<?php

namespace app\modules\api\controllers;



use yii;
use yii\rest\Controller;
use app\models\Order;
use app\models\OrdersItem;
use app\models\OrderSearch;
use app\models\OrdersItemSearch;
use yii\helpers\ArrayHelper;
use app\models\OrderArrive;

class ArrivalController extends Controller
{
	
	
	public function actionIndex(){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		$search= new OrdersItemSearch();
		 
		return $search->searchArrival($rdata);
	
	}
	
	public function actionSend(){
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
	   if(!array_key_exists("product_code", $rdata)){
		 	
		 	  Yii::$app->getResponse()->setStatusCode(422);
		 	   return [];
		 	 
		 }
		
		//list of product 
		 $data=OrdersItem::find()->where(["product_code"=>$rdata["product_code"],"arrival_status"=>20])->asArray()->all();
		 $dataGroup=ArrayHelper::index($data, null,"order_id");
		 
		// return $dataGroup;
		 
		 foreach ($dataGroup as $key=>$ordersItems)
		 {
		 	 $order=Order::find()->where(["id"=>$key])->asArray()->one();
		 	 
		 	 $lineIds=[];
		 	 $product_names=[];
		 	 $itemCount=count($ordersItems);
		 	 
		 	 for ($i = 0; $i < $itemCount; $i++) {
		 	   	$lineIds[]=$ordersItems[$i]["id"];
		 	   	$product_names[]=$ordersItems[$i]["product_name"]; 	
		 	  }
		 	  
		 	 // $this->sendSms($order["mobile_number"],$product_names,$key);
		 	  $arrive = new OrderArrive();
		 	  $arrive->order_id=$key;
		 	  $arrive->line_items=implode("|", $lineIds);
		 	  if($arrive->save())
		 	  {

		 	  	Yii::trace("Order Arrive Success");	  
		 	  	Yii::trace($lineIds);
		 	  	
		 	  	$lorder=Order::find()->where(["id"=>$key])->one();
		 	  	if(!is_null($lorder))
		 	  	{
		 	  		$lorder->order_status=15;
		 	  		$lorder->save(false);
		 	  	}
		 	  	
		 	  	OrdersItem::updateAll(["arrival_status"=>OrdersItem::STATUS_COMPLETED,"arrival_datetime"=>time()],["id"=>$lineIds]);
		 	  }else{
		 	  	 
		 	  	 Yii::trace("Order Arrive Item Invalid");
		 	  	 Yii::trace($arrive->getErrors());
		 	  }
		 	  
		 	 
		 }
		 
		 return $dataGroup;
	
		
	}
	
	public function sendSms($mobielNumber,$productNames,$orderId){
		
		     Yii::trace($mobielNumber." ".$orderId);
		     Yii::trace($productNames);
			$content="thanks for your order {$orderId}".implode(",", $productNames);
			 
			Yii::trace("SMS content");
			Yii::trace($content);
			$number=$mobielNumber;
			if($number !="")
			{
			  $response=Yii::$app->sms->sendSms($number,$content,[
       	    	 			"senderID"=>"STDEMO",
       	    	 			"username"=>"smstdemo",
       	    	 			"password"=>"12345",
       	    	 			"priority"=>"ndnd",
       	    	 			"messageType"=>"ndnd"
       	    	 	
       	    	 	]);
			  
			 
			  Yii::trace("Sms Api Response");
			  Yii::trace($response);
			}
		
	}
	

}
