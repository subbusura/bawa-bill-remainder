<?php

namespace app\modules\api\controllers;

use app\models\SmsSetting;

class SmsController extends \yii\rest\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionType(){
    	
    	
    	return [
    			[
    					'name'=>"New Order",
    					'value'=>"NEW_ORDER"
    			],
    			[
		    			'name'=>"After Arrived",
		    			'value'=>"AFTER_ARRIVED"
    			],
    			[
		    			'name'=>"After Delivered",
		    			'value'=>"AFTER_DELIVERED"
    			],

    	];
    	
    	
    }
    

    public function actionView($type){
    
    
    	return SmsSetting::find()->where(["sms_type"=>$type])->one();
    	
    }
    
    public function actionUpdate($type,$temp_type){
    
    
    	$model=SmsSetting::find()->where(["sms_type"=>$type])->one();
    	$rdata=Yii::$app->getRequest()->getBodyParams();
    
    	if(is_null($model))
    	{
    		$model = new SmsSetting();
    		$model->load($rdata,"");
    			
    		if($model->save())
    		{
    			return $model;
    
    		}else{
    
    			return $model->getErrors();
    		}
    			
    	}else{
    			
    		$model->load($rdata,"");
    			
    		if($model->save())
    		{
    			return $model;
    
    		}else{
    
    			return $model->getErrors();
    		}
    			
    	}
    }

}
