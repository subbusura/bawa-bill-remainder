<?php

namespace app\modules\api\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\User;
use yii\web\Response;
use app\models\Password;
use yii\filters\AccessControl;
use app\models\AdminUser;

class UserController extends Controller{
	
	public $enableCsrfValidation = false;
	
	
	public function behaviors()
	{
		
		
		return [
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['POST'],
						],
				],
		];
	}
	
	 
	 //View List
	 
	public function actionIndex(){
		
		return $this->render("index");
		
	} 
	
	public function actionView($id){
		
		return $this->render("view");
		
	}
	
	
	// Api List
	public function actionUser($id){
		
		\Yii::$app->response->format = Response::FORMAT_JSON;
		
		 $user = AdminUser::find()->where(["id"=>$id])->one();
		
		 return $user;
		
	}
	
	
	public function actionDelete($id){
		
		\Yii::$app->response->format = Response::FORMAT_JSON;
		
		if(Yii::$app->user->id==$id)
		{
			Yii::$app->getResponse()->statusCode=422;
			
			return ["status"=>false];
			
		}
		
		$user = AdminUser::find()->where(["id"=>$id])->one();
		if(is_null($user))
		{
			return [];	
		}
		
		 Yii::$app->authManager->revokeAll($id);	
		 
		 $user->delete();
		 
		 return ["status"=>true];
		 
	}
	
	public function actionCreate(){
		
					
		\Yii::$app->response->format = Response::FORMAT_JSON;
		
		$model = new AdminUser();
		$requestData=Yii::$app->getRequest()->getBodyParams();
		$model->load($requestData,"");
		if(!is_null($model->password) && $model->password!=""){
			$model->password=Yii::$app->getSecurity()->generatePasswordHash($model->password);
		}
		$model->auth_key=Yii::$app->getSecurity()->generateRandomString();
		$model->created_at=time();
		$model->updated_at=time();
		if($model->save())
		{
			 $auth=Yii::$app->authManager;
			$role=$auth->getRole($model->role);
			 $auth->assign($role,$model->id);
			
			return $model;
		}
		else{
			Yii::$app->response->statusCode=422;
			return $model->getErrors();
		}
	}
	
	//Update UserProfile
	public function actionUpdate($id){
		\Yii::$app->response->format = Response::FORMAT_JSON;
		$model= AdminUser::find()->where(['id'=>$id])->one();
		if(is_null($model))
		{
			return ["message"=>"invalid id"];
		}
		$requestData=Yii::$app->getRequest()->getBodyParams();
		$model->load($requestData,"");
		//$model->setScenario("update");
		  
		 if(array_key_exists("password",$requestData) && $requestData["password"] !="")
		 {
		 	$model->password=Yii::$app->getSecurity()->generatePasswordHash($requestData["password"]);
		 	$model->auth_key=Yii::$app->getSecurity()->generateRandomString();
		 		
		 }
		$model->updated_at=time();
		if($model->save())
		{
				
			$auth=Yii::$app->authManager;
			
			Yii::$app->authManager->revokeAll($model->id);
			
			$role=$auth->getRole($model->role);
			$auth->assign($role,$model->id);
			
			return $model;
		}
		else{
			Yii::$app->response->statusCode=422;
			return $model->getErrors();
		}
	}
	//Update Permission 
	
	public function actionUpdatePermission(){
		
		
		
	}
	
	
	public function actionList(){
		
		\Yii::$app->response->format = Response::FORMAT_JSON;

		$users= AdminUser::find()->all();
		
		return $users;
		
	}
	
	
	public function actionChangePassword(){
		\Yii::$app->response->format = Response::FORMAT_JSON;
		$model = new Password();
		$requestData=Yii::$app->getRequest()->getBodyParams();
		$id=Yii::$app->user->id;
		
		if(Yii::$app->request->isPost && $model->load($requestData,"") && $model->validate())
		{
			$user= AdminUser::find()->where(['id'=>$id])->one();
			if($user->validatePassword($model->current_password))
			{
				$user->password=Yii::$app->security->generatePasswordHash($model->new_password);
				$user->save(false);
				return ["Success"];
			}else{
				return ["current_password,Invalid Password"];
			}
		}else{
			return $model->getErrors();
		}
	}

public function actionGetRole(){
	\Yii::$app->response->format = Response::FORMAT_JSON;
	
	$manager=Yii::$app->authManager;
	
	$roles=$manager->getRoles();
	
	$result=[];
	foreach ($roles as $key =>$values)
	{
		array_push($result,$key);
		 
	}
	
	return $result;
	
}


public function actionRoleList(){
	
	\Yii::$app->response->format = Response::FORMAT_JSON;
	
	$roles=Yii::$app->authManager->getRoles();
	$result=[];
	
	$hiddenRole=["super user"];
	
	
	foreach ($roles as $key=>$value)
	{
		
		if(!in_array($key,$hiddenRole,true))
		{
			array_push($result, $key);
			
		}
		
		
		
	}
	
	
	return $result;
	
}

}