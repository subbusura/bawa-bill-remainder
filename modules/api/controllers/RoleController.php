<?php

namespace app\modules\api\controllers;

use yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\RoleForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class RoleController extends Controller{
	
	public $enableCsrfValidation = false;
	
	public function behaviors()
	{
		
		
		
		
		return [
				
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['POST'],
						],
				],
		];
	}
	
	//Api List
	
	//for internal use
	
	public function actionRoles(){
		
		
		\Yii::$app->response->format = Response::FORMAT_JSON;
		
		$roles=Yii::$app->authManager->getRoles();
		$result=[];
		
		$hiddenRole=["admin","super user"];
		
		
		foreach ($roles as $key=>$value)
		{
			
			if(!in_array($key,$hiddenRole,true))
			{
				array_push($result, $key);
				
			}
			
			
			
		}
		
		
		
		return $result;
		
	}
	
	//for user select 
	
	public function actionRoleList(){
		
		\Yii::$app->response->format = Response::FORMAT_JSON;
		
		$roles=Yii::$app->authManager->getRoles();
		$result=[];
		
		$hiddenRole=["super user"];
		
		
		foreach ($roles as $key=>$value)
		{
			
			if(!in_array($key,$hiddenRole,true))
			{
				array_push($result, $key);
				
			}
			
			
			 
		}
		
		
		 		
		return $result;

	}
	
	

	//add Role
	
	public function actionAddRole(){
		
		\Yii::$app->response->format = Response::FORMAT_JSON;
		
		$form = new RoleForm();
		
		$request= Yii::$app->getRequest()->getBodyParams();
		$form->load($request,"");
		if(!$form->validate())
		{
			
			Yii::$app->getResponse()->statusCode=422;
			
			 return $form->getErrors();
			
		}
		
		$permission=$request["permission"];
		
	    $auth=Yii::$app->authManager;
		 
		
		 $role=$auth->getRole($request["role"]);
		  
		  if(!is_null($role))
		  {
        	 return ["invalid Role"];  	
		  }
		  
		 $role=$auth->createRole($request["role"]);
		 
		 if($auth->add($role))
		 {
		 	 
		 		for ($i = 0; $i < count($permission); $i++) {
		 				
		 				$mpermission=$auth->getPermission($permission[$i]);
		 				
		 				if(!is_null($mpermission))
		 				{
		 					 if($auth->addChild($role, $mpermission))
		 					 {
		 					 	
		 					 }else{
		 					 	
		 					 	echo "Permission Error while add ".$permission[$i];
		 					 	
		 					 }
		 				}
		 				
		 				
		 		}
		 	
		 	
		 }
		  
	
		return ["success"];
		
		
	}
	
	
	public function actionUpdatePermission($role){
		
		\Yii::$app->response->format = Response::FORMAT_JSON;
		$request= Yii::$app->getRequest()->getBodyParams();
		$permission=$request["permissions"];
		$auth=Yii::$app->authManager;
		
		$hiddenRole=["admin","super user"];
		
		
		/* if(in_array($role, $hiddenRole))
		{
			
			return [];
		} */
		
		
		$role = $auth->getRole($role);
	
		  if($auth->removeChildren($role))
		  {

		  	
		  }
		  
		  for ($i = 0; $i < count($permission); $i++) {
		  
		  
		  	$new=$auth->getPermission($permission[$i]);
		  	 
		  	if(!is_null($new))
		  	{
		  		$auth->addChild($role, $new);
		  	}else{
		  		 
		  		
		  	}
		  	 
		  }
		  
		  
		 return [];
	}
	

	public function actionRemove($role){
		
		\Yii::$app->response->format = Response::FORMAT_JSON;
		
		$auth=Yii::$app->authManager;
		
		$hiddenRole=["admin","super user"];
		
		
		if(in_array($role, $hiddenRole))
		{
			
			return [];
		}
		
		
		$role = $auth->getRole($role);
		 
	     $auth->removeChildren($role);
		 
		 return $auth->remove($role);
		
		
	}
	
	public function actionAddPermission(){
		
		\Yii::$app->response->format = Response::FORMAT_JSON;
		
		$request= Yii::$app->getRequest()->getBodyParams();
			
		$permission=$request["name"];
		
		$auth=Yii::$app->authManager;
		
		 $mpermission=$auth->getPermission($permission);

		 
		  if(is_null($mpermission)){
		  	
		  	  $obj=$auth->createPermission($permission);
		  	   if($auth->add($obj))
		  	   {
		  	   		
		  	   		return ["Success"];
		  	   	
		  	   }else{
		  	   	
		  	   	
		  	   	  return [];
		  	   }
		  		
		  	
		  }else{
		  	
		  	   return [];
		  	
		  }
		
		
		return [];
		
		
	}

	
	public function actionGetPermissions($role){
			
		\Yii::$app->response->format = Response::FORMAT_JSON;
			
		$auth = Yii::$app->authManager;
		
		$permission=$auth->getPermissionsByRole($role);
		
		$result=[];
			
		foreach ($permission as $key=>$value)
		{
			array_push($result, $key);
				
		}
		
		return $result;
		
	}
	
	
	//update Role

	public function actionPermissions(){
		
		\Yii::$app->response->format = Response::FORMAT_JSON;
		
		$roles=Yii::$app->authManager->getPermissions();
		$result=[];
		
		foreach ($roles as $key=>$value)
		{
			array_push($result, $key);
		
		}
		 
		return $result;
		
		
	}
	
	
	public function actionGetRole(){
		
		\Yii::$app->response->format = Response::FORMAT_JSON;
		
		$auth=Yii::$app->authManager;
		
		
		$roles=$auth->getPermissionsByRole("Manager");
		 
		 $result=[];
		 
		 foreach ($roles as $key=>$value)
		 {
		 	array_push($result, $key);
		 
		 }
		 	
		 return $result;
			
		
	}
	

	
	
}