<?php

namespace app\modules\api\controllers;



use yii;
use yii\rest\Controller;
use app\models\Order;
use app\models\OrdersItem;
use app\models\OrderSearch;
use app\models\Salesman;

class SalesOrderController extends Controller
{
	public $serializer = [
			'class' => 'yii\rest\Serializer',
			'collectionEnvelope' => 'items',
	];
	
	public function actionIndex(){
		
		 
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		 $search= new OrderSearch();
		 
		 return $search->search($rdata);
	
	}
	
	public function actionLastOrder(){
	
		return Order::find()->orderBy(["id"=>SORT_DESC])->one();
	
	}
	
	public function actionCreate(){
		
	   $orderItemss=[new OrdersItem()];
	   $rdata=Yii::$app->getRequest()->getBodyParams();
	   $order= new Order();
	   $order->loadDefaultValues();
	   $order->load($rdata,"");
	   $order->user_id=Yii::$app->user->id;
	   $order->order_status=Order::STATUS_PENDING;
	   $order->created_at=time();
	   $order->image_path=$order->src;
	   foreach ($rdata["line_items"] as $i=>$data){
	   	
	   		$model=new OrdersItem();
	   		$model->setScenario("create");
	   		$model->load($data,"");
	   		$orderItemss[$i]=$model;
	   	
	   }
	   
	   $validate=OrdersItem::validateMultiple($orderItemss);
	   
	   if($order->save() && $validate)
	   {
	   		
	   	 	foreach ($orderItemss as $item)
	   	 	{
	   	 		 $item->order_id=$order->id;
	   	 		 $item->arrival_status=10;
	   	 		 $item->save(false);
	   	 		
	   	 	}
	   		
	   	 	$content="thanks for your order {$order->id}.We'll notify you when it arrive";
	   	 	
	   	 	$number=$order->mobile_number;
	   	 	 if($number !="")
	   	 	 {
	   	 	 	/* Yii::$app->sms->sendSms($number,$content,[
       	    	 			"senderID"=>"STDEMO",
       	    	 			"username"=>"smstdemo",
       	    	 			"password"=>"12345",
       	    	 			"priority"=>"ndnd",
       	    	 			"messageType"=>"ndnd"
       	    	 	
       	    	 	]); */
	   	 	 }
	   	 	 	
	   	 	return $order;
	   	
	   }else{
	   	
	   	  Yii::$app->getResponse()->setStatusCode(422);
	   	
	   	 $error=$this->getModelErrors($orderItemss);
	   	   if(!empty($error)){
	   	   	
	   	   	 return $error;
	   	   	
	   	   }
	   	   
	   	   return $order->getErrors();
	   	
	   }
	   
	}
	
	
	public function actionView($order_id){
		
		$order=Order::find()->where(["id"=>$order_id])->one();
			
		if(is_null($order))
		{
			return [];
		}
		return $order;
		
	}
	
	public function actionAddItem($order_id){
		
		   $order= Order::find()->where(["id"=>$order_id])->one();
		    
		   if(is_null($order))
		   {
		   	 return [];
		   }
		
		   $rdata=Yii::$app->getRequest()->getBodyParams();
		   
		   $model = new OrdersItem();
		   $model->setScenario("create");
		   $model->order_id=$order_id;
		   $model->load($rdata,"");
		   
		   if($model->save())
		   {
		   	
		   	return $model;
		   	
		   }else{
		   	Yii::$app->getResponse()->setStatusCode(422);
		   	  return $model->getErrors();
		   	 
		   }
		
	
	}
	
	public function actionUpdateItem($order_id,$orderItem){
		
		$orderItem=OrdersItem::find()->where(["id"=>$orderItem,"order_id"=>$order_id])->one();
		
		if(is_null($orderItem))
		{
			return [];
		}
		
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		$orderItem->load($rdata,"");
		
		
		if($orderItem->save())
		{
		
			return $orderItem;
		
		}else{
			
			Yii::$app->getResponse()->setStatusCode(422);
			return $orderItem->getErrors();
			 
		}
		
	}
	
	public function actionRemoveItem($order_id,$orderItem){
		
		$orderItem=OrdersItem::find()->where(["id"=>$orderItem,"order_id"=>$order_id])->one();
				
		if(is_null($orderItem))
		{
			return [];
		}
		
		return $orderItem->delete();
		
	}
	
	public function actionUpdate($order_id){
		
			$order=Order::find()->where(["id"=>$order_id])->one();
			
			if(is_null($order))
			{
				return [];
			}
			$rdata=Yii::$app->getRequest()->getBodyParams();
			$order->load($rdata,"");
			$order->image_path=$order->src;
			$order->updated_at=time();
			
			if($order->save(false))
			{
				return $order;
				
			}else{
				Yii::$app->getResponse()->setStatusCode(422);
				 return $order->getErrors();
			}
			
	}
	
	public function actionDelete($order_id){
		
		 	$model=Order::find()->where(["id"=>$order_id])->one();
		 	
		 	if(is_null($model))
		 	{
		 		Yii::$app->getResponse()->setStatusCode(422);
		 		return [];
		 		
		 	}
		 	
		 	 OrdersItem::deleteAll(["order_id"=>$order_id]);
		 	
		 	return $model->delete();
	}
	
	
	public function actionSalesMan(){
		
			 return Salesman::find()->all();
		
	}
	
	
	public function getModelErrors($models){
		
		  $error=[];
		  
		   foreach ($models as $model){
		   	
		   		 if(!$model->validate())
		   		 {
		   		 	  $error[]=$model->getErrors();	
		   		 }
		   }
		   
		   return $error;
		
		
	}
	

}
