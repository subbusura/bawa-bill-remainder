<?php

namespace app\modules\api\controllers;

use yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\RoleForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class PermissionController extends Controller{
	
	public $enableCsrfValidation = false;
	
	public function behaviors()
	{
		
		
		
		
		return [
				
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['POST'],
						],
				],
		];
	}
	
	//Api List
	
	//for internal use
	
	public function actionCheck($permission){
		
		
		if (\Yii::$app->user->can($permission))
		{
			return 'ok';
			
		}else{
			Yii::$app->getResponse()->setStatusCode(422);
			return 'np';
			
		}
		
	}
	
}