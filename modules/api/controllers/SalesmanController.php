<?php

namespace app\modules\api\controllers;

use Yii;
use app\models\Salesman;
use app\models\SalesmanSearch;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SalesmanController implements the CRUD actions for Salesman model.
 */
class SalesmanController extends Controller
{
	
	public $serializer = [
			'class' => 'yii\rest\Serializer',
			'collectionEnvelope' => 'items',
	];
	
	
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['DELETE'],
                ],
            ],
        ];
    }

 
    public function actionIndex()
    {
        $searchModel = new SalesmanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $dataProvider;
    }

    public function actionView($id)
    {
        return $this->findModel($id);
    }

    public function actionCreate()
    {
        $model = new Salesman();

        $rdata=Yii::$app->getRequest()->getBodyParams();
        
        if ($model->load($rdata,"") && $model->save()) {
        	Yii::$app->getResponse()->setStatusCode(201);
            return $model;
        }
        Yii::$app->getResponse()->setStatusCode(422);
        return $model->getErrors();
    }

    /**
     * Updates an existing Salesman model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $rdata=Yii::$app->getRequest()->getBodyParams();
        if ($model->load($rdata,"") && $model->save()) {
        	Yii::$app->getResponse()->setStatusCode(201);
            return $model;
        }

        Yii::$app->getResponse()->setStatusCode(422);
        return $model;
    }

    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        Yii::$app->getResponse()->setStatusCode(422);

        return ["ok"];
    }

  
    protected function findModel($id)
    {
        if (($model = Salesman::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
