<?php

namespace app\modules\api\controllers;

use yii;
use yii\rest\ActiveController;
use app\models\Product;
use yii\web\NotFoundHttpException;
use app\models\ProductSearch;

class ProductController extends ActiveController
{
	public $modelClass="app\models\Product";
	public $serializer = [
			'class' => 'yii\rest\Serializer',
			'collectionEnvelope' => 'items',
	];

	public function actions(){
		
		  $actions=parent::actions();
		  
		  unset($actions['index'],$actions['create'],$actions['update']);
		  
		  return $actions;
	}
	
	
	public function actionIndex(){
	    
		$rdata=Yii::$app->getRequest()->getBodyParams();
		
		
		$search= new ProductSearch();
		
		return $search->search($rdata);
		
		
		 
	}
	
	public function actionCreate(){		
		
		  $model = new Product();
		  $rdata=Yii::$app->getRequest()->getBodyParams();
		  $model->load($rdata,"");
		  
		  if($model->save())
		  {
		  	Yii::$app->getResponse()->setStatusCode(201);
		  	 return $model;
		  	
		  }else{
		  	
		  	 Yii::$app->getResponse()->setStatusCode(422);
		  	 return $model->getErrors();
		  	 
		  }
		  
		
	}
	
	public function actionUpdate($id){
		
		 	$model=Product::find()->where(["id"=>$id])->one();
		 	
		 	 if(is_null($model))
		 	 {
		 	 	throw new NotFoundHttpException("Object not found: $id");
		 	 	
		 	 }
		 	
		 	$rdata=Yii::$app->getRequest()->getBodyParams();
		 	unset($rdata["id"]);
		 	$model->load($rdata,"");
		 	
		 	if($model->save())
		 	{
		 		Yii::$app->getResponse()->setStatusCode(201);
		 		return $model;
		 		 
		 	}else{
		 		 
		 		Yii::$app->getResponse()->setStatusCode(422);
		 		return $model->getErrors();
		 	
		 	}
		 	
	}
	
	
	
}
