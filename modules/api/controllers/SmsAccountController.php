<?php

namespace app\modules\api\controllers;

use yii;

use app\models\SmsAccount;
use yii\web\NotFoundHttpException;

class SmsAccountController extends \yii\rest\Controller
{
    public function actionIndex()
    {
        return SmsAccount::find()->all();
    }
    
    
    public function actionCreate(){
    	
    	 $model= new SmsAccount();
    	 $rdata=Yii::$app->getRequest()->getBodyParams();
    	 $model->loadDefaultValues();
    	 $model->load($rdata,"");
    	 
    	 if($model->save())
    	 {
    	 	if($model->is_default==1)
    	 	{
    	 	
    	 		 SmsAccount::updateAll(["is_default"=>0],'id !='.$model->id);
    	 	   		
    	 	}
    	 	
    	 	return $model;
    	 	
    	 	
    	 }else{
    	 	
    	 	 return $model->getErrors();
    	 	
    	 }
    	 
    }
    
    
    public function actionUpdate($id){
    	
    	 $model = SmsAccount::find()->where(["id"=>$id])->one();
    	 
    	 if(is_null($model))
    	 {
    	 	return NotFoundHttpException();
    	 		
    	 }
    	 
    	 $rdata=Yii::$app->getRequest()->getBodyParams();
    	 $model->loadDefaultValues();
    	 $model->load($rdata,"");
    	 
    	 if($model->save())
    	 {
    	 	if($model->is_default==1)
    	 	{
    	 		 
    	 		SmsAccount::updateAll(["is_default"=>0],'id !='.$model->id);
    	 
    	 	}
    	 	 
    	 	return $model;
    	 	 
    	 	 
    	 }else{
    	 	 
    	 	return $model->getErrors();
    	 	 
    	 }
    	
    }
    
    public function actionView($id){
    	 
    	$model = SmsAccount::find()->where(["id"=>$id])->one();
    
    	if(is_null($model))
    	{
    		return NotFoundHttpException();
    
    	}
    
    	return $model;
    	 
    }
    
    public function actionDelete($id){
    
    	$model = SmsAccount::find()->where(["id"=>$id])->one();
    
    	if(is_null($model))
    	{
    		return NotFoundHttpException();
    
    	}
    
    	return $model->delete();
    
    }
    

}
