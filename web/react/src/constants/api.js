export const BASE_URL = process.env.REACT_APP_ENDPOINT;
export const ACCESS_TOKEN = process.env.REACT_APP_ACCESS_TOKEN;
export const ADMIN_BASE_URL=process.env.REACT_APP_BASE_URL;

export const ARRIVEL_LIST='/arrival'

export const PRINTER_LIST = '/printer';
export const PRINTER_DELETE = '/printer/delete';
export const PRINTER_CREATE = '/printer/create';

export const PRODUCT_LIST = '/product';
export const PRODUCT_CREATE = '/product/create';
export const PRODUCT_UPDATE = '/product/update';
export const PRODUCT_DELETE = '/product/delete';
export const PRODUCT_VIEW = '/product/view';
export const PRODUCT_SEARCH = '/product/search';

export const SALESMAN_LIST = '/salesman';
export const SALESMAN_CREATE = '/salesman/create';
export const SALESMAN_UPDATE = '/salesman/update';
export const SALESMAN_DELETE = '/salesman/delete';
export const SALESMAN_VIEW = '/salesman/view';


export const SUPPLIER_ORDER_CREATE ='/supplier-order/create';
export const SUPPLIER_ORDER_LIST ='/supplier-order';
export const SUPPLIER_ORDER_UPDATE = '/supplier-order/update';
export const SUPPLIER_ORDER_DELETE = '/supplier-order/delete';
export const SUPPLIER_ORDER_VIEW = '/supplier-order/view';
export const SUPPLIER_ORDER_ITEM_DELETE = '/supplier-order/remove-item';
export const SUPPLIER_ORDER_ITEM_UPDATE = '/supplier-order/update-item';
export const SUPPLIER_ORDER_ITEM_ADD = '/supplier-order/add-item';


export const DAILY_NEEDS = '/daily-product/need';

export const SUPPLIER_LIST = '/supplier';
export const SUPPLIER_CREATE = '/supplier/create';
export const SUPPLIER_UPDATE = '/supplier/update';
export const SUPPLIER_DELETE = '/supplier/delete';
export const SUPPLIER_VIEW = '/supplier/view';
export const SUPPLIER_SEARCH = '/supplier/search';


export const ORDER_LIST = '/order';
export const ORDER_CREATE = '/order/create';
export const ORDER_VIEW = '/order/view';
export const ORDER_UPDATE = '/order/update';
export const ORDER_DELIVERED='/order/delivered';
export const ORDER_DELETE = '/order/delete';
export const ORDER_ITEM_DELETE = '/order/remove-item';
export const ORDER_ITEM_UPDATE = '/order/update-item';
export const ORDER_ITEM_ADD = '/order/add-item';

export const SALES_ORDER_LIST = '/sales-order';
export const SALES_ORDER_CREATE = '/sales-order/create';
export const SALES_ORDER_VIEW = '/sales-order/view';
export const SALES_ORDER_UPDATE = '/sales-order/update';
export const SALES_ORDER_DELETE = '/sales-order/delete';
export const SALES_ORDER_ITEM_DELETE = '/sales-order/remove-item';
export const SALES_ORDER_ITEM_UPDATE = '/sales-order/update-item';
export const SALES_ORDER_ITEM_ADD = '/sales-order/add-item';
export const Sales_ORDER_SALESMAN = '/sales-order/sales-man';
export const SALES_ORDER_PRODUCT = '/sales-order/product';
export const SALES_ORDER_PRINT_REQ = '/sales-order/print-request';

export const Sales_ORDER_LastOrder = '/sales-order/last-order';

export const USER_LIST = '/user/list';
export const USER_VIEW = '/user/view';
export const USER_CREATE = '/user/create';
export const USER_UPDATE = '/user/update';
export const USER_DELETE = '/user/delete';

export const ROLE_LIST = '/user/role-list';
export const ROLE_ADD = '/role/add-role';
export const ROLE_DELETE = '/role/remove';
export const GET_PERMISSION = '/role/get-permissions';
export const UPDATE_PERMISSION = '/role/update-permission';
export const PERMISSION_LIST = '/role/permissions';

export const MESSAGE_ALERT_TYPES = '/sms/type';
export const MESSAGE_ALERT_VIEW = '/sms/view';
export const MESSAGE_ALERT_UPDATE = '/sms/update';

export const SMS_VENDOR_LIST = '/sms-account';
export const SMS_VENDOR_CREATE = '/sms-account/create';
export const SMS_VENDOR_UPDATE = '/sms-account/update';
export const SMS_VENDOR_DELETE = '/sms-account/delete';
export const SMS_VENDOR_VIEW = '/sms-account/view';

export const ORDER_ITEM_LIST = '/order-item';

export const REPORTS_SUPPLIER_ORDER_ITEMS='/report/supplier-order';
export const DAILY_WANTED_REPORTS='/report/daily-need';
export const REPORTS ='/report/order-items';
export const CUSTOMER_REPORTS = '/report/customer';

export const KEEPALIVE = '/default';

export const buildURL = (action, params = {}) => {
    let url = "";
    Object.keys(params).forEach((keys, index) => {

        if (index === 0) {
            url += "?" + keys + "=" + params[keys]
        } else {
            url += "&" + keys + "=" + params[keys]
        }
    })

    if (process.env.NODE_ENV !== "production") {

        const lparams = url === "" ? "?" : url;
        return BASE_URL + action + lparams + "&token=" + ACCESS_TOKEN;
    }
    return BASE_URL + action + url;

};

export const userProfile = () =>{
    const user =  window.__PRELOADED_STATE__ ;
    return user.global.user;
}

export const menuItem = () =>{
    const user =  window.__PRELOADED_STATE__ ;
    return user.global.menu_items;
}

