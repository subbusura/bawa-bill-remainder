export const _ORDER_STATUS=[
    { id: 10, name: 'Pending' },
    { id: 20, name: 'Processing' },
    { id: 15, name: 'Completed' },
    { id: 25, name: 'Delivered' },
    { id: 30, name: 'Not in Supply' },
    { id: 40, name: 'Not Available' },
    { id: 50, name: 'Canceled' },
];

export const _SUPPLIER_ORDER_STATUS=[
    { id: 10, name: 'Pending' },
    { id: 20, name: 'Processing' },
    { id: 15, name: 'Completed' },
    { id: 25, name: 'Delivered' },
    { id: 30, name: 'Not in Supply' },
    { id: 40, name: 'Not Available' },
    { id: 50, name: 'Canceled' },
];

export const _SUPPLIER_SEARCH_BY= [
    { id: 'supplier', name: 'Supplier' },
    { id: 'order_no', name: 'Order No' },
    { id: 'mobile_number', name: 'Mobile Number' },
];

export const _SEARCH_BY= [
    { id: 'product', name: 'Product' },
    { id: 'order_no', name: 'Order No' },
    { id: 'mobile_number', name: 'Mobile Number' },
];

export const _ORDER_ITEM_STATUS=[
    { id: 20, name: 'Processing' },
    { id: 25, name: 'Delivered' },
    { id: 30, name: 'Not in Supply' },
    { id: 40, name: 'Not Available' },
    { id: 50, name: 'Canceled' },
];

export const _DELIVERY_TYPE=[
    { id: 10, name: 'By Store' },
    { id: 20, name: 'Door Delivery' },
    { id: 30, name: 'By Courier' }
];