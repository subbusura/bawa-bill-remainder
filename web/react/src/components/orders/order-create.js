import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';
import Select, { Async } from 'react-select';
import CameraModal from './CameraModal';
import ImageModal from './imageModal';

import NewProductModal from './../salesorder/new-product-modal';

import { buildURL, ORDER_CREATE, PRODUCT_LIST, SUPPLIER_LIST, Sales_ORDER_SALESMAN, PRODUCT_SEARCH, SUPPLIER_SEARCH } from './../../constants/api';

export default class OrderCreate extends React.Component {

    constructor(props) {
        super(props);
        this.shortKey = this.shortKey.bind(this);
        this.state = {
            modal_status: false,
            order: {
                name: '',
                mobile_number: '',
                advance_payment: '',
                line_items: [],
                image_path: '',
                remarks: '',
                salesman_id: "",
                telephone: '',
            },
            item: {
                supplier_code: '',
                supplier_code: '',
                supplier_name: '',
                product_code: '',
                product_value: '',
                product_name: '',
                qty: '',

            },
            error: {
                name: [],
                mobile_number: [],
                advance_payment: [],
                supplier_code: [],
                supplier_name: [],
                product_code: [],
                product_name: [],
                qty: [],
                remarks: [],
                salesman_id: [],
                telephone: [],
            },
            empty: {
                name: [],
                mobile_number: [],
                advance_payment: [],
                supplier_code: [],
                supplier_name: [],
                product_code: [],
                product_name: [],
                qty: [],
                remarks: [],
                salesman_id: [],
                telephone: [],
            },
            item_index: '',
            isUpdate: false,
            product_list: [],
            supplier_list: [],
            product_items: [],
            supplier_items: [],
            selected_product: '',
            selected_supplier: '',
            update_item: '',
            imageStatus: false,
            imageSrc: '',
            imageModal: false,
            salesmanList: [],
            selected_salesman: "",
            isAdd: false,
            itemError: {
                product_name: [],
                qty: [],
                supplier_name: [],
            },
            imageDelete: true,
            productModal: false,
        }
    }

    componentWillMount() {
        //this.getSupplierList();
        //this.getProductList();
        this.getSalesman();
    }

    componentDidMount() {
        this.orderSuccess.focus();
        document.addEventListener("keydown", this.shortKey, false)
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.shortKey, false)
    }

    getProductList() {
        Axios.get(buildURL(PRODUCT_LIST, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setProductData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    getSupplierList() {
        Axios.get(buildURL(SUPPLIER_LIST, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setSupplierData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    getSalesman() {
        Axios.get(buildURL(Sales_ORDER_SALESMAN, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setSalesMan(mData.data);
                    //this.setProductData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    setSalesMan(salesmans) {
        let mProduct = [];   //this.state.product_items;
        for (let i = 0; i < salesmans.length; i++) {
            let item = {
                label: salesmans[i].name,
                value: salesmans[i].id,
                name: salesmans[i].product_code,
            }
            mProduct.push(item);
        }
        this.setState({ salesmanList: mProduct });
    }

    setProductData(product) {
        let mProduct = this.state.product_items;
        for (let i = 0; i < product.length; i++) {
            let item = {
                label: product[i].product_name,
                value: product[i].product_name,
                name: product[i].product_code,
            }
            mProduct.push(item);
        }
        this.setState({ product_list: product, product_items: mProduct });
    }

    setSupplierData(supplier) {
        let mSupplier = this.state.supplier_items;

        for (let i = 0; i < supplier.length; i++) {
            let item = {
                label: supplier[i].supplier_code,
                value: supplier[i].supplier_code,
                name: supplier[i].supplier_name,
            }
            mSupplier.push(item);
        }
        this.setState({ supplier_list: supplier, supplier_items: mSupplier });
    }

    onClickImage(e) {
        //return (<Webcam />)
        this.fileInput.click();
    }

    onCameraClick(e) {

        this.setState({ modal_status: true });

    }

    onModalClose() {

        this.setState({ modal_status: false, imageModal: false, productModal: false });

    }

    onNewProduct(e) {
        e.preventDefault();
        this.setState({ productModal: true });
    }

    HandleFileInput(e) {
        let file = this.fileInput.files;
        let mImage = '';
        let files = [];
        for (var i = 0; i < file.length; i++) {
            this.getBase64(file[i], (mfile) => this.fileReceived(mfile));
        }
        this.setState({ imageSrc: mImage, imageStatus: true });
    }
    getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
        };
    }

    fileReceived(file) {

        //console.log("Image BASE64", file);
        this.setState({ imageSrc: file, imageStatus: true });
    }

    onSelectDataChange(name, value) {
        if (name === 'product_name') {
            let mItem = this.state.item;
            if (value !== null) {
                mItem.product_code = value.name;
                mItem.product_name = value.label;
                mItem.product_value = value.label;
            } else {
                mItem.product_name = '';
                mItem.product_code = '';
            }
            this.setState({ item: mItem, selected_product: value });
        } else if (name === 'supplier_name') {
            let mItem = this.state.item;
            if (value !== null) {
                mItem.supplier_code = value.name;
                mItem.supplier_name = value.label;
                //mItem.product_value = value.label;
            } else {
                mItem.supplier_name = '';
                mItem.supplier_code = '';
            }
            this.setState({ item: mItem, selected_supplier: value });
        }
        else if (name === 'salesman_id') {
            let mOrder = this.state.order;
            if (value !== null) {
                mOrder.salesman_id = value.value;

                //mItem.product_value = value.label;
            } else {
                mOrder.salesman_id
            }
            this.setState({ order: mOrder, selected_salesman: value });
        }
    }

    onHandleChange(e) {
        let mItem = this.state.item;
        mItem[e.target.name] = e.target.value;
        if (e.target.name === 'product_code') {
            mItem.product_name = e.target.options[e.target.selectedIndex].getAttribute('data-name');
        }
        if (e.target.name === 'supplier_code') {
            mItem.supplier_name = e.target.options[e.target.selectedIndex].getAttribute('data-name');
        }
        this.setState({ item: mItem });
    }

    onCustomerChange(e) {
        const re = /^[0-9\b]+$/;
        let mItem = this.state.order;
        if (e.target.name === 'mobile_number' || e.target.name === 'telephone') {
            if (e.target.value === '' || re.test(e.target.value)) {
                mItem[e.target.name] = e.target.value;
            }
        } else {
            mItem[e.target.name] = e.target.value.toUpperCase();
        }
        this.setState({ order: mItem });
    }

    onSave(e) {
        e.preventDefault();

        this.resetError();

        let mError = this.validate();
        if (mError.product_name.length > 0 || mError.qty.length > 0) {
            this.setState({ itemError: mError });
            return false;
        } else {
            this.setState({ itemError: { product_name: [], qty: [], supplier_name: [] } });
        }

        let mOrder = this.state.order;
        let mItem = this.state.item;
        let itemList = this.state.order.line_items;

        // let error = this.validateItem(mItem);
        // Object.keys(error).forEach((key)=>{
        //     if(error[key].length>0){
        //         console.log(error[key].length)
        //         return false;
        //     }
        // })

        for (let i = 0; i < itemList.length; i++) {
            if (mItem.product_code === itemList[i].product_code) {
                window.alert("Item already exist")
                return false;
            }
        }

        if (mItem.product_name === "" || mItem.supplier_name === "" || mItem.qty === "") {
            return false;
        }

        let item_temp = {
            supplier_code: '',
            supplier_name: '',
            product_code: '',
            product_name: '',
            qty: '',
        };
        Object.keys(item_temp).forEach((key) => {
            item_temp[key] = mItem[key];
        });
        mOrder.line_items.push(item_temp)
        this.setState({ order: mOrder }, () => this.resetItem());
    }

    validate() {
        let mProduct = this.state.item;
        let mError = {
            product_name: [],
            qty: [],
            supplier_name: [],
        };
        if (mProduct.product_code === "") {
            mError.product_name.push("Product cannot be empty");
        }
        if (mProduct.supplier_code === "") {
            mError.supplier_name.push("Supplier cannot be empty");
        }
        if (mProduct.qty === "") {
            mError['qty'].push("Quantity cannot be empty");
        }
        else if (mProduct.qty <= 0) {
            mError.qty.push("Quantity must be greater than zero");
        }
        return mError;
    }

    validateItem() {
        let mItem = this.state.item;
        let mError = this.state.error;
        let mOrder = this.state.order;
        if (mItem.product_code === "") {
            mError.product_code.push('Product code cannot be blank');
        }
        if (mItem.product_name === "") {
            mError.product_name.push('Product name cannot be blank');
        }
        if (mItem.supplier_code === "") {
            mError.supplier_code.push('Supplier code cannot be blank');
        }
        if (mItem.supplier_name === "") {
            mError.supplier_name.push('Supplier name cannot be blank');
        }
        if (mItem.qty === "") {
            mError.qty.push('Quantity cannot be blank');
        }
        if (mOrder.name === "") {
            mError.name.push('Name cannot be blank');
        }
        if (mOrder.mobile_number === "") {
            mError.mobile_number.push('Phone number cannot be blank');
        }
        if (mOrder.advance_payment === "") {
            mError.advance_payment.push('Advance payment cannot be blank');
        }
        this.setState({ error: mError });
        return mError;
    }

    resetError() {
        let mError = this.state.error;
        Object.keys(mError).forEach((key) => {
            mError[key] = [];
        });
        this.setState({ error: mError });
    }

    onUpdate(e) {
        e.preventDefault();
        let mOrder = this.state.order;
        let mItem = this.state.item;
        let itemList = this.state.order.line_items;

        // let error = this.validateItem(mItem);
        // Object.keys(error).forEach((key)=>{
        //     if(error[key].length>0){
        //         console.log(error[key].length)
        //         return false;
        //     }
        // })

        for (let i = 0; i < itemList.length; i++) {
            if (this.state.update_item !== mItem.product_code && mItem.product_code === itemList[i].product_code) {
                this.inputSelect.focus();
                window.alert("Item already exist");
                return false;
            }
        }

        let item_temp = {
            supplier_code: '',
            supplier_name: '',
            product_code: '',
            product_name: '',
            qty: '',
        };
        Object.keys(item_temp).forEach((key) => {
            item_temp[key] = mItem[key];
        });

        mOrder.line_items[this.state.item_index] = item_temp;

        this.setState({ order: mOrder, isUpdate: false }, () => this.resetItem());

    }

    resetItem() {
        this.inputSelect.focus();
        let mItem = this.state.item;
        Object.keys(mItem).forEach((key) => {
            mItem[key] = ""
        });
        this.setState({ item: mItem, selected_product: '', selected_supplier: '' });
    }

    onItemEdit(e, item, index) {
        e.preventDefault();
        // let mItem = this.state.item;
        // let mIndex = this.state.item_index;
        // mItem = item;
        // mIndex = index;
        let mSelectedProduct = {
            label: item.product_name,
            value: item.product_name,
            name: item.product_code,
        }
        let mSelectedSupplier = {
            label: item.supplier_name,
            value: item.supplier_name,
            name: item.supplier_code,
        }

        const mItem = item;
        let mNew = {};
        Object.keys(mItem).forEach((key) => {
            mNew[key] = mItem[key];
        })

        this.setState({
            item: mNew,
            item_index: index,
            update_item: item.product_code,
            isUpdate: true,
            selected_product: mSelectedProduct,
            selected_supplier: mSelectedSupplier
        });
    }

    onItemDelete(e, index) {
        e.preventDefault();
        let mOrder = this.state.order;

        let mFilter = mOrder.line_items.filter((item, mIndex) => {
            if (index !== mIndex) {
                return item;
            }
        });
        mOrder.line_items = mFilter;
        this.setState({ order: mOrder, isUpdate: false }, () => this.resetItem());
    }

    shortKey(e) {

        if (e.altKey && e.which === 70) {
            if (!this.state.isAdd && this.state.order.line_items.length > 0) {
                this.onOrderCreate(e)
            }
        }

        //Alt+P
        if (e.altKey && e.which === 80) {
            this.setState({ productModal: true });
        }
    }

    onOrderCreate(e) {
        e.preventDefault();
        this.setState({ isAdd: true });
        let mOrder = {
            name: this.state.order.name,
            mobile_number: this.state.order.mobile_number,
            advance_payment: this.state.order.advance_payment,
            line_items: this.state.order.line_items,
            telephone: this.state.order.telephone,
            //image: '',
            image_path: this.state.imageSrc,
            remarks: this.state.order.remarks,
            salesman_id: this.state.order.salesman_id,
        }

        this.resetError();
        Axios.post(buildURL(ORDER_CREATE), mOrder)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert('Order created successully');
                    //window.location = '/order'
                    this.resetData();
                    this.onPrint(mData.data);
                    this.setState({ isAdd: false })
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false, isAdd: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isAdd: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isAdd: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false, isAdd: false });
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error)
                    this.setState({ progressStatus: false, isAdd: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false, isAdd: false });
            });
    }

    onPrint(order) {

        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            window.open('http://bawademo.test/print?id=' + order.id, 'sharer', 'toolbar=0,status=0,width=548,height=325');
        } else {
            window.open('/print?id=' + order.id, 'sharer', 'toolbar=0,status=0,width=548,height=325');
        }
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    resetData() {
        this.orderSuccess.focus();
        let mOrder = this.state.order;
        Object.keys(mOrder).forEach((key) => {
            if (key === 'line_items') {
                mOrder[key] = []
            } else {
                mOrder[key] = "";
            }
        });
        this.setState({ order: mOrder, imageSrc: '', imageStatus: false, selected_salesman: '' });
    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }

    resetError() {
        let mError = this.state.error;
        Object.keys(mError).forEach((key) => {
            mError[key] = [];
        });
        this.setState({ error: mError });
    }

    onImageClick(e) {
        e.preventDefault();
        this.setState({ imageModal: true });
    }

    onDeleteImage() {
        this.setState({ imageSrc: '', imageModal: false, imageStatus: false })
    }

    loadProducts(value, callback) {
        Axios.get(buildURL(PRODUCT_SEARCH, { query: value }))
            .then((response) => {
                let product = response.data.data;
                let mProduct = [];
                for (let i = 0; i < product.length; i++) {
                    let item = {
                        label: product[i].product_name,
                        value: product[i].product_name,
                        name: product[i].product_code,
                    }
                    mProduct.push(item);
                }
                //console.log('mProduct',mProduct);
                let data = { options: mProduct }
                callback(null, data);
            })
            .catch((error) => {
                callback(null, [])
            })
    }

    loadSupplier(value, callback) {
        Axios.get(buildURL(SUPPLIER_SEARCH, { query: value }))
            .then((response) => {
                let product = response.data.data;
                let mProduct = [];
                for (let i = 0; i < product.length; i++) {
                    let item = {
                        label: product[i].supplier_name,
                        value: product[i].supplier_name,
                        name: product[i].supplier_code,
                    }
                    mProduct.push(item);
                }
                //console.log('mProduct',mProduct);
                let data = { options: mProduct }
                callback(null, data);
            })
            .catch((error) => {
                callback(null, [])
            })
    }

    render() {
        return (
            <div>
                <div className='row'>
                    {this.renderCusomerForm()}
                </div>
                <div className='row'>
                    {this.renderItemsForm()}
                </div>
                <div className='row'>
                    {
                        this.state.order.line_items.length > 0 ?
                            this.renderItems() : ""
                    }
                </div>
                {
                    this.state.imageModal ? <ImageModal image={this.state.imageSrc} isDelete={this.state.imageDelete} onDeleteImage={() => this.onDeleteImage()} onClose={() => this.onModalClose()} /> : ""
                }
                {
                    this.state.productModal ? <NewProductModal onClose={() => this.onModalClose()} /> : ""
                }
            </div>
        )
    }

    renderCusomerForm() {
        return (
            <div className='box no-border'>
                <div className='box-body'>
                    <div className='col-md-2'>
                        <div className={this.state.error.mobile_number.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Mobile Number</label>
                            <input ref={(node) => this.orderSuccess = node} maxLength={10} className='form-control' type='text' name='mobile_number' value={this.state.order.mobile_number} onChange={(e) => this.onCustomerChange(e)} />
                            <div className='help-block'>{this.state.error.mobile_number.length > 0 ? this.state.error.mobile_number : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.error.telephone.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Telephone Number</label>
                            <input maxLength={13} className='form-control' type='text' name='telephone' value={this.state.order.telephone} onChange={(e) => this.onCustomerChange(e)} />
                            <div className='help-block'>{this.state.error.telephone.length > 0 ? this.state.error.telephone : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.error.name.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Customer Name</label>
                            <input className='form-control' name='name' style={{ textTransform: 'uppercase' }} value={this.state.order.name} onChange={(e) => this.onCustomerChange(e)} />
                            <div className='help-block'>{this.state.error.name.length > 0 ? this.state.error.name : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.error.advance_payment.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Advance Payment</label>
                            <input className='form-control' type='number' name='advance_payment' value={this.state.order.advance_payment} onChange={(e) => this.onCustomerChange(e)} />
                            <div className='help-block'>{this.state.error.advance_payment.length > 0 ? this.state.error.advance_payment : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.error.salesman_id.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Sales Representative</label>
                            <Select
                                ref={(node) => this.SalesinputSelect = node}
                                name="salesman_id"
                                value={this.state.selected_salesman}
                                onChange={(data) => this.onSelectDataChange("salesman_id", data)}
                                options={this.state.salesmanList}
                            />
                            <div className='help-block'>{this.state.error.salesman_id.length > 0 ? this.state.error.salesman_id : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.error.remarks.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Remarks</label>
                            <textarea className='form-control' name='remarks' style={{ textTransform: 'uppercase' }} value={this.state.order.remarks} onChange={(e) => this.onCustomerChange(e)} />

                            <div className='help-block'>{this.state.error.remarks.length > 0 ? this.state.error.remarks : ""}</div>
                        </div>
                    </div>
                    {this.state.imageStatus ?
                        <div className='col-md-2'>
                            <img style={{ height: '100px', width: '200px' }} src={this.state.imageSrc} alt='' onClick={(e) => this.onImageClick(e)} />
                        </div>
                        :
                        ""
                    }
                    <div className='col-md-2 pull-right '>
                        <input
                            type="file"
                            ref={input => {
                                this.fileInput = input;
                            }}
                            style={{ display: "none" }}
                            onChange={(e) => this.HandleFileInput(e)}

                            accept="image/*"
                        />
                        <div className='form-group'>
                            <label>Image Upload</label>
                            <div>
                                <a className='btn btn-warning' onClick={(e) => this.onClickImage(e)}><i className='fa fa-folder-open-o' /></a>
                                <button className='btn btn-info' onClick={(e) => this.onCameraClick(e)}><i className='fa fa-camera' />

                                </button>
                                {/* <img src={this.state.image} alt='' /> */}
                            </div>

                            {this.state.modal_status ? <CameraModal onReceived={(file) => this.fileReceived(file)} onClose={() => this.onModalClose()} /> : ""}

                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderItemsForm() {

        let product = this.state.product_list.map((item, index) => {
            return (
                <option key={index} data-name={item.product_name} value={item.id}>{item.product_name}</option>
            )
        })
        let supplier = this.state.supplier_list.map((item, index) => {
            return (
                <option key={index} data-name={item.supplier_name} value={item.id}>{item.supplier_name}</option>
            )
        })

        return (

            <div className='box no-border'>
                <div className='box-body'>
                    <div className='col-md-4'>
                        <div className={this.state.itemError.product_name.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Product Name</label>
                            {/* <input className='form-control' disabled name='product_name' value={this.state.item.product_name} onChange={(e) => this.onHandleChange(e)} /> */}
                            <Select.Async
                                ref={(node) => this.inputSelect = node}
                                name="product_name"
                                value={this.state.selected_product}
                                onChange={(data) => this.onSelectDataChange("product_name", data)}
                                loadOptions={this.loadProducts}
                                defaultOptions
                                cacheOptions
                            />
                            <div className='help-block'>{this.state.itemError.product_name.length > 0 ? this.state.itemError.product_name : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.itemError.supplier_name.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Supplier Name</label>
                            {/* <input className='form-control' disabled name='supplier_name' value={this.state.item.supplier_name} onChange={(e) => this.onHandleChange(e)} /> */}
                            <Select.Async
                                ref={(node) => this.supplierInputSelect = node}
                                name="supplier_name"
                                value={this.state.selected_supplier}
                                onChange={(data) => this.onSelectDataChange("supplier_name", data)}
                                loadOptions={this.loadSupplier}
                                defaultOptions
                                cacheOptions
                            />
                            <div className='help-block'>{this.state.itemError.supplier_name.length > 0 ? this.state.itemError.supplier_name : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.itemError.qty.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Quantity</label>
                            <input className='form-control' type='number' name='qty' value={this.state.item.qty} onChange={(e) => this.onHandleChange(e)} />
                            <div className='help-block'>{this.state.itemError.qty.length > 0 ? this.state.itemError.qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        {
                            this.state.isUpdate ?
                                <button className='btn btn-primary col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onUpdate(e)}>Update</button>
                                :
                                <button className='btn btn-success col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onSave(e)}>Add</button>
                        }

                    </div>

                    <div className='col-md-2'>
                        <button className='btn btn-warning col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onNewProduct(e)}>Add New Product (Alt+P) </button>
                    </div>
                </div>
            </div>
        )
    }

    renderItems() {

        let items = this.state.order.line_items.map((item, index) => {
            return (
                <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.product_name}</td>
                    <td>{item.supplier_name}</td>
                    <td>{item.qty}</td>
                    <td>
                        {
                            <div>
                                <a href="" onClick={(e) => this.onItemEdit(e, item, index)} ><span className='fa fa-pencil'></span> </a>
                                <a href="" onClick={(e) => this.onItemDelete(e, index)} style={{ marginLeft: '10px' }} ><span className='fa fa-trash'></span> </a>
                            </div>
                        }
                    </td>
                </tr>
            )
        })

        return (
            <div className='box no-border'>
                <div className='box-header with-border'>
                    <h3 className='box-title'>Order Iems</h3>
                    <button className='btn btn-success pull-right col-md-1' disabled={this.state.isAdd} onClick={(e) => this.onOrderCreate(e)} >{'Save (Alt + F)'}</button>
                </div>
                <div className='box-body table-responsive no-padding'>
                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Product Name</th>
                                <th>Supplier Name</th>
                                <th>Quantity</th>
                                <th>Acition</th>
                            </tr>
                        </thead>
                        <tbody>{items}</tbody>
                    </table>
                </div>
            </div>
        )
    }
}