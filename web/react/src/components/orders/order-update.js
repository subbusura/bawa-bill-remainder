import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';
import Select, { Async } from 'react-select';
import CameraModal from './CameraModal';
import ImageModal from './imageModal';
import { _ORDER_STATUS,_DELIVERY_TYPE } from './../../constants/global';
import { buildURL, ORDER_VIEW, ORDER_CREATE, ORDER_ITEM_ADD, SUPPLIER_LIST, PRODUCT_LIST, PRODUCT_SEARCH, SUPPLIER_SEARCH, ORDER_UPDATE, Sales_ORDER_SALESMAN } from './../../constants/api';

import OrderItems from './order-items';

export default class OrderUpdate extends React.Component {

    constructor(props) {
        super(props);
        this.shortKey = this.shortKey.bind(this);
        let params = this.props.match.params;
        this.state = {
            modal_status: false,
            order: {
                name: '',
                mobile_number: '',
                advance_payment: '',
                items: [],
                image: '',
                image_path: '',
                remarks: '',
                salesman_id: "",
                order_status: '',
                telephone: '',
                d_type:''
            },
            item: {
                supplier_code: '',
                supplier_name: '',
                product_code: '',
                product_name: '',
                qty: '',
            },
            item_index: '',
            isUpdate: false,
            order_id: params.id,
            product_list: [],
            supplier_list: [],
            product_items: [],
            supplier_items: [],
            selected_product: '',
            selected_supplier: '',
            error: {
                name: [],
                mobile_number: [],
                advance_payment: [],
                supplier_code: [],
                supplier_name: [],
                product_code: [],
                product_name: [],
                qty: [],
                remarks: [],
                salesman_id: [],
                telephone: [],
            },
            empty: {
                name: [],
                mobile_number: [],
                advance_payment: [],
                supplier_code: [],
                supplier_name: [],
                product_code: [],
                product_name: [],
                qty: [],
                remarks: [],
                salesman_id: [],
                telephone: [],
            },
            imageStatus: false,
            imageSrc: '',
            imageModal: false,
            salesmanList: [],
            selected_salesman: "",
            isOrderUpdate: false,
            itemError: {
                product_name: [],
                qty: [],
                supplier_name: [],
            },
            imageDelete: true,
        }
    }

    componentDidMount() {
        this.getOrder();
        document.addEventListener("keydown", this.shortKey, false)
    }

    componentWillMount() {
        //this.getProductList();
        //this.getSupplierList();
        this.getSalesman();
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.shortKey, false)
    }

    shortKey(e) {

        if (e.altKey && e.which === 70) {
            if (!this.state.isOrderUpdate && this.state.order.items.length > 0) {
                this.onUpdateOrder(e)
            }
        }
    }

    onClickImage(e) {

        this.fileInput.click();
    }

    onCameraClick(e) {

        this.setState({ modal_status: true });

    }

    onModalClose() {

        this.setState({ modal_status: false, imageModal: false });

    }

    HandleFileInput(e) {
        let file = this.fileInput.files;
        let mImage = '';
        let files = [];
        for (var i = 0; i < file.length; i++) {
            this.getBase64(file[i], (mfile) => this.fileReceived(mfile));
        }
        //(mImage)

    }

    getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
        };
    }

    fileReceived(file) {

        //console.log("Image BASE64", file);
        this.setState({ imageSrc: file, imageStatus: true });
    }

    getProductList() {
        Axios.get(buildURL(PRODUCT_LIST, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setProductData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    getSupplierList() {
        Axios.get(buildURL(SUPPLIER_LIST, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setSupplierData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    getSalesman() {

        Axios.get(buildURL(Sales_ORDER_SALESMAN, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {

                    this.setSalesMan(mData.data);

                    //this.setProductData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    setSalesMan(salesmans) {

        let mProduct = [];   //this.state.product_items;

        for (let i = 0; i < salesmans.length; i++) {
            let item = {
                label: salesmans[i].name,
                value: salesmans[i].id,
                name: salesmans[i].product_code,
            }
            mProduct.push(item);
        }
        this.setState({ salesmanList: mProduct });
    }

    setProductData(product) {
        let mProduct = this.state.product_items;

        for (let i = 0; i < product.length; i++) {
            let item = {
                label: product[i].product_name,
                value: product[i].product_name,
                name: product[i].product_code,
            }
            mProduct.push(item);
        }
        this.setState({ product_list: product, product_items: mProduct });
    }

    setSupplierData(supplier) {
        let mSupplier = this.state.supplier_items;

        for (let i = 0; i < supplier.length; i++) {
            let item = {
                label: supplier[i].supplier_name,
                value: supplier[i].supplier_name,
                name: supplier[i].supplier_code,
            }
            mSupplier.push(item);
        }
        this.setState({ supplier_list: supplier, supplier_items: mSupplier });
    }

    onDeliveryTypeChange(e){
        
                let mItem = this.state.order;
                mItem[e.target.name] = e.target.value;
                this.setState({ order: mItem });
    }

    onSelectDataChange(name, value) {
        if (name === 'product_name') {
            let mItem = this.state.item;
            if (value !== null) {
                mItem.product_code = value.name;
                mItem.product_name = value.label;
                mItem.product_value = value.label;
            } else {
                mItem.product_name = '';
                mItem.product_code = '';
            }
            this.setState({ item: mItem, selected_product: value });
        } else if (name === 'supplier_name') {
            let mItem = this.state.item;
            if (value !== null) {
                mItem.supplier_code = value.name;
                mItem.supplier_name = value.label;
                //mItem.product_value = value.label;
            } else {
                mItem.supplier_name = '';
                mItem.supplier_code = '';
            }
            this.setState({ item: mItem, selected_supplier: value });
        }
        else if (name === 'salesman_id') {
            let mOrder = this.state.order;
            if (value !== null) {
                mOrder.salesman_id = value.value;

                //mItem.product_value = value.label;
            } else {
                mOrder.salesman_id
            }
            this.setState({ order: mOrder, selected_salesman: value });
        }
    }

    getOrder() {
        Axios.get(buildURL(ORDER_VIEW, { order_id: this.state.order_id, expand: 'items,salesMan' }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setOrders(mData.data);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    setOrders(order) {
        let mImage = this.state.imageSrc;
        let status = false;
        let mSalesMan = this.state.selected_salesman;
        Object.keys(order).forEach((key) => {
            if (key === 'image_path') {
                if (order[key] !== null && order[key] !== "") {
                    mImage = order[key];
                    status = true
                }
            } else if (key === 'salesMan') {
                if (order[key] !== null) {
                    mSalesMan = {
                        value: order[key].id,
                        label: order[key].name,
                    }
                }
            }
        })
        this.setState({ order: order, imageSrc: mImage, imageStatus: status, selected_salesman: mSalesMan });
    }

    onSave(e) {
        e.preventDefault();

        let mError = this.validate();
        if (mError.product_name.length > 0 || mError.qty.length > 0) {
            this.setState({ itemError: mError });
            return false;
        } else {
            this.setState({ itemError: { product_name: [], qty: [], supplier_name: [] } });
        }

        let mItem = this.state.item;
        let itemList = this.state.order.items;
        for (let i = 0; i < itemList.length; i++) {
            if (mItem.product_code === itemList[i].product_code) {
                window.alert("Item already exist")
                return false;
            }
        }
        Axios.post(buildURL(ORDER_ITEM_ADD, { order_id: this.state.order_id }), this.state.item)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert('Added successully');
                    this.resetItem()
                    this.getOrder();
                    //window.location = '/order'
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    validate() {
        let mProduct = this.state.item;
        let mError = {
            product_name: [],
            qty: [],
            supplier_name: [],
        };
        if (mProduct.product_code === "") {
            mError.product_name.push("Product cannot be empty");
        }
        if (mProduct.supplier_code === "") {
            mError.supplier_name.push("Supplier cannot be empty");
        }
        if (mProduct.qty === "") {
            mError['qty'].push("Quantity cannot be empty");
        }
        else if (mProduct.qty <= 0) {
            mError.qty.push("Quantity must be greater than zero");
        }
        return mError;
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    resetItem() {
        this.inputSelect.focus();
        let mItem = this.state.item;
        Object.keys(mItem).forEach((key) => {
            mItem[key] = "";
        })
        this.setState({ item: mItem, selected_product: '', selected_supplier: '' });
    }

    onCustomerChange(e) {
        const re = /^[0-9\b]+$/;
        let mItem = this.state.order;
        if (e.target.name === 'mobile_number' || e.target.name === 'telephone') {
            if (e.target.value === '' || re.test(e.target.value)) {
                mItem[e.target.name] = e.target.value;
            }
        } else {
            mItem[e.target.name] = e.target.value.toUpperCase();
        }
        this.setState({ order: mItem });
    }

    onHandleChange(e) {
        let mItem = this.state.item;
        mItem[e.target.name] = e.target.value;
        if (e.target.name === 'product_code') {
            mItem.product_name = e.target.options[e.target.selectedIndex].getAttribute('data-name');
        }
        if (e.target.name === 'supplier_code') {
            mItem.supplier_name = e.target.options[e.target.selectedIndex].getAttribute('data-name');
        }
        this.setState({ item: mItem });
    }

    onUpdateOrder(e) {
        e.preventDefault();
        this.setState({ isOrderUpdate: true })
        let item = {
            name: this.state.order.name,
            mobile_number: this.state.order.mobile_number,
            advance_payment: this.state.order.advance_payment,
            telephone:this.state.order.telephone,
            image_path: this.state.imageSrc,
            salesman_id: this.state.order.salesman_id,
            order_status: this.state.order.order_status,
            remarks: this.state.order.remarks,
            d_type:this.state.order.d_type
        };
        Axios.post(buildURL(ORDER_UPDATE, { order_id: this.state.order_id }), item)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert('Updated successully')
                    this.setState({ isOrderUpdate: false });
                    //window.location = '/order';
                    window.history.back();
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false, isOrderUpdate: false });
            })
    }

    loadProducts(value, callback) {
        Axios.get(buildURL(PRODUCT_SEARCH, { query: value }))
            .then((response) => {
                let product = response.data.data;
                let mProduct = [];
                for (let i = 0; i < product.length; i++) {
                    let item = {
                        label: product[i].product_name,
                        value: product[i].product_name,
                        name: product[i].product_code,
                    }
                    mProduct.push(item);
                }
                //console.log('mProduct',mProduct);
                let data = { options: mProduct }
                callback(null, data);
            })
            .catch((error) => {
                callback(null, [])
            })
    }

    loadSupplier(value, callback) {
        Axios.get(buildURL(SUPPLIER_SEARCH, { query: value }))
            .then((response) => {
                let product = response.data.data;
                let mProduct = [];
                for (let i = 0; i < product.length; i++) {
                    let item = {
                        label: product[i].supplier_name,
                        value: product[i].supplier_name,
                        name: product[i].supplier_code,
                    }
                    mProduct.push(item);
                }
                //console.log('mProduct',mProduct);
                let data = { options: mProduct }
                callback(null, data);
            })
            .catch((error) => {
                callback(null, [])
            })
    }

    onItemAddSuccess(item) {
        let order = this.state.order;
        order.items.push(item);
        this.setState({ order: order });
    }

    onItemDelete(id) {
        let order = this.state.order;
        let filter = order.items.filter((item, index) => {
            if (id !== item.id) {
                return item;
            }
        })
        order.items = filter;
        this.setState({ order: order });
    }

    onImageClick(e) {
        e.preventDefault();
        this.setState({ imageModal: true });
    }

    onDeleteImage() {
        this.setState({ imageSrc: '', imageModal: false, imageStatus: false })
    }

    render() {
        return (
            <div>
                <div className='row'>
                    <div>
                        <button className='btn btn-info col-md-1 pull-right ' disabled={this.state.isOrderUpdate} onClick={(e) => this.onUpdateOrder(e)} >{'Update (Alt+F)'}</button>
                    </div>
                </div>
                <div className='row'>
                    <br />
                    {this.renderCusomerForm()}
                </div>
                <div className='row'>
                    {this.renderItemsForm()}
                </div>
                <div className='row'>
                    {
                        this.state.order.items.length > 0 ?
                            this.renderItems() : ""
                    }
                </div>
                {
                    this.state.imageModal ? <ImageModal image={this.state.imageSrc} isDelete={this.state.imageDelete} onDeleteImage={() => this.onDeleteImage()} onClose={() => this.onModalClose()} /> : ""
                }
            </div>
        )
    }

    renderCusomerForm() {
        let mStatus = _ORDER_STATUS.map((item, index) => {
            return (<option value={item.id} key={index}>{item.name}</option>)
        })
        
        let deliveryType = _DELIVERY_TYPE.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })
        return (
            <div>
                <div className='box no-border'>
                    <div className='box-body'>
                        <div className='row'>
                            <div className='col-md-2'>
                                <div className='form-group'>
                                    <label>Phone Number</label>
                                    <input className='form-control' maxLength={10} name='mobile_number' value={this.state.order.mobile_number} onChange={(e) => this.onCustomerChange(e)} />
                                </div>
                            </div>
                            <div className='col-md-2'>
                                <div className={this.state.error.telephone.length > 0 ? 'form-group has-error' : 'form-group'}>
                                    <label>Telephone Number</label>
                                    <input maxLength={13} className='form-control' type='text' name='telephone' value={this.state.order.telephone} onChange={(e) => this.onCustomerChange(e)} />
                                    <div className='help-block'>{this.state.error.telephone.length > 0 ? this.state.error.telephone : ""}</div>
                                </div>
                            </div>
                            <div className='col-md-2'>
                                <div className='form-group'>
                                    <label>Customer Name</label>
                                    <input className='form-control' style={{ textTransform: 'uppercase' }} name='name' value={this.state.order.name} onChange={(e) => this.onCustomerChange(e)} />
                                </div>
                            </div>

                            <div className='col-md-2'>
                                <div className='form-group'>
                                    <label>Advance Payment</label>
                                    <input className='form-control' name='advance_payment' value={this.state.order.advance_payment} onChange={(e) => this.onCustomerChange(e)} />
                                </div>
                            </div>

                     

                            <div className='col-md-2'>
                                <div className={this.state.error.salesman_id.length > 0 ? 'form-group has-error' : 'form-group'}>
                                    <label>Sales Man</label>
                                    <Select
                                        ref={(node) => this.SalesinputSelect = node}
                                        name="salesman_id"
                                        value={this.state.selected_salesman}
                                        onChange={(data) => this.onSelectDataChange("salesman_id", data)}
                                        options={this.state.salesmanList}
                                    />
                                    <div className='help-block'>{this.state.error.salesman_id.length > 0 ? this.state.error.salesman_id : ""}</div>
                                </div>
                            </div>
                            <div className='col-md-2'>

                        <div className='form-group'>
                        <label>Delivery Type</label>
                            <select className='form-control' value={this.state.order.d_type} style={{ marginLeft: '10px', marginRight: '10px' }} name='d_type' onChange={(e) => { this.onDeliveryTypeChange(e) }}>
                                {deliveryType}
                             </select>
                         </div>
                        
                        </div>
                        </div>
                    </div>
                </div>
                <div className='box no-border'>
                    <div className='box-body'>
                        <div className='row'>
                            <div className='col-md-3'>
                                <div className={this.state.error.remarks.length > 0 ? 'form-group has-error' : 'form-group'}>
                                    <label>Remarks</label>
                                    <textarea className='form-control' style={{ textTransform: 'uppercase' }} name='remarks' value={this.state.order.remarks} onChange={(e) => this.onCustomerChange(e)} />

                                    <div className='help-block'>{this.state.error.remarks.length > 0 ? this.state.error.remarks : ""}</div>
                                </div>
                            </div>
                            <div className='col-md-3'>
                                <div className='form-group'>
                                    <label>Staus</label>
                                    <select className='form-control pull-left' name='order_status' value={this.state.order.order_status} onChange={(e) => this.onCustomerChange(e)} >
                                        <option value="">--Select--</option>
                                        {mStatus}
                                    </select>
                                </div>
                            </div>
                            {this.state.imageStatus ?
                                <div className='col-md-3'>
                                    <img style={{ height: '100px', width: '200px' }} src={this.state.imageSrc} alt='' onClick={(e) => this.onImageClick(e)} />
                                </div>
                                :
                                ""
                            }
                            <div className='col-md-3 pull-right'>
                                <input
                                    type="file"
                                    ref={input => {
                                        this.fileInput = input;
                                    }}
                                    style={{ display: "none" }}
                                    onChange={(e) => this.HandleFileInput(e)}

                                    accept="image/*"
                                />
                                <div className='form-group'>
                                    <label>Image Upload</label>
                                    <div>
                                        <a className='btn btn-warning' onClick={(e) => this.onClickImage(e)}><i className='fa fa-folder-open-o' /></a>
                                        <button className='btn btn-info' onClick={(e) => this.onCameraClick(e)}><i className='fa fa-camera' /></button>
                                        {this.state.modal_status ? <CameraModal onReceived={(file) => this.fileReceived(file)} onClose={() => this.onModalClose()} /> : ""}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderItems() {

        return (
            <div className='col-md-10 col-md-offset-1'>
                <div className='box no-border'>
                    <div className='box-body with-border'>
                        <h3 className='box-title'>Order Items</h3>
                    </div>
                    <div className='box-body no-padding'>
                        {
                            this.state.order.items.map((item, index) => {
                                return (
                                    <div key={index} >
                                        <OrderItems
                                            items={item}
                                            orderId={this.state.order_id}
                                            product={this.state.product_list}
                                            supplier={this.state.supplier_list}
                                            onDeleteSuccess={(id) => this.onItemDelete(id)}
                                        />
                                        <hr style={{ marginTop: '1px', marginBottom: '1px' }} />
                                    </div>

                                )
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }

    renderItemsForm() {

        let product = this.state.product_list.map((item, index) => {
            return (
                <option key={index} data-name={item.product_name} value={item.id}>{item.product_code}</option>
            )
        })
        let supplier = this.state.supplier_list.map((item, index) => {
            return (
                <option key={index} data-name={item.supplier_name} value={item.id}>{item.supplier_code}</option>
            )
        })

        return (

            <div className='box no-border'>
                <div className='box-body'>
                    {/* <div className='col-md-2'>
                        <div className='form-group'>
                            <label>Product Code</label>
                            <Select
                                ref={(node) => this.inputSelect = node}
                                name="product_code"
                                value={this.state.item.product_code}
                                onChange={(data) => this.onSelectDataChange("product_code", data)}
                                options={this.state.product_items}
                            />
                        </div>
                    </div> */}
                    <div className='col-md-4'>
                        <div className={this.state.itemError.product_name.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Product Name</label>
                            {/* <input className='form-control' disabled name='product_name' value={this.state.item.product_name} onChange={(e) => this.onHandleChange(e)} /> */}
                            <Select.Async
                                ref={(node) => this.inputSelect = node}
                                name="product_name"
                                value={this.state.selected_product}
                                onChange={(data) => this.onSelectDataChange("product_name", data)}
                                loadOptions={this.loadProducts}
                                defaultOptions
                                cacheOptions
                            />
                            <div className='help-block' style={{ fontSize: '12px' }}  >{this.state.itemError.product_name.length > 0 ? this.state.itemError.product_name : ""}</div>
                        </div>
                    </div>

                    <div className='col-md-2'>
                        <div className={this.state.itemError.supplier_name.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Supplier Name</label>
                            {/* <input className='form-control' disabled name='supplier_name' value={this.state.item.supplier_name} onChange={(e) => this.onHandleChange(e)} /> */}
                            <Select.Async
                                ref={(node) => this.supplierInputSelect = node}
                                name="supplier_name"
                                value={this.state.selected_supplier}
                                onChange={(data) => this.onSelectDataChange("supplier_name", data)}
                                loadOptions={this.loadSupplier}
                                defaultOptions
                                cacheOptions
                            />
                            <div className='help-block' style={{ fontSize: '12px' }}  >{this.state.itemError.supplier_name.length > 0 ? this.state.itemError.supplier_name : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.itemError.qty.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Quantity</label>
                            <input className='form-control' type='number' name='qty' value={this.state.item.qty} onChange={(e) => this.onHandleChange(e)} />
                            <div className='help-block' style={{ fontSize: '12px' }}  >{this.state.itemError.qty.length > 0 ? this.state.itemError.qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        {
                            this.state.isUpdate ?
                                <button className='btn btn-primary pull-right col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onUpdate(e)}>Update</button>
                                :
                                <button className='btn btn-success pull-right col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onSave(e)}>Add</button>
                        }

                    </div>
                </div>
            </div>
        )
    }

}