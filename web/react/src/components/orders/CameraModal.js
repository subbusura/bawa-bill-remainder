import React from 'react';
import Webcam from 'react-webcam';
export default class CameraModal extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            isShow: true,
            order: {
                name: '',
                mobile_number: '',
                advance_payment: '',
                items: [],
                image: '',
                order_no: '',
                date: '',
            },
        }
    }

    setRef = (webcam) => {

        this.webcam = webcam;
    }

    onCameraClick() {

        const imageSrc = this.webcam.getScreenshot();
        this.props.onReceived(imageSrc);
        //console.log("image Capture", imageSrc);

    }

    componentDidMount() {
        if (this.props.order !== null) {
            this.setState({ order: this.props.order });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.order !== null) {
            this.setState({ order: nextProps.order });
        }
    }

    onClose(e) {
        e.preventDefault();
        this.setState({ isShow: false });
        this.props.onClose();
    }

    render() {

        return (
            <div>
                <div className={this.state.isShow ? 'modal fade in' : 'modal fade'} style={this.state.isShow ? { display: 'block', paddingRight: '16px' } : { display: 'none' }} >

                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">

                                <button type="button" className="close" onClick={(e) => this.onClose(e)} data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 className="modal-title">Web Camera</h4>

                            </div>
                            <div className="modal-body" style={{ maxHeight: "450px", overflow: 'auto' }} >

                                <Webcam
                                    audio={false}
                                    height={350}
                                    ref={this.setRef}
                                    screenshotFormat="image/jpeg"
                                    width={350}
                                    videoConstraints={{
                                        width: 1280,
                                        height: 720,
                                        facingMode: 'user',
                                    }}
                                />

                                <button className='btn btn-info' onClick={(e) => this.onCameraClick(e)}><i className='fa fa-camera' />

                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )

    }

}