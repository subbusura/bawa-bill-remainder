import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';


import { buildURL, ORDER_DELETE,ORDER_DELIVERED, ORDER_UPDATE } from './../../constants/api';

export default class OrdersListItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            canOrder: props.orderEdit,
            order: {
                id: '',
                name: '',
                mobile_number: '',
                advance_payment: '',
                items: [],
                image: '',
                salesMan: null,
                image_path: '',
                remarks: '',
                confim_one:'',
                confim_two:''
            },
            modal_status: false,
        }
    }

    componentDidMount() {
        
        this.setState({ order: this.props.item })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ order: nextProps.item })
    }

    componentDidUpdate(prevProps) {

        //console.log("component did update", prevProps);

        if (this.props.orderEdit !== prevProps.orderEdit) {

            this.setState({ canOrder: true })

        }
    }

    onDelete(e) {
        e.preventDefault();
        if (window.confirm('Do you want to remove this order?')) {

            Axios.delete(buildURL(ORDER_DELETE, { order_id: this.state.order.id }))
                .then((response) => {
                    let mData = response.data;
                    if (mData.statusCode === 200) {
                        this.props.onDeleteSuccess();
                    } else if (mData.statusCode === 500) {
                        this.errorAlert('Internal Server Error')
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 401) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 403) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 404) {
                        this.errorAlert('Page not found');
                        this.setState({ progressStatus: false });
                    }
                })
                .catch((error) => {
                    this.errorAlert(error);
                });
        }
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    getStatusColor(code) {

        let status = {};
        switch (code) {
            case 10:
                status = {
                    border: "1px solid #dd4b39"
                };
                break;
            case 15:
                status = {
                    border: "1px solid #00a65a"
                };
                break;
            case 20:
                status = {
                    border: "1px solid #f39c12"
                };
                break
            case 25:
                status = {
                    border: "1px solid #3c8dbc"
                };
                break;
            case 50:
                status = {
                    border: "1px solid #db12f3"
                };
                
                break;
        }
        return status;
    }

    getBackgroundColor(code) {

        let status = {};
        switch (code) {
            case 10:
                status = {
                    backgroundColor: "#dd4b39",
                    color: '#ffffff',//#f39c12,
                };
                break;
            case 15:
                status = {
                    backgroundColor: "#00a65a",
                    color: '#ffffff'
                };
                break;
            case 20:
                status = {
                    backgroundColor: "#f39c12",//"#00c0ef",
                    color: '#ffffff'
                };
                break;
            case 25:
                status = {
                    backgroundColor: "#3c8dbc",
                    color: '#ffffff'
                };
                break;
            case 30:
                status = {
                    backgroundColor: "#141415",
                    color: '#ffffff'
                };
                break;
            case 40:
                status = {
                    backgroundColor: "#e84393",
                    color: '#ffffff'
                };
                break;
            case 50:
                status = {
                    backgroundColor: "#db12f3",
                    color: '#ffffff'
                };
                break;
        }
        return status;
    }

    getItemStatusColor(code){

        let status = {};
        switch (code) {
            case 10:
                status = {
                    color: '#dd4b39',//#f39c12,
                };
                break;
            case 15:
                status = {
                    color: '#00a65a'
                };
                break;
            case 20:
                status = {
                    color: '#f39c12'
                };
                break;
            case 25:
                status = {
                    color: '#3c8dbc'
                };
                break;
            case 30:
                status = {
                    color: '#141415'
                };
                break;
                case 40:
                status = {
                    color: '#e84393'
                };
                break;
            case 50:
                status = {
                    color: '#db12f3'
                };
                break;
        }
        return status;

    }

    onChangeCheckBox(e){

        const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        let order = this.state.order;
        if(e.target.checked)
        {
           
            order[e.target.name]=1;
           // console.log(e.target.name,1,"Checkbox");
        }else{
            order[e.target.name]=0;
           // console.log(0,"Checkbox");
        }
        this.sendCheckbox(order.id,{confim_one:order.confim_one,confim_two:order.confim_two});
        this.setState({order:order});
        

    }

    sendCheckbox(orderid,data){

        Axios.post(buildURL(ORDER_UPDATE, { order_id: this.state.order.id }), data)
            .then((response) => {
                let mData = response.data;
                if (parseInt(mData.statusCode) === 200) {
                    this.successAlert('Updated successully')
                    
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false, isOrderUpdate: false });
            })


    }

    onView(e) {
        e.preventDefault();
        this.props.onModal(this.state.order, this.state.canOrder);
    }

    onEdit(e) {
        e.preventDefault();
        if (this.props.canOrder) {
            this.props.history.push('/sales/order/update/' + this.state.order.id);
        } else {
            this.props.history.push('/order/update/' + this.state.order.id);
        }
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    onUpdateStatus(e) {
        e.preventDefault();
        let item = {
            order_status: 25
        };
        Axios.get(buildURL(ORDER_DELIVERED, { order_id: this.state.order.id }), item)
            .then((response) => {
                let mData = response.data;
                if (parseInt(mData.statusCode) === 200) {
                    this.successAlert('Updated successully')
                    this.setState({ isOrderUpdate: false });
                    this.props.onStatusSuccess();
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false, isOrderUpdate: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false, isOrderUpdate: false });
            })
    }

    onImageView(e) {
        e.preventDefault();
        this.props.onImageModal(this.state.order.image_path);
    }

    render() {

         let order_type = "";

         if(this.state.order.d_type==10)
         {
            order_type="By Store";

         }else if(this.state.order.d_type==20)
         {
            order_type="Door Delivery";
         }else if(this.state.order.d_type==30)
         {
             order_type="By Courier";
         }
       
        return (

            <div className='box no-border' >
                <div className="box-header with-border" style={this.getBackgroundColor(parseInt(this.state.order.order_status))} >

                    <div className='row'>
                        <h3 className='col-md-6 ' style={{ fontSize: '14px', marginTop: '-3px', marginBottom: '-3px' }} >Order No : {this.state.order.id}</h3>
                        <h3 className='col-md-6 ' style={{ fontSize: '14px', marginTop: '-3px', marginBottom: '-3px' }} >Date : {this.state.order.formatted_date}</h3>
                    </div>
                </div>
                <div className='box-body'>
                    <div className='row'>
                        <h3 className='col-md-12' style={{ fontSize: '12px', marginTop: '-3px' }} >Name : {this.state.order.name}</h3>
                        <h3 className='col-md-12' style={{ fontSize: '12px', marginTop: '-3px' }} >Phone No : {this.state.order.mobile_number}</h3>
                        <h3 className='col-md-12' style={{ fontSize: '12px', marginTop: '-3px' }} >Advance : {this.state.order.advance_payment}</h3>
                        <h3 className='col-md-12' style={{ fontSize: '12px', marginTop: '-3px' }} >Sales Rep : {this.state.order.salesMan !== null ? this.state.order.salesMan.name : ""}</h3>
                        <h3 className='col-md-12' style={{ fontSize: '12px', marginTop: '-3px' }} >Delivery Type : {order_type}</h3>
                    </div>
                    <div className='row' style={{ height: '100px', overflow: 'auto' }}>
                        <div className='col-md-12'>
                            {this.renderTable()}
                        </div>
                    </div>

                </div>
                <div className='box-footer' style={{ height: '130px' }}>
                    {
                        this.state.order.remarks !== '' ?
                            < div className='row'>
                                <h4 className='col-md-3' style={{ fontSize: '12px', marginTop: '-3px' }} ><strong>Remarks:</strong></h4>
                                <h4 className='col-md-9'
                                    style={{
                                        fontSize: '12px',
                                        marginTop: '-3px',
                                        width: "210px",
                                        whiteSpace: "nowrap",
                                        overflow: "hidden",
                                        textOverflow: "ellipsis"
                                    }} >
                                    {this.state.order.remarks}
                                </h4>
                            </div>
                            :
                            ""
                    }
                    <div className='row'>
                        <h3 className='col-md-3' style={{ fontSize: '14px', }} >{this.state.canOrder === true ? <a href="#" onClick={(e) => this.onEdit(e)} className='pull-right'>Edit</a> : <a href="#" onClick={(e) => this.onEdit(e)} className='pull-right'> Edit</a>}</h3>
                        <h3 className='col-md-3' style={{ fontSize: '14px', }} >{this.state.canOrder === true ? <a href="#" onClick={(e) => this.onView(e)} className='pull-right'> View</a> : <a href="#" onClick={(e) => this.onView(e)} className='pull-right'> View</a>}</h3>
                        <div className='col-md-5' style={{ marginTop: "15px" }} >
                            {parseInt(this.state.order.order_status) === 15 ?
                                <a href="#" onClick={(e) => this.onUpdateStatus(e)} > <span className='label label-info' >Mark as Delivered</span></a>
                                :
                                ""
                            }
                        </div>
                    </div>
                    <div className='row'>
                        {
                            this.state.order.image_path !== null && this.state.order.image_path !== "" ?
                                <a href="#" onClick={(e) => this.onImageView(e)} className='col-md-12' ><h3 className='col-md-6 col-md-offset-3' style={{ fontSize: '14px', marginTop: '3px' }} >ATTACHMENT</h3></a>
                                : ""
                        }
                    </div>
                    <div className='row'>
                        <div className='col-md-6'>
                            <label className="checkbox-inline"><input  name={"confim_one"} checked={this.state.order.confim_one===1?true:false} onChange={(e)=>this.onChangeCheckBox(e,'varified')} type='checkbox' />Verified</label>
                        </div>
                        <div className='col-md-6'>
                            <label className="checkbox-inline"><input name={"confim_two"}  checked={this.state.order.confim_two===1?true:false} onChange={(e)=>this.onChangeCheckBox(e,'not_reachable')} type='checkbox' />Not Reachable</label>
                        </div>
                    </div>

                </div>
            </div >

        )
    }

    renderTable() {
        let items = this.state.order.items.map((item, index) => {

            let style=this.getItemStatusColor(parseInt(item.arrival_status));

            return (
                // <tr key={index} >
                //     <td style={{ fontSize: '12px' }} >{item.product_name}</td>
                //     <td style={{ fontSize: '12px' }} >{item.qty}</td>
                // </tr>
                <ul key={index} style={{ listStyleType: 'none', margin: '0px', padding: '0px' }} >
                    <li style={style}>
                        <div>
                            <div className='col-md-8 pull-left '>{item.product_name}</div>
                            <div className='col-md-1' style={{ paddingLeft: '20px' }} >{item.qty}</div>
                            <div className='col-md-1' style={{ paddingLeft: '10px' }} >{item.qty-item.arrival_qty}</div>
                        </div>
                    </li>
                </ul>
            )
        })
        return (
            // <table className='table' style={{ maxHeight: '10px' }}  >
            //     {/* <thead>
            //         <tr>
            //             <th style={{ fontSize: '12px' }} >Product Name</th>
            //             <th style={{ fontSize: '12px' }} >Quantity</th>
            //         </tr>
            //     </thead>
            //     <tbody>{items}</tbody> */}
            // </table>
            <div>
                <div>
                    <div className='col-md-8'><label>Product Name</label></div>
                    <div className='col-md-1'><label>Qty</label></div>
                    <div className='col-md-1'><label>Pending</label></div>
                </div>
                {items}
            </div>
        )
    }

}