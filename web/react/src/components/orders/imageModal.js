import React from 'react';

export default class ImageModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isShow: true,
        }
    }

    onClose(e) {
        e.preventDefault();
        this.setState({ isShow: false });
        this.props.onClose();
    }

    onDelete(e) {
        e.preventDefault();
        if (window.confirm("Confire..!\nDo you want to delete this image?")) {
            this.props.onDeleteImage();
        }
    }

    render() {
        return (
            <div>
                <div className={this.state.isShow ? 'modal fade in' : 'modal fade'} style={this.state.isShow ? { display: 'block', paddingRight: '16px' } : { display: 'none' }} >

                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" onClick={(e) => this.onClose(e)} data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 className="modal-title">Image</h4>

                            </div>
                            <div className="modal-body" style={{ maxHeight: "500px", overflow: 'auto' }} >

                                <img src={this.props.image} alt='' />

                            </div>
                            {this.props.isDelete ?
                                < div className='modal-footer'>
                                    <button className='btn btn-danger col-md-2 pull-right' onClick={(e) => this.onDelete(e)} >DELETE</button>
                                </div>
                                : ""
                            }
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}