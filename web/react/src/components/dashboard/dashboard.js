import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';
import Pagination from 'react-js-pagination';
import * as moment from 'moment';
import OrderViewModal from './../orders/order-view-modal';
import ImageModal from './../orders/imageModal';

import OrdersListItem from './../orders/order-list-item';
import Select from 'react-select';

import { buildURL, ORDER_LIST, Sales_ORDER_SALESMAN } from './../../constants/api';

import {_ORDER_STATUS,_SEARCH_BY,_ORDER_ITEM_STATUS} from './../../constants/global';

export default class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            orders: [],
            canOrder: false,
            filter: {
                name: '',
                from: '',
                to: '',
                order_status: '',
                item_status: '',
                search_by:'product'
            },
            order: null,
            modal_status: false,
            salesmanList: [],
            image_modal: false,
            image_path: '',
            imageDelete:false,
        }
    }

    componentWillMount() {
        this.CheckUpdatePermissions();
        this.getOrderList();
        this.getSalesman();
    }

    CheckUpdatePermissions() {

        Axios.get("/api/permission/check", { params: { permission: 'Order List' } })
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ canOrder: true })
                } else if (mData.statusCode === 500) {

                    this.setState({ canOrder: false })
                } else if (mData.statusCode === 401) {
                    this.setState({ canEdit: false })

                } else if (mData.statusCode === 403) {
                    this.setState({ canOrder: false })

                } else if (mData.statusCode === 404) {
                    this.setState({ canOrder: false })

                }
            })
            .catch((error) => {
                //this.errorAlert(error);
            });

    }

    getOrderList() {
        Axios.get(buildURL(ORDER_LIST, { expand: 'items,salesMan', 'per-page':0, page:1 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ orders: mData.data.items, progressStatus: false, page: mData.data._meta });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    componentDidMount() {

        let from = moment().format("YYYY-MM-DD");
        let today = moment().format("YYYY-MM-DD");

        const filter = this.state.filter;

        let cachFrom=localStorage.getItem("from");
        let cachTo=localStorage.getItem("to");
      //  console.log("From Data",cachFrom,"to Date",cachTo);
        filter.from =cachFrom===null?from:cachFrom;
        filter.to = cachTo===null?today:cachTo;
        this.setState({
            filter: filter
        })
    }

    getSalesman() {
        Axios.get(buildURL(Sales_ORDER_SALESMAN, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setSalesMan(mData.data);
                    //this.setProductData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    setSalesMan(salesmans) {
        let mProduct = [];   //this.state.product_items;
        for (let i = 0; i < salesmans.length; i++) {
            let item = {
                label: salesmans[i].name,
                value: salesmans[i].id,
                name: salesmans[i].product_code,
            }
            mProduct.push(item);
        }
        this.setState({ salesmanList: mProduct });
    }

    onFilter(e) {
        e.preventDefault();
        Axios.post(buildURL(ORDER_LIST, { expand: 'items,salesMan', 'per-page':0, page:1 }), this.state.filter)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ orders: mData.data.items, progressStatus: false, page: mData.data._meta });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }


    onDateChange(e){

        let mFilter = this.state.filter;
        

        if(e.target.name=='from')
        {
            mFilter[e.target.name] = e.target.value;
            localStorage.setItem("from",e.target.value);
        }else if(e.target.name=='to'){

            mFilter[e.target.name] = e.target.value;
            localStorage.setItem("to",e.target.value);
        }


        this.setState({ filter: mFilter });

    }

    onHandleChange(e) {
        let mFilter = this.state.filter;
        mFilter[e.target.name] = e.target.value;
        this.setState({ filter: mFilter });
    }

    onModalView(order) {
        this.setState({ order: order, modal_status: true });
    }

    onImageModal(image) {
        this.setState({ image_modal: true, image_path: image });
    }

    onStatusSuccess(){
        this.getOrderList();
    }

    onModalClose() {
        this.setState({ modal_status: false, image_modal: false });
    }

    render() {
        return (
            <div>
                <div className='row'>
                    {this.renderFilter()}
                </div>
                <div className='row'>
                    <br />
                    {this.renderItems()}
                </div>
                {this.state.modal_status ? <OrderViewModal order={this.state.order} onClose={() => this.onModalClose()} canOrder={this.state.canOrder} /> : ""}
                {this.state.image_modal ? <ImageModal image={this.state.image_path} isDelete={this.state.imageDelete} onClose={() => this.onModalClose()} /> : ""}
            </div>
        )
    }

    renderFilter() {

        let mStatus = _ORDER_STATUS.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })

        let mOrderItems = _ORDER_ITEM_STATUS.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })

        let searchBy = _SEARCH_BY.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })

        return (
            <div>
                <div>
                    <div className='col-md-12'>
                        <div className='form-inline'>
                            <div className='form-group'>
                                <label>Search</label>
                                <input className='form-control' style={{ textTransform:'uppercase', marginLeft: '10px', marginRight: '10px', width: '200px', fontSize: '13px', padding: '1px' }} name='name' placeholder='' onChange={(e) => { this.onHandleChange(e) }} />
                            </div>

                            <div className='form-group'>
                                <label>By</label>
                                <select className='form-control' style={{ marginLeft: '10px', marginRight: '10px' }} name='search_by' onChange={(e) => { this.onHandleChange(e) }}>
                                   {searchBy}
                                </select>
                            </div>

                            <div className='form-group'>
                                <label>From</label>
                                <input type='date' className='form-control' value={this.state.filter.from} style={{ marginLeft: '10px', marginRight: '10px' }} name='from' placeholder='from' onChange={(e) => { this.onDateChange(e) }} />
                            </div>
                            <div className='form-group'>
                                <label>To</label>
                                <input type='date' className='form-control' value={this.state.filter.to} style={{ marginLeft: '10px', marginRight: '10px' }} name='to' placeholder='to' onChange={(e) => { this.onDateChange(e) }} />
                            </div>
                            <div className='form-group'>
                                <label>Order</label>
                                <select className='form-control' style={{ marginLeft: '10px', marginRight: '10px' }} name='item_status' onChange={(e) => { this.onHandleChange(e) }}>
                                    <option value='' >All</option>
                                    {mStatus}
                                </select>
                            </div>
                            <div className='form-group'>
                                <label>Item</label>
                                <select className='form-control' style={{ marginLeft: '10px', marginRight: '10px' }} name='order_status' onChange={(e) => { this.onHandleChange(e) }}>
                                     <option value='' >All</option>
                                        <option value='15' >Completed</option>
                                         {mOrderItems}
                                </select>
                            </div>
                            <button className='btn btn-info' onClick={(e) => this.onFilter(e)} >Search</button>
                            {/* <div className='form-group'>
                                <label>Sales Rep</label>
                                <Select
                                    ref={(node) => this.SalesinputSelect = node}
                                    name="salesman_id"
                                    value={this.state.selected_salesman}
                                    onChange={(data) => this.onSelectDataChange("salesman_id", data)}
                                    options={this.state.salesmanList}
                                />
                            </div> */}
                        </div>
                        <br/>
                        <div className='form-group'>
                            <span className='label label-danger' style={{ fontSize: '10px' }} >Pending</span>
                            <span className='label label-warning' style={{ fontSize: '10px', marginLeft: '2px' }} >Processing</span>
                            <span className='label label-success' style={{ fontSize: '10px', marginLeft: '2px' }} >Completed</span>
                            <span className='label label-primary' style={{ fontSize: '10px', marginLeft: '2px' }} >Delivered</span>
                            <span className='label' style={{ fontSize: '10px', marginLeft: '2px',backgroundColor:'#141415' }} >Not In Supply</span>
                            <span className='label' style={{ fontSize: '10px', marginLeft: '2px',backgroundColor:'#e84393' }} >Not Available</span>
                            <span className='label' style={{ fontSize: '10px', marginLeft: '2px',backgroundColor:'#db12f3' }} >Canceled</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderItems() {
        return (
            <div>
                {
                    this.state.orders.map((item, index) => {
                        return (
                            <div className='col-md-3' key={index}>
                                <OrdersListItem
                                    history={this.props.history}
                                    key={index} item={item}
                                    orderEdit={this.state.canOrder}
                                    onModal={(result, canOrder) => this.onModalView(result, canOrder)}
                                    onImageModal={(result) => this.onImageModal(result)}
                                    onStatusSuccess={() => this.onStatusSuccess()}
                                />
                            </div>
                        )
                    })
                }
            </div>
        )
    }

}