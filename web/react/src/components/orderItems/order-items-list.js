import React from 'react';
import Select, { Async } from 'react-select';
import Axios from 'axios';
import Alert from 'react-s-alert';
import { buildURL, ORDER_ITEM_LIST, SUPPLIER_SEARCH, SUPPLIER_LIST, ORDER_ITEM_UPDATE } from './../../constants/api';
import { _ORDER_STATUS, _SEARCH_BY, _ORDER_ITEM_STATUS } from './../../constants/global';
import index from 'axios';

export default class OrderItemsList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            itemList: [],
            supplier_list: [],
            selectedSupplier: {
                value: '',
                label: '',
                index: '',
            },
            supplier_items: [],
            progressStatus: false,
            filter: {
                name: '',
                search_by: 'product'
            },
        }
    }

    componentWillMount() {
        this.getSupplierList();
        this.getItems();
    }

    getItems() {
        Axios.get(buildURL(ORDER_ITEM_LIST))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setItems(mData.data);
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found')
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    getSupplierList() {
        Axios.get(buildURL(SUPPLIER_LIST, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setSupplierData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    onFilter(e) {
        e.preventDefault();
        Axios.get(buildURL(ORDER_ITEM_LIST, this.state.filter))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ itemList: mData.data, progressStatus: false });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    setSupplierData(supplier) {
        let mSupplier = [];

        for (let i = 0; i < supplier.length; i++) {
            let item = {
                label: supplier[i].supplier_name,
                value: supplier[i].supplier_code,
                name: supplier[i].supplier_name,
            }
            mSupplier.push(item);
        }
        this.setState({ supplier_list: mSupplier });
    }

    loadSupplier(value, callback) {
        if (value === "") { return false }
        Axios.get(buildURL(SUPPLIER_SEARCH, { query: value }))
            .then((response) => {
                let product = response.data.data;
                let mProduct = [];
                for (let i = 0; i < product.length; i++) {
                    let item = {
                        label: product[i].supplier_name,
                        value: product[i].supplier_name,
                        name: product[i].supplier_code,
                    }
                    mProduct.push(item);
                }
                let data = { options: mProduct }
                callback(null, data);
            })
            .catch((error) => {
                callback(null, [])
            })
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    setItems(items) {
        let mItems = [];
        for (let i = 0; i < items.length; i++) {
            let mOrderItem = {
                order_id: '',
                product_name: "",
                qty: "",
                supplier: {
                    label: '',
                    value: ''
                },
                isUpdate: false,
                arrival_status: 20,
            };
            Object.keys(items[i]).forEach((key) => {
                if (key === 'supplier_name') {
                    mOrderItem.supplier.label = items[i]['supplier_name'];
                } else if (key === 'supplier_code') {
                    mOrderItem.supplier.value = items[i]['supplier_code'];
                } else if(key === 'arrival_status'){
                    mOrderItem[key] = 20;
                }else{
                    mOrderItem[key] = items[i][key];
                }
            })
            mItems.push(mOrderItem);
        }
        this.setState({ itemList: mItems, supplier_items: [] });
    }

    onHandleChange(e) {
        let mFilter = this.state.filter;
        mFilter[e.target.name] = e.target.value;
        this.setState({ filter: mFilter });
    }

    onStatusChange(e, item, index) {
        let mItems = this.state.itemList;
        for (let i = 0; i < mItems.length; i++) {
            if (i === index) {
                mItems[i].arrival_status = parseInt(e.target.value);
                mItems[i].isUpdate = true;
            }
        }
        this.setState({ itemList: mItems });
    }
    onSelectDataChange(name, value, index, item) {

        //console.log('Item on change', item)

        let mItems = this.state.itemList;
        let mSupplierItems = this.state.supplier_items;
        let mSupplier = { label: '', value: '', index: '', id: '', order_id: '' }

        if (value !== null) {
            for (let i = 0; i < mItems.length; i++) {
                if (i === index) {
                    mItems[i].supplier = value;
                    mItems[i].isUpdate = true;
                }
            }
            mSupplierItems = mSupplierItems.filter((mItem, fIndex) => {
                if (mItem.id !== item.id) {
                    return mItem;
                }
            })
            mSupplier = value;
            mSupplier.index = index;
            mSupplier.id = item.id;
            mSupplier.order_id = item.order_id;
            mSupplierItems.push(mSupplier);
        }
        else {
            for (let i = 0; i < mItems.length; i++) {
                if (i === index) {
                    mItems[i].supplier = {
                        value: '',
                        label: ''
                    };
                    mItems[i].isUpdate = false;
                }
            }
            mSupplierItems = mSupplierItems.filter((mItem, fIndex) => {
                if (mItem.id !== item.id) {
                    return mItem;
                }
            })
            //console.log('mSupplierItems', mSupplierItems);
            mSupplier = { value: '', label: '', index: '' };

        }
        this.setState({ itemList: mItems, selectedSupplier: mSupplier, supplier_items: mSupplierItems });
    }

    onItemUpdate(e, item, index) {

        this.setState({ progressStatus: true });

        let mItem = {
            supplier_name: '',
            supplier_code: '',
            arrival_status:''
        };

        for (let i = 0; i < this.state.itemList.length; i++) {
            if (i === parseInt(index)) {
                mItem.supplier_name = this.state.itemList[i].supplier.label
                mItem.supplier_code = this.state.itemList[i].supplier.value
                mItem.arrival_status = this.state.itemList[i].arrival_status
            }
        }
        let result = this.onUpdate(item, mItem, index);
    }

    onUpdate(item, mItem, index) {
        Axios.post(buildURL(ORDER_ITEM_UPDATE, { order_id: item.order_id, orderItem: item.id, }), mItem)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert('Updated successully');
                    this.deleteItem(index);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    onMultipleUpdateItem(item, mItem, index) {
        //console.log('mItem',mItem);
        Axios.post(buildURL(ORDER_ITEM_UPDATE, { order_id: item.order_id, orderItem: item.id, }), mItem)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert('Updated successully');
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    deleteItem(index) {
        let mItems = this.state.itemList.filter((item, mIndex) => {
            if (parseInt(index) !== parseInt(mIndex)) {
                return item;
            }
        });
        this.setState({ itemList: mItems, progressStatus: false });
    }

    onMultipleUpdate(e) {
        e.preventDefault();

        //console.log('Item list', this.state.supplier_items);
        //return false;

        this.setState({ progressStatus: true });

        let mItem = {
            supplier_name: '',
            supplier_code: '',
        };

        for (let i = 0; i < this.state.supplier_items.length; i++) {
            mItem.supplier_name = this.state.supplier_items[i].label
            mItem.supplier_code = this.state.supplier_items[i].value

            //console.log('onUpdate', this.state.supplier_items[i], mItem, i)

            this.onMultipleUpdateItem(this.state.supplier_items[i], mItem, i)

        }
        this.setState({ supplier_items: [], progressStatus: false });
        this.getItems();
    }

    render() {
        return (
            <div>
                <div className='row'>
                    {this.renderFilter()}
                    {/* <button
                        className='btn btn-success pull-right'
                        disabled={this.state.supplier_items.length > 0 ? false : true}
                        onClick={(e) => this.onMultipleUpdate(e)}
                    >Update All</button> */}
                </div>
                <div className='row'>
                    <br />
                    {this.renderTable()}
                </div>

            </div>
        )
    }

    renderFilter() {
        let mStatus = _ORDER_STATUS.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })

        let searchBy = _SEARCH_BY.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })

        return (
            <div>
                <div className='col-md-10'>
                    <div className='form-inline'>
                        <div className='form-group'>
                            <label>Search</label>
                            <input className='form-control' style={{ textTransform: 'uppercase', marginLeft: '10px', marginRight: '10px', width: '200px', fontSize: '13px', padding: '1px' }} name='name' placeholder='Mobile No, Order No' onChange={(e) => { this.onHandleChange(e) }} />
                        </div>
                        <div className='form-group'>
                            <label>By</label>
                            <select className='form-control' style={{ marginLeft: '10px', marginRight: '10px' }} name='search_by' onChange={(e) => { this.onHandleChange(e) }}>
                                {searchBy}
                            </select>
                        </div>
                        <button className='btn btn-info col-md-1 pull-right' style={{ marginLeft: '10px', marginRight: '10px' }} onClick={(e) => this.onFilter(e)} >Search</button>
                    </div>
                </div>
            </div>
        )
    }

    renderTable() {

        let status = _ORDER_ITEM_STATUS.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })

        let items = this.state.itemList.map((item, mIndex) => {
            return (
                <tr key={mIndex}>
                    <td>{mIndex + 1}</td>
                    <td>{item.order_id}</td>
                    <td>{item.product_name}</td>
                    <td>
                        {
                            <Select
                                ref={(node) => this.supplierInputSelect = node}
                                name="supplier_name"
                                value={item.supplier}
                                onChange={(data) => this.onSelectDataChange("supplier_name", data, mIndex, item)}
                                options={this.state.supplier_list}
                                defaultOptions
                                cacheOptions
                            />
                        }
                    </td>
                    <td>{item.qty}</td>
                    <td>
                        {
                            <select className='form-control' value={item.arrival_status} name='arrival_status' onChange={(e) => this.onStatusChange(e, item, mIndex)} >
                                {status}
                            </select>
                        }
                    </td>
                    <td>
                        {
                            <button className='btn btn-info' disabled={!item.isUpdate} onClick={(e) => this.onItemUpdate(e, item, mIndex)} >Update</button>
                        }
                    </td>
                </tr>
            )
        })

        let progress = this.state.progressStatus ? this.getProgress() : "";

        return (
            <div className='box no-border' >
                <table className='table table-striped table-bordered'>
                    <thead>
                        <tr className={"table-header-color"} >
                            <th style={{ width: '5%' }} >S.no</th>
                            <th style={{ width: '6%' }} >Order No</th>
                            <th style={{ width: '25%' }}>Product</th>
                            <th style={{ width: '32%' }}>Supplier</th>
                            <th style={{ width: '5%' }} >Quantity</th>
                            <th style={{ width: '10%' }} >Status</th>
                            <th style={{ width: '8%' }} >Action</th>
                        </tr>
                    </thead>
                    <tbody>{items}</tbody>
                </table>
                {progress}
            </div>
        )
    }

    getProgress() {
        return (
            <div className='overlay' >
                <i className='fa fa-refresh fa-spin'></i>
            </div>
        )
    }
}