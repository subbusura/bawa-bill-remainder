import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';
import { Link } from 'react-router-dom';
import * as moment from 'moment'; 
import { buildURL, SUPPLIER_ORDER_LIST, SUPPLIER_ORDER_DELETE, SALES_ORDER_PRINT_REQ } from './../../constants/api';
import SalesOrdersListItem from './supplier-order-list-item';
import SalesOrderViewModal from './supplier-order-view-modal';
import Can from './../permission/Can';
import Pagination from 'react-js-pagination';
import { _SUPPLIER_ORDER_STATUS, _SUPPLIER_SEARCH_BY } from './../../constants/global';

export default class SupplierOrdersList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            list: [],
            order: null,
            canEdit: false,
            canDelete: false,
            modal_status: false,
            filter: {
                name: '',
                from: '',
                to: '',
                order_status: '',
                search_by: 'product'
            },
            page: {
                currentPage: 1,
                pageCount: 1,
                perPage: 20,
                totalCount: 3
            },
        }
    }

    componentWillMount() {
        this.CheckDeletePermissions();
        this.CheckUpdatePermissions();
        this.getOrderList();
    }


    CheckUpdatePermissions() {

        Axios.get("/api/permission/check", { params: { permission: 'supplier order update' } })
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ canEdit: true })
                } else if (mData.statusCode === 500) {

                    this.setState({ canEdit: false })
                } else if (mData.statusCode === 401) {
                    this.setState({ canEdit: false })

                } else if (mData.statusCode === 403) {
                    this.setState({ canEdit: false })

                } else if (mData.statusCode === 404) {
                    this.setState({ canEdit: false })

                }
            })
            .catch((error) => {
                // this.errorAlert(error);
            });


    }

    CheckDeletePermissions() {

        Axios.get("/api/permission/check", { params: { permission: 'supplier order delete' } })
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ canDelete: true })
                } else if (mData.statusCode === 500) {

                    this.setState({ canDelete: false })
                } else if (mData.statusCode === 401) {
                    this.setState({ canDelete: false })

                } else if (mData.statusCode === 403) {
                    this.setState({ canDelete: false })

                } else if (mData.statusCode === 404) {
                    this.setState({ canDelete: false })

                }
            })
            .catch((error) => {
                // this.errorAlert(error);
            });


    }

    componentDidMount() {

        let from = moment().format("YYYY-MM-DD");
        let today = moment().format("YYYY-MM-DD");

        const filter = this.state.filter;
        filter.from = from;
        filter.to = today;
        this.setState({
            filter: filter
        })

    }

    onDeleteSuccess() {
        this.setState({ list: [] });
        this.getOrderList();
    }

    getOrderList() {

        Axios.get(buildURL(SUPPLIER_ORDER_LIST, { expand: 'items,user', 'per-page': this.state.page.perPage, page: this.state.page.currentPage }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ list: mData.data.items, page: mData.data._meta, progressStatus: false, page: mData.data._meta });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    onDelete(e, item) {
        e.preventDefault();
        if (window.confirm('Do you want to remove this order?')) {

            Axios.delete(buildURL(SUPPLIER_ORDER_DELETE, { order_id: item.id }))
                .then((response) => {
                    let mData = response.data;
                    if (mData.statusCode === 200) {
                        this.successAlert('Order deleted successully');
                    } else if (mData.statusCode === 500) {
                        this.errorAlert('Internal Server Error')
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 401) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 403) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 404) {
                        this.errorAlert('Page not found');
                        this.setState({ progressStatus: false });
                    }
                })
                .catch((error) => {
                    this.errorAlert(error);
                });
        }
    }

    onView(e, item) {
        e.preventDefault();
        this.setState({ order: item, modal_status: true });
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    onModalClose() {
        this.setState({ modal_status: false, order: null });
    }

    onHandleChange(e) {
        let mFilter = this.state.filter;
        mFilter[e.target.name] = e.target.value;
        this.setState({ filter: mFilter });
    }

    onFilter(e) {
        e.preventDefault();
        Axios.post(buildURL(SUPPLIER_ORDER_LIST, { expand: 'items,user', 'per-page': this.state.page.perPage, page: this.state.page.currentPage }), this.state.filter)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ list: mData.data.items, page: mData.data._meta, progressStatus: false, page: mData.data._meta });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    onPrint(e, order) {
        e.preventDefault();
       

        if (!window.confirm('Conirm..!\nYou want to print ?')) {
            return false;
        }
          if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
             window.open('http://bawademo.test/print?id=' + order.id, 'sharer', 'toolbar=0,status=0,width=548,height=325');
         } else {

             window.open('/print?id=' + order.id, 'sharer', 'toolbar=0,status=0,width=548,height=325');
         }

    }
    onSMSresend(e, order) {
        e.preventDefault();

        Axios.get('/api/sales-order/resend?order_id=' + order.id)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert("SMS Send successfuly!");
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });


    }

    onPageFilter(page) {
        Axios.post(buildURL(SUPPLIER_ORDER_LIST, { expand: 'items, user', 'per-page': this.state.page.perPage, page: page }), this.state.filter)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ list: mData.data.items, page: mData.data._meta, progressStatus: false, page: mData.data._meta });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    render() {
        return (
            <div>
                <div className='row'>
                    {this.renderFilter()}
                </div>
                <div className='row'>
                    <br />
                    <div className='col-md-12' >
                        {this.renderTable()}
                    </div>
                    {/* <div className='col-md-4'>
                    <OrdersListItem item={item} onDeleteSuccess={()=>this.onDeleteSuccess()} />
                    </div> */}
                </div>
                {this.state.modal_status ? <SalesOrderViewModal order={this.state.order} onClose={() => this.onModalClose()} /> : ""}
            </div>
        )
    }

    renderFilter() {

        let mStatus = _SUPPLIER_ORDER_STATUS.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })

        let searchBy = _SUPPLIER_SEARCH_BY.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })

        return (
            <div>
                <div className='col-md-2'>
                    <Can permission={'supplier order create'}><Link to='/sorder/order/create' className='btn btn-success'>New</Link></Can>
                </div>
                <div className='col-md-10'>
                    <div className='form-inline'>
                        <div className='form-group'>
                            <label>Search</label>
                            <input className='form-control' style={{ textTransform: 'uppercase', marginLeft: '10px', marginRight: '10px', width: '200px', fontSize: '13px', padding: '1px' }} name='name' placeholder='Mobile No, Order No' onChange={(e) => { this.onHandleChange(e) }} />
                        </div>

                        <div className='form-group'>
                            <label>By</label>
                            <select className='form-control' style={{ marginLeft: '10px', marginRight: '10px' }} name='search_by' onChange={(e) => { this.onHandleChange(e) }}>
                                {searchBy}
                            </select>
                        </div>

                        <div className='form-group'>
                            <label>From</label>
                            <input type='date' className='form-control' style={{ marginLeft: '10px', marginRight: '10px' }} value={this.state.filter.from} name='from' placeholder='from' onChange={(e) => { this.onHandleChange(e) }} />
                        </div>
                        <div className='form-group'>
                            <label>To</label>
                            <input type='date' className='form-control' style={{ marginLeft: '10px', marginRight: '10px' }} name='to' value={this.state.filter.to} placeholder='to' onChange={(e) => { this.onHandleChange(e) }} />
                        </div>
                        <div className='form-group'>
                            <label>Status</label>
                            <select className='form-control' style={{ marginLeft: '10px', marginRight: '10px' }} name='order_status' onChange={(e) => { this.onHandleChange(e) }}>
                                <option value='' >All</option>
                                {mStatus}
                            </select>
                        </div>
                        <button className='btn btn-info col-md-1 pull-right' style={{ marginLeft: '10px', marginRight: '10px' }} onClick={(e) => this.onFilter(e)} >Search</button>
                    </div>
                </div>
            </div>
        )
    }

    renderTable() {

        let items = this.state.list.map((item, index) => {

            let status = "";

            switch (item.order_status) {
                case 10:
                    status = <span className="label label-danger">Pending</span>;
                    break;
                case 15:
                    status = <span className="label label-success">Completed</span>;
                    break;
                case 20:
                    status = <span className="label label-warning">Processing</span>;
                    break;
                case 25:
                    status = <span className="label label-primary">Delivered</span>;
                    break;
                case 30:
                    status = <span className='label' style={{ fontSize: '10px', backgroundColor: '#141415' }}>Not In Supply</span>;
                    break;
                case 40:
                    status = <span className='label' style={{ fontSize: '10px', backgroundColor: '#e84393' }}>Not Available</span>;
                    break;
                case 50:
                    status = <span className='label' style={{ fontSize: '10px', backgroundColor: '#db12f3' }}>Canceled</span>;
                    break;
            }


            return (
                <tr key={index}>
                    <td>{((this.state.page.currentPage - 1) * this.state.page.perPage) + (index + 1)}</td>
                    <td>{item.id}</td>
                    <td>{item.supplier_name}</td>
                    <td>{item.mobile_number}</td>
                    <td>{item.company_representative}</td>
                    <td>{item.items.length}</td>
                    <td>{item.supplier_name}</td>
                    <td>{item.remarks}</td>
                    <td>{item.formatted_date}</td>
                    <td>{status}</td>

                    <td>{
                        <div>
                            <a href='#' title='View' style={{ marginRight: '5px' }} onClick={(e) => this.onView(e, item)} ><span className='fa fa-eye' /></a>
                            <Link style={{ marginRight: '5px' }} title='Edit' to={'/sorder/order/update/' + item.id} ><span className='fa fa-edit' /></Link>
                            <a href='#' title='Print' style={{ marginLeft: '5px' }} onClick={(e) => this.onPrint(e, item)} ><span className='fa fa-print' /></a>
                            <a href='#' onClick={(e) => this.onDelete(e, item)} title='Delete' ><span className='fa fa-trash' /></a> 
                        </div>
                    }</td>
                </tr>
            )
        })

        return (
            <div className='box no-border'>
                <div className='box-body table-responsive no-padding'>
                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr className={"table-header-color"} >
                                <th>S.no</th>
                                <th>Order No</th>
                                <th>Supplier Name</th>
                                <th>Mobile No</th>
                                <th>Company Representative</th>
                                <th>No. of Items</th>
                                <th>Sales Representative</th>
                                <th>Remarks</th>
                                <th>Ordered Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>{items}</tbody>
                    </table>
                </div>
                {this.state.list.length > 0 ?
                    <Pagination
                        activePage={this.state.page.currentPage}
                        itemsCountPerPage={this.state.page.perPage}
                        totalItemsCount={this.state.page.totalCount}
                        pageRangeDisplayed={this.state.page.pageCount}
                        onChange={(e, p) => { this.onPageChange(e, p) }}
                    />
                    : ""}
            </div>
        )
    }

    onPageChange(page) {
        let mPage = this.state.page;
        mPage.currentPage = page;
        this.onPageFilter(page);
        this.setState({ page: mPage })
    }

}