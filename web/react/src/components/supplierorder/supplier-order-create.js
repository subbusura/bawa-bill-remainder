import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';
import Select, { Async } from 'react-select';
import SignatureModal from './SignatureModal';
import ImageModal from './../orders/imageModal';

import NewProductModal from './new-product-modal';
import {_DELIVERY_TYPE } from './../../constants/global';
import { buildURL, SUPPLIER_ORDER_CREATE, Sales_ORDER_LastOrder, Sales_ORDER_SALESMAN, PRODUCT_LIST, SUPPLIER_LIST, PRODUCT_SEARCH, SALES_ORDER_PRINT_REQ } from './../../constants/api';


const empty_array ={
    supplier_id: [],
    supplier_name: [],
    mobile_number: [],
    company_representative: [],
    order_status:[],
    line_items: [],
    remarks:[],
    salesman_id:[],
};


export default class SupplierOrderCreate extends React.Component {

    constructor(props){
        super(props)
        this.escFunction = this.escFunction.bind(this);
        this.state={
            modal_status: false,
            supplier_items:[],
            supplier_list:[],
            order:{
                supplier_id: '',
                supplier_name: '',
                mobile_number: '',
                image_path: '',
                signature:'',
                company_representative: '',
                order_status:10,
                line_items: [],
                remarks: "",
                salesman_id: "",
            },
            item:{
                product_code: '',
                product_value: '',
                product_name: '',
                qty: '',
                free_qty:'',
                scheme_value:''
            },
            error: {
                supplier_id: [],
                supplier_name: [],
                mobile_number: [],
                company_representative: [],
                order_status:[],
                line_items: [],
                remarks:[],
                salesman_id:[],
            },
            selectedSupplier: {
                value: '',
                label: '',
                index: '',
            },
            selected_supplier:{
                value: '',
                label: '',
                value: '',
            },
            itemError: {
                product_name: [],
                product_value: [],
                product_code: [],
                qty: [],
                free_qty:[],
                scheme_value:[]
            },
            productModal: false,
        }
    }

    componentDidMount() {
       
        document.addEventListener("keydown", this.escFunction, false);
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
    }

    componentWillMount() {
        this.getSalesman();
        this.getSupplierList();
    }

    fileReceived(file) {

        this.setState({ imageSrc: file, imageStatus: true });
        this.onModalClose();

    }

    escFunction(e) {


        //Alt+f
        if (e.altKey && e.which === 70) {
            // e.preventDefault();
            if (!this.state.isAdd && this.state.order.line_items.length > 0) {
                this.onOrderCreate(e)
            }

        }
        //Alt+i
        if (e.altKey && e.which === 73) {
            e.preventDefault();

        }
        //Alt+r
        if (e.altKey && e.which === 82) {
            e.preventDefault();

        }
        //Alt+b
        if (e.altKey && e.which === 66) {
            e.preventDefault();

        }

        //Alt+l
        if (e.altKey && e.which === 76) {
            e.preventDefault();
            this.onPrint(this.state.lastOrder);
        }

        //Alt+P
        if (e.altKey && e.which === 80) {
            e.preventDefault();
            this.setState({ productModal: true });
            //console.log("");
        }

    }


    getSalesman() {

        Axios.get(buildURL(Sales_ORDER_SALESMAN, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {

                    this.setSalesMan(mData.data);

                    //this.setProductData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    setSalesMan(salesmans) {

        let mProduct = [];   //this.state.product_items;

        for (let i = 0; i < salesmans.length; i++) {
            let item = {
                label: salesmans[i].name,
                value: salesmans[i].id,
                name: salesmans[i].product_code,
            }
            mProduct.push(item);
        }
        this.setState({ salesmanList: mProduct });
    }


    getSupplierList() {
        Axios.get(buildURL(SUPPLIER_LIST, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setSupplierData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                //this.errorAlert(error);
            });
    }

    loadProducts(value, callback) {
        Axios.get(buildURL(PRODUCT_SEARCH, { query: value }))
            .then((response) => {
                let product = response.data.data;
                let mProduct = [];
                for (let i = 0; i < product.length; i++) {
                    let item = {
                        label: product[i].product_name,
                        value: product[i].product_name,
                        name: product[i].product_code,
                    }
                    mProduct.push(item);
                }
                //console.log('mProduct',mProduct);
                let data = { options: mProduct }
                callback(null, data);
            })
            .catch((error) => {
                callback(null, [])
            })
    }

    resetItem() {
        this.inputSelect.focus();
        let mItem = this.state.item;
        Object.keys(mItem).forEach((key) => {
            mItem[key] = ""
        });
        this.setState({ item: mItem, selected_product: '' });
    }


    setSupplierData(supplier) {
        let mSupplier = this.state.supplier_items;
        for (let i = 0; i < supplier.length; i++) {
            let item = {
                label: supplier[i].supplier_name,
                value: supplier[i].supplier_code,
                name: supplier[i].supplier_name,
            }
            mSupplier.push(item);
        }
        this.setState({ supplier_list: supplier, supplier_items: mSupplier });
    }

    onHandleChange(e) {
        let mItem = this.state.item;
        mItem[e.target.name] = e.target.value;
        if (e.target.name === 'product_code') {
            mItem.product_name = e.target.options[e.target.selectedIndex].getAttribute('data-name');
        }
        if (e.target.name === 'supplier_code') {
            mItem.supplier_name = e.target.options[e.target.selectedIndex].getAttribute('data-name');
        }
        this.setState({ item: mItem });
    }



    onCameraClick(e) {

        this.setState({ modal_status: true });

    }

    onModalClose() {

        this.setState({ modal_status: false, imageModal: false, productModal: false });

    }

    resetError() {
        let mError = this.state.error;
        Object.keys(mError).forEach((key) => {
            mError[key] = [];
        });
        this.setState({ error: mError });
    }

    validate() {
        let mProduct = this.state.item;
        let mError = {
            product_name: [],
            qty: [],
            free_qty:[],
            scheme_value:[]
        };
        if (mProduct.product_code === "") {
            mError.product_name.push("Product name cannot be empty");
        }
        if (mProduct.free_qty === "") {
            mError.free_qty.push("Free cannot be empty");
        }
        if (mProduct.scheme_value === "") {
            mError.scheme_value.push("Scheme  cannot be empty");
        }
        if (mProduct.qty === "") {
            mError['qty'].push("Quantity cannot be empty");
        }
        else if (parseInt(mProduct.qty) <= 0) {
            mError.qty.push("Quantity must be greater than zero");
        }
        return mError;
    }

    onNewProduct(e) {
        e.preventDefault();
        this.setState({ productModal: true });
    }
    onSave(e) {
        e.preventDefault();

        this.resetError();

        let mError = this.validate();
        if (mError.product_name.length > 0 || mError.qty.length > 0) {
            this.setState({ itemError: mError });
            return false;
        } else {
            this.setState({ itemError: { product_name: [], qty: [],free_qty:[],scheme_value:[] } });
        }

        let mOrder = this.state.order;
        let mItem = this.state.item;
        let itemList = this.state.order.line_items;

        for (let i = 0; i < itemList.length; i++) {
            if (mItem.product_code === itemList[i].product_code) {
                window.alert("Item already exist")
                return false;
            }
        }

        let item_temp = {
            supplier_code: '',
            supplier_name: '',
            product_code: '',
            product_name: '',
            qty: '',
            free_qty:'',
            scheme_value:''
        };
        Object.keys(item_temp).forEach((key) => {
            
            if(key == 'free_qty')
            {
                if(typeof mItem[key] =='undefined' || mItem[key] == '')
                {
                    item_temp[key] = 0;
                }else{
                    item_temp[key] = mItem[key];
                }

            }else{
                item_temp[key] = mItem[key];
            }
            
        });

        if (item_temp.qty == "" || item_temp.product_name == "") {
            window.alert("Product Name and Quantity can't be empty!");
            return false;
        }

        mOrder.line_items.push(item_temp)

        this.setState({ order: mOrder, selected_product: '' }, () => this.resetItem());
    }

    onItemEdit(e, item, index) {
        e.preventDefault();
        // let mItem = this.state.item;
        // let mIndex = this.state.item_index;
        // mItem = item;
        // mIndex = index;
        let mSelectedProduct = {
            label: item.product_name,
            value: item.product_name,
            name: item.product_code,
        }
        const mItem = item;
        let mNew = {};
        Object.keys(mItem).forEach((key) => {
            mNew[key] = mItem[key];
        })

        this.setState({ item: mNew, selected_product: mSelectedProduct, item_index: index, update_item: item.product_code, isUpdate: true });
    }

    
    onUpdate(e) {
        e.preventDefault();
        let mOrder = this.state.order;
        let mItem = this.state.item;
        let itemList = this.state.order.line_items;

        
        // let error = this.validateItem(mItem);
        // Object.keys(error).forEach((key)=>{
        //     if(error[key].length>0){
        //         console.log(error[key].length)
        //         return false;
        //     }
        // })

        for (let i = 0; i < itemList.length; i++) {
            if (this.state.update_item !== mItem.product_code && mItem.product_code === itemList[i].product_code) {
                window.alert("Item already exist")
                return false;
            }
        }

        let item_temp = {
            supplier_code: '',
            supplier_name: '',
            product_code: '',
            product_name: '',
            qty: '',
            free_qty:'',
            scheme_value:''
        };
        Object.keys(item_temp).forEach((key) => {
            
            item_temp[key] = mItem[key];
            
        });

        mOrder.line_items[this.state.item_index] = item_temp;

        if (item_temp.qty == "" || item_temp.product_name == "") {
            window.alert("Product Name and Quantity can't be empty!");
            return false;
        }

        this.setState({ order: mOrder, isUpdate: false }, () => this.resetItem());

    }

    resetData() {
      
        let nobj={};
        let mOrder = this.state.order;
        Object.keys(mOrder).forEach((key) => {
            if (key === 'line_items') {
                nobj[key] = [];
            }else{
                nobj[key] = "";
            }
        });

        nobj["line_items"]=[];
           

        this.setState({ order: nobj, imageSrc: '', imageStatus: false, selected_salesman: '',selected_supplier:{
            value: '',
            label: '',
            name: '',
        } },()=>{
             
            this.orderSuccess.focus();
        });
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }


    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((key) => {
            mError[key] = error[key];
        });
        this.setState({ error: mError });
    }


    onOrderCreate(e) {
        e.preventDefault();
        this.setState({ isAdd: true });
        let mOrder = {
      
            supplier_id: this.state.selected_supplier.value,
            supplier_name:this.state.selected_supplier.name,
            company_representative: this.state.order.company_representative,
            order_status:this.state.order.order_status,
            mobile_number: this.state.order.mobile_number,
            line_items: this.state.order.line_items,
            remarks: this.state.order.remarks,
            salesman_id: this.state.order.salesman_id,
            signature:this.state.imageSrc
        }
        this.resetError();

        Axios.post(buildURL(SUPPLIER_ORDER_CREATE), mOrder)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert('Order created successully');
                   
                    //this.onPrint(mData.data);
                    this.setState({ isAdd: false, lastOrder: mData.data },()=>{
                        this.resetData();

                    });
                    //this.props.history.push('/sales/order/create');
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false, isAdd: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isAdd: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isAdd: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false, isAdd: false });
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error)
                    this.setState({ progressStatus: false, isAdd: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false, isAdd: false });
            });
       
        console.log("Call",mOrder);
    }



    onModalClose() {

        this.setState({ modal_status: false, imageModal: false, productModal: false });

    }

    onItemDelete(e, index) {
        e.preventDefault();
        let mOrder = this.state.order;

        let mFilter = mOrder.line_items.filter((item, mIndex) => {
            if (index !== mIndex) {
                return item;
            }
        });
        mOrder.line_items = mFilter;
        this.setState({ order: mOrder, isUpdate: false }, () => this.resetItem());
    }


    onCustomerChange(e) {
        const re = /^[0-9\b]+$/;
        let mItem = this.state.order;
        if (e.target.name === 'mobile_number' || e.target.name === 'telephone') {
            if (e.target.value === '' || re.test(e.target.value)) {
                mItem[e.target.name] = e.target.value;
            }
        } else {
            mItem[e.target.name] = e.target.value.toUpperCase();
        }
        this.setState({ order: mItem });
    }

    onSelectDataChange(name, value) {
        if (name === 'product_name') {
            let mItem = this.state.item;
            if (value !== null) {
                mItem.product_code = value.name;
                mItem.product_name = value.label;
                mItem.product_value = value.label;
            } else {
                mItem.product_name = '';
                mItem.product_code = '';
            }
            this.setState({ item: mItem, selected_product: value });
        } else if (name === 'supplier_name') {

            console.log("supplier onchange",value)
            let mItem = this.state.selectedSupplier;
            if (value !== null) {
                mItem.supplier_code = value.label;
                mItem.supplier_name = value.name;
                //mItem.product_value = value.label;
            } else {
                mItem.supplier_name = '';
                mItem.supplier_code = '';
            }
            this.setState({ item: mItem, selected_supplier: value });
        } else if (name === 'salesman_id') {
            let mOrder = this.state.order;
            if (value !== null) {
                mOrder.salesman_id = value.value;

                //mItem.product_value = value.label;
            } else {
                mOrder.salesman_id
            }
            this.setState({ order: mOrder, selected_salesman: value });
        }
    }

   
    render() {
        return (
            <div>
                <div className='row'>
                    {this.renderCusomerForm()}
                </div>
                <div className='row'>
                    {this.renderItemsForm()}
                </div>
                <div className='row'>
                    {
                        this.state.order.line_items.length > 0 ?
                            this.renderItems() : ""
                    }
                </div>
                {
                    this.state.imageModal ? <ImageModal image={this.state.imageSrc} isDelete={this.state.imageDelete} onDeleteImage={() => this.onDeleteImage()} onClose={() => this.onModalClose()} /> : ""
                }
                {
                    this.state.productModal ? <NewProductModal onClose={() => this.onModalClose()} /> : ""
                }
            </div>
        )
    }

    renderCusomerForm() {

        return (
            <div className='box no-border'>
                <div className='box-body'>
                    <div className='row'>

                        <div className='col-md-2'>
                            <div className={this.state.error.mobile_number.length > 0 ? 'form-group has-error' : 'form-group'}>
                                <label>Supplier Name</label>
                                <Select
                                ref={(node) => this.supplierInputSelect = node}
                                name="supplier"
                                value={this.state.selected_supplier}
                                onChange={(data) => this.onSelectDataChange("supplier_name", data)}
                                options={this.state.supplier_items}
                                defaultOptions
                                cacheOptions
                            />
                                <div className='help-block'>{this.state.error.supplier_id.length > 0 ? this.state.error.supplier_id : ""}</div>
                            </div>
                        </div>

                        <div className='col-md-2'>
                            <div className={this.state.error.mobile_number.length > 0 ? 'form-group has-error' : 'form-group'}>
                                <label>Mobile Number</label>
                                <input ref={(node) => this.orderSuccess = node} maxLength={10} className='form-control' type='text' name='mobile_number' value={this.state.order.mobile_number} onChange={(e) => this.onCustomerChange(e)} />
                                <div className='help-block'>{this.state.error.mobile_number.length > 0 ? this.state.error.mobile_number : ""}</div>
                            </div>
                        </div>
                        
                        <div className='col-md-2'>
                            <div className={this.state.error.company_representative.length > 0 ? 'form-group has-error' : 'form-group'}>
                                <label>Company Representative</label>
                                <input className='form-control' style={{ textTransform: 'uppercase' }} name='company_representative' value={this.state.order.company_representative} onChange={(e) => this.onCustomerChange(e)} />
                                <div className='help-block'>{this.state.error.company_representative.length > 0 ? this.state.company_representative : ""}</div>
                            </div>
                        </div>

                        <div className='col-md-2'>
                            <div className={this.state.error.salesman_id.length > 0 ? 'form-group has-error' : 'form-group'}>
                                <label>Sales Representative</label>
                                <Select
                                    ref={(node) => this.SalesinputSelect = node}
                                    name="salesman_id"
                                    value={this.state.selected_salesman}
                                    onChange={(data) => this.onSelectDataChange("salesman_id", data)}
                                    options={this.state.salesmanList}
                                />
                                <div className='help-block'>{this.state.error.salesman_id.length > 0 ? this.state.error.salesman_id : ""}</div>
                            </div>
                        </div>

                        <div className='col-md-2'>
                            <div className={this.state.error.remarks.length > 0 ? 'form-group has-error' : 'form-group'}>
                                <label>Remarks</label>
                                <textarea style={{ textTransform: 'uppercase' }} className='form-control' name='remarks' value={this.state.order.remarks} onChange={(e) => this.onCustomerChange(e)} />

                                <div className='help-block'>{this.state.error.remarks.length > 0 ? this.state.error.remarks : ""}</div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                
                        {this.state.imageStatus ?
                            <div className='col-md-2 col-md-offset-8'>
                                <img style={{ height: '100px', width: '200px' }} src={this.state.imageSrc} alt='' onClick={(e) => this.onImageClick(e)} />
                            </div>
                            :
                            ""
                        }
                        <div className='col-md-2 pull-right ' >
                            <input
                                type="file"
                                ref={input => {
                                    this.fileInput = input;
                                }}
                                style={{ display: "none" }}
                                onChange={(e) => this.HandleFileInput(e)}

                                accept="image/*"
                            />
                            <div className='form-group'>
                                <label>Add Signature</label>
                                <div>
                                  
                                    <button className='btn btn-info' onClick={(e) => this.onCameraClick(e)}><i className='fa fa-camera' />

                                    </button>
                                    <img src={this.state.image} alt='' />
                                </div>

                                {this.state.modal_status ? <SignatureModal onReceived={(file) => this.fileReceived(file)} onClose={() => this.onModalClose()} /> : ""}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderItemsForm() {

        
        let supplier = this.state.supplier_list.map((item, index) => {
            return (
                <option key={index} data-name={item.supplier_name} value={item.id}>{item.supplier_code}</option>
            )
        })

        return (

            <div className='box no-border'>
                <div className='box-body'>
                    <div className='col-md-4'>
                        <div className={this.state.itemError.product_name.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Product Name</label>
                            <Select.Async
                                ref={(node) => this.inputSelect = node}
                                name="product_name"
                                value={this.state.selected_product}
                                onChange={(data) => this.onSelectDataChange("product_name", data)}
                                loadOptions={this.loadProducts}
                                defaultOptions
                                cacheOptions
                            />
                            {/* <select className='form-control' name='product_code' value={this.state.item.product_code} onChange={(e) => this.onHandleChange(e)} >
                                <option value=''>--Select--</option>
                                {product}
                            </select> */}
                            <div className='help-block'>{this.state.itemError.product_name.length > 0 ? this.state.itemError.product_name : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-1'>
                        <div className={this.state.itemError.qty.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Quantity</label>
                            <input className='form-control' type='number' name='qty' value={this.state.item.qty} onChange={(e) => this.onHandleChange(e)} required />
                            <div className='help-block'>{this.state.itemError.qty.length > 0 ? this.state.itemError.qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-1'>
                        <div className={this.state.itemError.free_qty.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Free</label>
                            <input className='form-control' type='number' name='free_qty' value={this.state.item.free_qty} onChange={(e) => this.onHandleChange(e)} required />
                            <div className='help-block'>{this.state.itemError.free_qty.length > 0 ? this.state.itemError.free_qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.itemError.scheme_value.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Scheme</label>
                            <input className='form-control' type='text' name='scheme_value' value={this.state.item.scheme_value} onChange={(e) => this.onHandleChange(e)} required />
                            <div className='help-block'>{this.state.itemError.scheme_value.length > 0 ? this.state.itemError.scheme_value : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        {
                            this.state.isUpdate ?
                                <button className='btn btn-primary col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onUpdate(e)}>Update</button>
                                :
                                <button className='btn btn-success col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onSave(e)}>Add</button>
                        }

                    </div>

                    <div className='col-md-2'>
                        <button className='btn btn-warning col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onNewProduct(e)}>Add New Product (Alt+P)</button>
                    </div>

                    {/* <div className='col-md-2 pull-right'>
                        {
                            this.state.lastOrder != "" && this.state.lastOrder != null ?
                                <button className='btn btn-primary pull-right col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onPrint(this.state.lastOrder)}>Last Order Print(Alt+L)</button>
                                :
                                ""
                        }

                    </div> */}
                </div>
            </div>
        )
    }

    renderItems() {

        let items = this.state.order.line_items.map((item, index) => {
            return (
                <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.product_name}</td>
                    <td>{item.qty}</td>
                    <td>{item.free_qty}</td>
                    <td>{item.scheme_value}</td>
                    <td>
                        {
                            <div>
                                <a href="" onClick={(e) => this.onItemEdit(e, item, index)} ><span className='fa fa-pencil'></span> </a>
                                <a href="" onClick={(e) => this.onItemDelete(e, index)} style={{ marginLeft: '10px' }} ><span className='fa fa-trash'></span> </a>
                            </div>
                        }
                    </td>
                </tr>
            )
        })

        return (
            <div className='box no-border'>
                <div className='box-header with-border'>
                    <h3 className='box-title'>Order Items</h3>
                    <button className='btn btn-success pull-right col-md-1' disabled={this.state.isAdd} onClick={(e) => this.onOrderCreate(e)} >Save (Alt + F)</button>
                </div>
                <div className='box-body table-responsive no-padding'>
                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Free</th>
                                <th>Scheme</th>
                                <th>Acition</th>
                            </tr>
                        </thead>
                        <tbody>{items}</tbody>
                    </table>
                </div>
            </div>
        )
    }
}