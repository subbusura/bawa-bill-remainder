import React from 'react';

export default class SupplierOrderViewModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isShow: true,
            order: {
                name: '',
                mobile_number: '',
                advance_payment: '',
                items: [],
                image: '',
                order_no: '',
                date: '',
            },
        }
    }

    componentDidMount() {
        if (this.props.order !== null) {
            this.setState({ order: this.props.order });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.order !== null) {
            this.setState({ order: nextProps.order });
        }
    }

    onClose(e) {
        e.preventDefault();
        this.setState({ isShow: false });
        this.props.onClose();
    }

    render() {

        return (
            <div>
                <div className={this.state.isShow ? 'modal fade in' : 'modal fade'} style={this.state.isShow ? { display: 'block', paddingRight: '16px' } : { display: 'none' }} >

                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">

                                <button type="button" className="close" onClick={(e) => this.onClose(e)} data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 className="modal-title">Order Detail</h4>

                            </div>
                            <div className="modal-body" style={{ maxHeight: "450px", overflow: 'auto' }} >
                                <div className='row'>

                                    {this.renderItem('Order Number', this.state.order.id)}
                                    {this.renderItem('Supplier Name', this.state.order.supplier_name)}
                                    {this.renderItem('Phone Number', this.state.order.mobile_number)}
                                    {this.renderItem('Company Representative', this.state.order.company_representative)}
                                    {this.renderItem('Ordered Date', this.state.order.created_at)}
                                    

                                    {/* <div className='col-md-4'>
                                        <h4 style={{ fontSize: '14px' }} >Order Number</h4>
                                        <label>{this.state.order.id}</label>
                                    </div> */}

                                    {/* <h3 className='col-md-6' style={{ fontSize: '14px', marginTop: '-3px' }} >Order Number : {this.state.order.id}</h3>
                                    <h3 className='col-md-6' style={{ fontSize: '14px', marginTop: '-3px' }} >Name : {this.state.order.name}</h3>
                                    <h3 className='col-md-6' style={{ fontSize: '14px', marginTop: '-3px' }} >Phone Number : {this.state.order.mobile_number}</h3>
                                    <h3 className='col-md-6' style={{ fontSize: '14px', marginTop: '-3px' }} >Advance Payment : {this.state.order.advance_payment}</h3>
                                    <h3 className='col-md-6' style={{ fontSize: '14px', marginTop: '-3px' }} >Ordered Date : {this.state.order.created_at}</h3>
                                    <h3 className='col-md-6' style={{ fontSize: '14px', marginTop: '-3px' }} >Updated Date : {this.state.order.updated_at}</h3> */}

                                </div>
                                <div className='row'>
                                    <br />
                                    <div className='col-md-12' style={{ minHeight: '180px', maxHeight: '180px' }}>
                                        {this.renderTable()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )

    }

    renderTable() {
        let items = this.state.order.items.map((item, index) => {
            return (
                <tr key={index} >
                    <td>{index + 1}</td>
                    <td>{item.product_code}</td>
                    <td>{item.product_name}</td>
                    <td>{item.qty}</td>
                    <td>{item.free_qty}</td>
                    <td>{item.scheme_value}</td>
                    <td>{item.remarks}</td>
                </tr>
            )
        })
        return (
            <table className='table table-striped table-bordered' style={{ maxHeight: '10px' }}  >
                <thead>
                    <tr className={"table-header-color"} >
                        <th >S.no</th>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th>Free Quantity</th>
                        <th>Scheme</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                <tbody>{items}</tbody>
            </table>
        )
    }

    renderItem(title, item) {
        return (
            <div className='col-md-4'>
                <h4 style={{ fontSize: '14px' }} >{title}</h4>
                <label>{item}</label>
            </div>
        )
    }


}