import React from 'react';
import Webcam from 'react-webcam';
import SignaturePad from 'react-signature-pad-wrapper'

export default class SignatureModal extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            isShow: true,
            order: {
                name: '',
                mobile_number: '',
                advance_payment: '',
                items: [],
                image: '',
                order_no: '',
                date: '',
            },
        }
    }

    setRef = (webcam) => {

        this.webcam = webcam;
    }

    onCameraClick() {

        //const imageSrc = this.webcam.getScreenshot();

        if(!this.signaturePad.isEmpty())
        {
            this.props.onReceived(this.signaturePad.toDataURL());
        }

    }

     download(dataURL, filename) {
        if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
          window.open(dataURL);
        } else {
          var blob = this.dataURLToBlob(dataURL);
          var url = window.URL.createObjectURL(blob);
      
          var a = document.createElement("a");
          a.style = "display: none";
          a.href = url;
          a.download = filename;
      
          document.body.appendChild(a);
          a.click();
      
          window.URL.revokeObjectURL(url);
        }
      }
      
       dataURLToBlob(dataURL) {
        // Code taken from https://github.com/ebidel/filer.js
        var parts = dataURL.split(';base64,');
        var contentType = parts[0].split(":")[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);
      
        for (var i = 0; i < rawLength; ++i) {
          uInt8Array[i] = raw.charCodeAt(i);
        }
      
        return new Blob([uInt8Array], { type: contentType });
      }

    componentDidMount() {

   

        if (this.props.order !== null) {
            this.setState({ order: this.props.order });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.order !== null) {
            this.setState({ order: nextProps.order });
        }
    }

    onClose(e) {
        e.preventDefault();
        this.setState({ isShow: false },()=>{
            this.props.onClose();
        });
      
    }

    render() {

        return (
            <div>
                <div className={this.state.isShow ? 'modal fade in' : 'modal fade'} style={this.state.isShow ? { display: 'block', paddingRight: '16px' } : { display: 'none' }} >

                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">

                                <button type="button" className="close" onClick={(e) => this.onClose(e)} data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 className="modal-title">Sign Below</h4>

                            </div>
                            <div className="modal-body" style={{ maxHeight: "450px", overflow: 'auto' }} >

                                <SignaturePad  height={250} options={{minWidth: 5, maxWidth: 10, penColor: 'rgb(66, 133, 244)'}}  ref={ref => this.signaturePad = ref} />

                                <button className='btn btn-info' onClick={(e) => this.onCameraClick(e)}><i className='fa fa-camera' />

                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )

    }

}