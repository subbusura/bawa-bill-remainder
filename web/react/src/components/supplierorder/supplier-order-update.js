import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';
import Select, { Async } from 'react-select';
import { buildURL,SUPPLIER_ORDER_VIEW, SUPPLIER_ORDER_ITEM_ADD,SUPPLIER_ORDER_UPDATE,Sales_ORDER_SALESMAN, SUPPLIER_LIST, PRODUCT_LIST, SALES_ORDER_UPDATE, PRODUCT_SEARCH } from './../../constants/api';
import {_DELIVERY_TYPE } from './../../constants/global';
import SignatureModal from './SignatureModal';
import SalesOrderItems from './supplier-order-items';
import ImageModal from './../orders/imageModal';

export default class SupplierOrderUpdate extends React.Component {

    constructor(props) {
        super(props);
        this.shortKey = this.shortKey.bind(this);
        let params = this.props.match.params;
        this.state = {
            modal_status: false,
            order: {
                supplier_id: '',
                supplier_name: '',
                mobile_number: '',
                advance_payment: '',
                items: [],
                image: '',
                image_path: '',
                signature: '',
                remarks: "",
                salesman_id: "",
                company_representative: '',
                order_status:10,
            },
            item: {
                product_code: '',
                product_value: '',
                product_name: '',
                qty: '',
                free_qty:'',
                scheme_value:''
            },
            error: {
                supplier_id: [],
                supplier_name: [],
                mobile_number: [],
                company_representative: [],
                order_status:[],
                line_items: [],
                remarks:[],
                salesman_id:[],
            },
            empty: {
                supplier_id: [],
                supplier_name: [],
                mobile_number: [],
                company_representative: [],
                order_status:[],
                line_items: [],
                remarks:[],
                salesman_id:[],
            },
            item_index: '',
            isUpdate: false,
            order_id: params.id,
            product_list: [],
            supplier_list: [],
            product_items: [],
            supplier_items: [],
            selected_product: '',
            selected_supplier: '',
            imageStatus: false,
            imageSrc: '',
            imageModal: false,
            salesmanList: [],
            selected_salesman: "",
            isUpdate: true,
            isItemUpdate: true,
            itemError: {
                product_name: [],
                product_value: [],
                product_code: [],
                qty: [],
                free_qty:[],
                scheme_value:[]
            },
            imageDelete: true,
        }
    }

    componentDidMount() {
        this.getOrder();
        document.addEventListener("keydown", this.shortKey, false);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.shortKey, false);
    }

    componentWillMount() {
        //this.getProductList();
        this.getSupplierList();
        this.getSalesman();
    }

    shortKey(e) {

        if (e.altKey && e.which === 70) {
            if (this.state.isUpdate) {
                this.onUpdateOrder(e)
            }
        }
    }

    onClickImage(e) {

        this.fileInput.click();
    }

    getSalesman() {

        Axios.get(buildURL(Sales_ORDER_SALESMAN, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {

                    this.setSalesMan(mData.data);

                    //this.setProductData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }


    onModalClose() {
        console.log("onModalClose","clickd",this.state.modal_status);
        this.setState({ modal_status: false, imageModal: false });

    }
    setSalesMan(salesmans) {

        let mProduct = [];   //this.state.product_items;

        for (let i = 0; i < salesmans.length; i++) {
            let item = {
                label: salesmans[i].name,
                value: salesmans[i].id,
                name: salesmans[i].product_code,
            }
            mProduct.push(item);
        }
        this.setState({ salesmanList: mProduct });
    }

    onCameraClick(e) {

        console.log("onCameraClick","clickd",this.state.modal_status);
        this.setState({ modal_status: true });
    }

   
    HandleFileInput(e) {
        let file = this.fileInput.files;
        let mImage = '';
        let files = [];
        for (var i = 0; i < file.length; i++) {
            this.getBase64(file[i], (mfile) => this.fileReceived(mfile));
        }
    }

    getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
        };
    }

    fileReceived(file) {

        this.onModalClose();
        this.setState({ imageSrc: file, imageStatus: true });
    }

    getProductList() {
        Axios.get(buildURL(PRODUCT_LIST, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setProductData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    getSupplierList() {
        Axios.get(buildURL(SUPPLIER_LIST, { page: 0, 'per-page': 0 }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setSupplierData(mData.data.items);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    setProductData(product) {
        let mProduct = this.state.product_items;

        for (let i = 0; i < product.length; i++) {
            let item = {
                label: product[i].product_name,
                value: product[i].product_name,
                name: product[i].product_code,
            }
            mProduct.push(item);
        }
        this.setState({ product_list: product, product_items: mProduct });
    }

    setSupplierData(supplier) {
        let mSupplier = this.state.supplier_items;

        for (let i = 0; i < supplier.length; i++) {
            let item = {
                label: supplier[i].supplier_name,
                value: supplier[i].supplier_code,
                name: supplier[i].supplier_name,
            }
            mSupplier.push(item);
        }
        this.setState({ supplier_list: supplier, supplier_items: mSupplier });
    }

    onSelectDataChange(name, value) {
        if (name === 'product_name') {
            let mItem = this.state.item;
            if (value !== null) {
                mItem.product_code = value.name;
                mItem.product_name = value.label;
                mItem.product_value = value.label;
            } else {
                mItem.product_name = '';
                mItem.product_code = '';
            }
            this.setState({ item: mItem, selected_product: value });
        } else if (name === 'supplier_name') {
            let mItem = this.state.item;
            if (value !== null) {
                mItem.supplier_code = value.name;
                mItem.supplier_name = value.label;
                //mItem.product_value = value.label;
            } else {
                mItem.supplier_name = '';
                mItem.supplier_code = '';
            }
            this.setState({ item: mItem, selected_supplier: value });
        }
        else if (name === 'salesman_id') {
            let mOrder = this.state.order;
            if (value !== null) {
                mOrder.salesman_id = value.value;

                //mItem.product_value = value.label;
            } else {
                mOrder.salesman_id
            }
            this.setState({ order: mOrder, selected_salesman: value });
        }
    }

    getOrder() {
        Axios.get(buildURL(SUPPLIER_ORDER_VIEW, { order_id: this.state.order_id, expand: 'items, salesMan' }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setOrder(mData.data)
                    //this.setState({ order: mData.data, imageSrc: mData.data.image_path });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
            });
    }

    setOrder(order) {
        let mImage = this.state.imageSrc;
        let status = false;
        let mSalesMan = this.state.selected_salesman;
        let mSupplier = this.state.selected_supplier;
        Object.keys(order).forEach((key) => {
            if (key === 'signature') {
                if (order[key] !== null && order[key] !== "") {
                    mImage = order[key];
                    status = true
                }
            } else if (key === 'salesMan') {
                if (order[key] !== null) {
                    mSalesMan = {
                        value: order[key].id,
                        label: order[key].name,
                    }
                }
            }else if(key === 'supplier_name'){
                mSupplier={
                    value: order.supplier_id,
                    label: order.supplier_name,
                }
            }
        })

    this.setState({ order: order, imageSrc: mImage, imageStatus: status, selected_supplier:mSupplier,selected_salesman: mSalesMan });
    }

    onSave(e) {
        e.preventDefault();

        let mError = this.validate();
        if (mError.product_name.length > 0 || mError.qty.length > 0) {
            this.setState({ itemError: mError });
            return false;
        } else {
            this.setState({ itemError: { product_name: [], qty: [],free_qty:[],scheme_value:[] } });
        }

        let mItem = this.state.item;
        let itemList = this.state.order.items;
        for (let i = 0; i < itemList.length; i++) {
            if (mItem.product_code === itemList[i].product_code) {
                window.alert("Item already exist")
                return false;
            }
        }

        Axios.post(buildURL(SUPPLIER_ORDER_ITEM_ADD, { order_id: this.state.order_id }), this.state.item)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert('Added successully');
                    this.resetItem()
                    this.getOrder();
                    //window.location = '/order'
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    validate() {
        let mProduct = this.state.item;
        let mError = {
            product_name: [],
            qty: [],
            free_qty:[],
            scheme_value:[]
        };
        if (mProduct.product_code === "") {
            mError.product_name.push("Product name cannot be empty");
        }
        if (mProduct.qty === "") {
            mError['qty'].push("Quantity cannot be empty");
        }
        else if (parseInt(mProduct.qty) <= 0) {
            mError.qty.push("Quantity must be greater than zero");
        }
        return mError;
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    resetItem() {
        this.inputSelect.focus();
        let mItem = this.state.item;
        Object.keys(mItem).forEach((key) => {
            mItem[key] = "";
        })
        this.setState({ item: mItem, selected_product: '' });
    }

    onDeliveryTypeChange(e){
        
                let mItem = this.state.order;
                mItem[e.target.name] = e.target.value;
                this.setState({ order: mItem });
            }

    onCustomerChange(e) {
        const re = /^[0-9\b]+$/;
        let mItem = this.state.order;
        if (e.target.name === 'mobile_number' || e.target.name === 'telephone') {
            if (e.target.value === '' || re.test(e.target.value)) {
                mItem[e.target.name] = e.target.value;
            }
        } else {
            mItem[e.target.name] = e.target.value.toUpperCase();
        }
        this.setState({ order: mItem });
    }

    onHandleChange(e) {
        let mItem = this.state.item;
        mItem[e.target.name] = e.target.value;
        if (e.target.name === 'product_code') {
            mItem.product_name = e.target.options[e.target.selectedIndex].getAttribute('data-name');
        }
        if (e.target.name === 'supplier_code') {
            mItem.supplier_name = e.target.options[e.target.selectedIndex].getAttribute('data-name');
        }
        this.setState({ item: mItem });
    }

    onUpdateOrder(e) {
      //  this.setState({ isUpdate: false });
        e.preventDefault();
        let item = {
            supplier_id: '',
            supplier_name: '',
            mobile_number: '',
            company_representative: '',
            image_path: '',
            salesman_id: '',
            remarks: '',

        };
        Object.keys(item).forEach((key) => {
            item[key] = this.state.order[key];
        })

        item.supplier_id=this.state.selected_supplier.value,
        item.supplier_name=this.state.selected_supplier.label,
        item.signature = this.state.imageSrc;

        Axios.post(buildURL(SUPPLIER_ORDER_UPDATE, { order_id: this.state.order_id }), item)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert('Updated successully');
                    this.setState({ isUpdate: true });
                    this.props.history.push('/sorder/order/');
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false, isUpdate: true });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isUpdate: true });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isUpdate: true });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false, isUpdate: true });
                }
            })
            .catch((error) => {
                this.setState({ progressStatus: false, isUpdate: true });
                this.errorAlert(error);
            })
    }

    loadProducts(value, callback) {
        Axios.get(buildURL(PRODUCT_SEARCH, { query: value }))
            .then((response) => {
                let product = response.data.data;
                let mProduct = [];
                for (let i = 0; i < product.length; i++) {
                    let item = {
                        label: product[i].product_name,
                        value: product[i].product_name,
                        name: product[i].product_code,
                    }
                    mProduct.push(item);
                }
                let data = { options: mProduct }
                callback(null, data);
            })
            .catch((error) => {
                callback(null, [])
            })
    }

    onItemAddSuccess(item) {
        let order = this.state.order;
        order.items.push(item);
        this.setState({ order: order });
    }

    onItemDelete(id) {
        let order = this.state.order;
        let filter = order.items.filter((item, index) => {
            if (id !== item.id) {
                return item;
            }
        })
        order.items = filter;
        this.setState({ order: order });
    }

    onImageClick(e) {
        e.preventDefault();
        this.setState({ imageModal: true });
    }

    onDeleteImage() {
        this.setState({ imageSrc: '', imageModal: false, imageStatus: false })
    }



    onCheckChange(e) {
        let mOrder = this.state.order;
        mOrder[e.target.name] = !mOrder[e.target.name];
        this.setState({ order: mOrder });
    }

    render() {
        return (
            <div>
                <div className='row'>
                    {this.renderCusomerForm()}
                </div>
                <div className='row'>
                    {this.renderItemsForm()}
                </div>
                <div className='row'>
                    {
                        this.state.order.items.length > 0 ?
                            this.renderItems() : ""
                    }
                </div>
                {
                    this.state.imageModal ? <ImageModal image={this.state.imageSrc} isDelete={this.state.imageDelete} onDeleteImage={() => this.onDeleteImage()} onClose={() => this.onModalClose()} /> : ""
                }
            </div>
        )
    }

    renderCusomerForm() {

        let deliveryType = _DELIVERY_TYPE.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })


        return (
            <div className='box no-border'>
                <div className='box-body'>
                    <div>
                        <div className='row'>
                            <div className='col-md-2'>
                                <div className={this.state.error.mobile_number.length > 0 ? 'form-group has-error' : 'form-group'}>
                                    <label>Supplier Name</label>
                                    <Select
                                    ref={(node) => this.supplierInputSelect = node}
                                    name="supplier"
                                    value={this.state.selected_supplier}
                                    onChange={(data) => this.onSelectDataChange("supplier_name", data)}
                                    options={this.state.supplier_items}
                                    defaultOptions
                                    cacheOptions
                                />
                                    <div className='help-block'>{this.state.error.supplier_id.length > 0 ? this.state.error.supplier_id : ""}</div>
                                </div>
                            </div>
                            <div className='col-md-2'>
                                <div className='form-group'>
                                    <label>Phone Number</label>
                                    <input className='form-control' name='mobile_number' maxLength={10} type='text' value={this.state.order.mobile_number} onChange={(e) => this.onCustomerChange(e)} />
                                </div>
                            </div>
                            <div className='col-md-2'>
                            <div className={this.state.error.company_representative.length > 0 ? 'form-group has-error' : 'form-group'}>
                                <label>Company Representative</label>
                                <input className='form-control' style={{ textTransform: 'uppercase' }} name='company_representative' value={this.state.order.company_representative} onChange={(e) => this.onCustomerChange(e)} />
                                <div className='help-block'>{this.state.error.company_representative.length > 0 ? this.state.company_representative : ""}</div>
                             </div>
                            </div>
                           
                            <div className='col-md-2'>
                                <div className={this.state.error.salesman_id.length > 0 ? 'form-group has-error' : 'form-group'}>
                                    <label>Sales Representative</label>
                                    <Select
                                        ref={(node) => this.SalesinputSelect = node}
                                        name="salesman_id"
                                        value={this.state.selected_salesman}
                                        onChange={(data) => this.onSelectDataChange("salesman_id", data)}
                                        options={this.state.salesmanList}
                                    />
                                    <div className='help-block'>{this.state.error.salesman_id.length > 0 ? this.state.error.salesman_id : ""}</div>
                                </div>
                            </div>
                            <div className='col-md-2'>
                                <div className={this.state.error.remarks.length > 0 ? 'form-group has-error' : 'form-group'}>
                                    <label>Remarks</label>
                                    <textarea style={{ textTransform: 'uppercase' }} className='form-control' name='remarks' value={this.state.order.remarks} onChange={(e) => this.onCustomerChange(e)} />

                                    <div className='help-block'>{this.state.error.remarks.length > 0 ? this.state.error.remarks : ""}</div>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
    
                            {this.state.imageSrc != "" ?
                                <div className='col-md-2 col-md-offset-8'>
                                    <img style={{ height: '100px', width: '200px' }} src={this.state.imageSrc} alt='' onClick={(e) => this.onImageClick(e)} />
                                </div>
                                :
                                ""
                            }
                            <div className='col-md-2 pull-right'>
                                <input
                                    type="file"
                                    ref={input => {
                                        this.fileInput = input;
                                    }}
                                    style={{ display: "none" }}
                                    onChange={(e) => this.HandleFileInput(e)}

                                    accept="image/*"
                                />
                               <div className='form-group'>
                                <label>Update Signature</label>
                                <div>
                                  
                                    <button className='btn btn-info' onClick={(e) => this.onCameraClick(e)}><i className='fa fa-camera' />

                                    </button>
                                    <img src={this.state.image} alt='' />
                                </div>

                                {this.state.modal_status ? <SignatureModal onReceived={(file) => this.fileReceived(file)} onClose={() => this.onModalClose()} /> : ""}

                            </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-12'>
                        <button className='btn btn-info pull-right col-md-1' disabled={!this.state.isUpdate} onClick={(e) => this.onUpdateOrder(e)} >Update</button>
                    </div>
                </div>
            </div>
        )
    }

    renderItems() {

        return (
            <div className='col-md-10 col-md-offset-1'>
                <div className='box no-border'>
                    <div className='box-body with-border'>
                        <h3 className='box-title'>Order Items</h3>
                    </div>
                    <div className='box-body no-padding'>
                        {
                            this.state.order.items.map((item, index) => {
                                return (
                                    <div key={index} >
                                        <SalesOrderItems
                                            items={item}
                                            orderId={this.state.order_id}
                                            product={this.state.product_list}
                                            supplier={this.state.supplier_list}
                                            onDeleteSuccess={(id) => this.onItemDelete(id)}
                                        />
                                        <hr style={{ marginTop: '1px', marginBottom: '1px' }} />
                                    </div>

                                )
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }

    renderItemsForm() {

        let product = this.state.product_list.map((item, index) => {
            return (
                <option key={index} data-name={item.product_name} value={item.id}>{item.product_code}</option>
            )
        })
        let supplier = this.state.supplier_list.map((item, index) => {
            return (
                <option key={index} data-name={item.supplier_name} value={item.id}>{item.supplier_code}</option>
            )
        })

        return (

            <div className='box no-border'>
                <div className='box-body'>
                    <div className='col-md-4'>
                        <div className={this.state.itemError.product_name.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Product Name</label>
                            <Select.Async
                                ref={(node) => this.inputSelect = node}
                                name="product_name"
                                value={this.state.selected_product}
                                onChange={(data) => this.onSelectDataChange("product_name", data)}
                                loadOptions={this.loadProducts}
                                defaultOptions
                                cacheOptions
                            />
                            <div className='help-block' style={{ fontSize: '12px' }}  >{this.state.itemError.product_name.length > 0 ? this.state.itemError.product_name : ""}</div>
                            {/* <select className='form-control' name='product_code' value={this.state.item.product_code} onChange={(e) => this.onHandleChange(e)} >
                                <option value=''>--Select--</option>
                                {product}
                            </select> */}
                        </div>
                    </div>



                    <div className='col-md-2'>
                        <div className={this.state.itemError.qty.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Quantity</label>
                            <input className='form-control' type='number' name='qty' value={this.state.item.qty} onChange={(e) => this.onHandleChange(e)} />
                            <div className='help-block' style={{ fontSize: '12px' }} >{this.state.itemError.qty.length > 0 ? this.state.itemError.qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-1'>
                        <div className={this.state.itemError.free_qty.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Free</label>
                            <input className='form-control' type='number' name='free_qty' value={this.state.item.free_qty} onChange={(e) => this.onHandleChange(e)} required />
                            <div className='help-block'>{this.state.itemError.free_qty.length > 0 ? this.state.itemError.free_qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.itemError.scheme_value.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Scheme</label>
                            <input className='form-control' type='text' name='scheme_value' value={this.state.item.scheme_value} onChange={(e) => this.onHandleChange(e)} required />
                            <div className='help-block'>{this.state.itemError.scheme_value.length > 0 ? this.state.itemError.scheme_value : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>

                        <button className='btn btn-success pull-right col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onSave(e)}>Add</button>

                    </div>
                </div>
            </div>
        )
    }

}