import React from 'react';
import Axios from 'axios';
import Loader from './../ui/loader';
import Alert from 'react-s-alert';
import { Link } from 'react-router-dom';

import { buildURL, SALES_ORDER_PRODUCT } from './../../constants/api';

export default class NewProductModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            product: {
                product_name: "",
            },
            error: {
                product_name: [],
            },
            isDisabled: true,
            isShow: true
        }
    }

    componentDidMount(){
        this.orderSuccess.focus();
    }

    reserProduct() {
        let mProduct = this.state.product;

        Object.keys(mProduct).forEach((key) => {
            mProduct[key] = ""
        });
        this.setState({ product: mProduct });
    }

    onHandleChange(e) {
        let mProduct = this.state.product;

        mProduct[e.target.name] = e.target.value;

        this.setState({ product: mProduct, isDisabled: false });
    }

    onAdd(e) {
        e.preventDefault();
        this.resetError();
        this.setState({ isDisabled: true });
        Axios.post(buildURL(SALES_ORDER_PRODUCT), this.state.product)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 201) {
                    this.successAlert('Addedd successully');
                    this.onClose(e);
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ isDisabled: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access')
                    this.setState({ isDisabled: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access')
                    this.setState({ isDisabled: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found')
                    this.setState({ isDisabled: false });
                }
            })
            .catch((error) => {
                window.alert(error)
                this.setState({ isDisabled: false });
            })
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    setError(error) {
        let mError = this.state.error;

        Object.keys(error).forEach((key) => {
            mError[key] = error[key]
        })

        this.setState({ error: mError });
    }

    resetError() {
        let mError = this.state.error;

        Object.keys(mError).forEach((key) => {
            mError[key] = []
        })

        this.setState({ error: mError });
    }

    onClose(e) {
        e.preventDefault();
        this.props.onClose();
        this.setState({ isShow: false });
    }

    render() {
        return (
            <div>
                <div className={this.state.isShow ? 'modal fade in' : 'modal fade'} style={this.state.isShow ? { display: 'block', paddingRight: '16px' } : { display: 'none' }} >

                    <div className="modal-dialog modal-md">
                        <div className="modal-content">

                            <div className="modal-header">

                                <button type="button" className="close" onClick={(e) => this.onClose(e)} data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                            </div>

                            <div className="modal-body" style={{ maxHeight: "450px", overflow: 'auto' }} >
                                <div className='row'>
                                    <div className='col-md-12'>

                                        <div className={this.state.error.product_name.length > 0 ? 'form-group has-error' : 'form-group'} >
                                            <label>Product Name</label>
                                            <input ref={(node) => this.orderSuccess = node} className='form-control' name='product_name' value={this.state.product.product_name} onChange={(e) => this.onHandleChange(e)} />
                                            <div className='help-block'>{this.state.error.product_name.length > 0 ? this.state.error.product_name : ""}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className='modal-footer'>
                                <button className='btn btn-primary col-md-2 pull-right' disabled={this.state.isDisabled} onClick={(e) => this.onAdd(e)} >Add</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}