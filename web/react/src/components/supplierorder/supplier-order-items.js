import React from 'react';
import Axios from 'axios';
import { buildURL,SUPPLIER_ORDER_ITEM_DELETE,SUPPLIER_ORDER_ITEM_UPDATE, SALES_ORDER_ITEM_UPDATE, PRODUCT_SEARCH } from './../../constants/api';
import Alert from 'react-s-alert';
import Select from 'react-select';

export default class SupplierOrderItems extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            item: {
                supplier_code: '',
                supplier_name: '',
                product_code: '',
                product_name: '',
                qty: '',
            },
            item_index: '',
            isUpdate: false,
            order_id: '',
            product_list: [],
            supplier_list: [],
            product_items: [],
            supplier_items: [],
            selected_product: '',
            error: {
                product_name: [],
                product_value: [],
                product_code: [],
                qty: [],
                free_qty:[],
                scheme_value:[]
            }
        }
    }

    componentWillMount() {
    }

    componentDidMount() {
        let mSelectedProduct = {
            label: this.props.items.product_name,
            value: this.props.items.product_name,
            name: this.props.items.product_code,
        }
        this.setState({
            item: this.props.items,
            order_id: this.props.orderId,
            product_list: this.props.product,
            supplier_list: this.props.supplier,
            selected_product: mSelectedProduct,
        });
        this.setProductData(this.props.product);
        this.setSupplierData(this.props.supplier);
    }

    setProductData(product) {
        let mProduct = this.state.product_items;
        if (mProduct.length <= 0)
            for (let i = 0; i < product.length; i++) {
                let item = {
                    label: product[i].product_name,
                    value: product[i].product_name,
                    name: product[i].product_code,
                }
                mProduct.push(item);
            }
        this.setState({ product_items: mProduct });
    }

    setSupplierData(supplier) {
        let mSupplier = this.state.supplier_items;
        if (mSupplier.length <= 0)
            for (let i = 0; i < supplier.length; i++) {
                let item = {
                    label: supplier[i].supplier_code,
                    value: supplier[i].supplier_code,
                    name: supplier[i].supplier_name,
                }
                mSupplier.push(item);
            }
        this.setState({ supplier_items: mSupplier });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            item: nextProps.items,
            order_id: nextProps.orderId,
            product_list: nextProps.product,
            supplier_list: nextProps.supplier
        });
        this.setProductData(nextProps.product);
        this.setSupplierData(nextProps.supplier);
    }

    onHandleChange(e) {
        let mItem = this.state.item;
        mItem[e.target.name] = e.target.value;
        if (e.target.name === 'product_code') {
            mItem.product_name = e.target[e.target.selectedIndex].getAttribute('data-name');
        }
        if (e.target.name === 'supplier_code') {
            mItem.supplier_name = e.target[e.target.selectedIndex].getAttribute('data-name');
        }
        this.setState({ item: mItem });
    }

    onSelectDataChange(name, value) {
        if (name === 'product_name') {
            let mItem = this.state.item;
            if (value !== null) {
                mItem.product_code = value.name;
                mItem.product_name = value.label;
                mItem.product_value = value.label;
            } else {
                mItem.product_name = '';
                mItem.product_code = '';
            }
            this.setState({ item: mItem, selected_product: value });
        } else if (name === 'supplier_code') {
            let mItem = this.state.item;
            if (value !== null) {
                mItem.supplier_code = value.label;
                mItem.supplier_name = value.name;
                //mItem.product_value = value.label;
            } else {
                mItem.supplier_name = '';
                mItem.supplier_code = '';
            }
            this.setState({ item: mItem, selected_supplier: value });
        }
    }

    onUpdate(e) {
        e.preventDefault();
        let mError = this.validate();
        if (mError.product_name.length > 0 || mError.qty.length > 0) {
            this.setState({ error: mError });

            return false;

        } else {

            this.setState({ error: { product_name: [], qty: [],free_qty:[],scheme_value:[] } });
        }
     
        Axios.post(buildURL(SUPPLIER_ORDER_ITEM_UPDATE, { order_id: this.state.order_id, orderItem: this.state.item.id, }), this.state.item)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert('Updated successully');
                    //this.getOrder();
                    //window.location = '/order'
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    validate() {
        let mProduct = this.state.item;
        let mError = {
            product_name: [],
            qty: []
        };
        if (mProduct.product_code === "") {
            mError.product_name.push("Product name cannot be empty");
        }
        if (mProduct.qty === "") {
            mError['qty'].push("Quantity cannot be empty");
        }
        else if (mProduct.qty <= 0) {
            mError.qty.push("Quantity must be greater than zero");
        }
        return mError;
    }

    onDelete(e) {
        e.preventDefault();
        if (window.confirm('Do you want o delete this item?')) {
            Axios.delete(buildURL(SUPPLIER_ORDER_ITEM_DELETE, { orderItem: this.state.item.id, order_id: this.state.order_id }))
                .then((response) => {
                    let mData = response.data;
                    if (mData.statusCode === 200) {
                        this.successAlert("Deleted successully")
                        this.props.onDeleteSuccess(this.state.item.id);
                    } else if (mData.statusCode === 500) {
                        this.errorAlert('Internal Server Error')
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 401) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 403) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 404) {
                        this.errorAlert('Page not found');
                        this.setState({ progressStatus: false });
                    }
                })
                .catch((error) => {
                    this.errorAlert(error);
                });
        }
    }

    loadProducts(value, callback) {
        Axios.get(buildURL(PRODUCT_SEARCH, { query: value }))
            .then((response) => {
                let product = response.data.data;
                let mProduct = [];
                for (let i = 0; i < product.length; i++) {
                    let item = {
                        label: product[i].product_name,
                        value: product[i].product_name,
                        name: product[i].product_code,
                    }
                    mProduct.push(item);
                }
                //console.log('mProduct',mProduct);
                let data = { options: mProduct }
                callback(null, data);
            })
            .catch((error) => {
                callback(null, [])
            })
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    render() {
        let product = this.state.product_list.map((item, index) => {
            return (<option data-name={item.product_name} value={item.id}>{item.product_code}</option>)
        })
        let supplier = this.state.supplier_list.map((item, index) => {
            return (<option data-name={item.supplier_name} value={item.id}>{item.supplier_code}</option>)
        })
        return (
            <div className='row' >
                <div className='col-md-12'>
                    <div className='col-md-5'>
                        <div className={this.state.error.product_name.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Product Name</label>
                            <Select.Async
                                ref={(node) => this.inputSelect = node}
                                name="product_name"
                                value={this.state.selected_product}
                                onChange={(data) => this.onSelectDataChange("product_name", data)}
                                loadOptions={this.loadProducts}
                                defaultOptions
                                cacheOptions
                            />
                            <div className='help-block' style={{ fontSize: '12px' }}  >{this.state.error.product_name.length > 0 ? this.state.error.product_name : ""}</div>
                            {/* <select className='form-control' name='product_code' value={this.state.item.product_code} onChange={(e) => this.onHandleChange(e)} >
                                <option value=''>--Select--</option>
                                {product}
                            </select> */}
                        </div>

                    </div>


                    <div className='col-md-1'>
                        <div className={this.state.error.qty.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Quantity</label>
                            <input className='form-control' type='number' name='qty' value={this.state.item.qty} onChange={(e) => this.onHandleChange(e)} />
                            <div className='help-block' style={{ fontSize: '12px' }} >{this.state.error.qty.length > 0 ? this.state.error.qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-1'>
                        <div className={this.state.error.free_qty.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Free</label>
                            <input className='form-control' type='number' name='free_qty' value={this.state.item.free_qty} onChange={(e) => this.onHandleChange(e)} />
                            <div className='help-block' style={{ fontSize: '12px' }} >{this.state.error.free_qty.length > 0 ? this.state.error.free_qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.error.scheme_value.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Scheme</label>
                            <input className='form-control' type='text' name='scheme_value' value={this.state.item.scheme_value} onChange={(e) => this.onHandleChange(e)} />
                            <div className='help-block' style={{ fontSize: '12px' }} >{this.state.error.scheme_value.length > 0 ? this.state.error.qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div>
                            <button className='btn btn-info col-md-5' style={{ marginTop: '25px' }} onClick={(e) => this.onUpdate(e)}>Update</button>
                            <button className='btn btn-danger col-md-5' style={{ marginTop: '25px', marginLeft: '2px' }} onClick={(e) => this.onDelete(e)}>Delete</button>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}