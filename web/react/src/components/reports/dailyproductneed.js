import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';

import { buildURL, REPORTS ,DAILY_WANTED_REPORTS} from './../../constants/api';

import { _ORDER_STATUS, _SEARCH_BY,_ORDER_ITEM_STATUS,_DELIVERY_TYPE, } from './../../constants/global';

export default class DailyProductNeed extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            reports: [],
            filter: {
                name: '',
                from: '',
                to: '',
                order_status: '',
                item_status:'',
                search_by: 'product'
            },
            progressStatus: false,
        }
    }

    getReports(e) {
        e.preventDefault();
        this.setState({ progressStatus: true });
        Axios.get(buildURL(DAILY_WANTED_REPORTS, this.state.filter))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ reports: mData.data, progressStatus: false });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server error');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    onHandleChange(e) {
        let mFilter = this.state.filter;
        mFilter[e.target.name] = e.target.value;
        this.setState({ filter: mFilter });
    }

    

    onDateChange(e){
        
                let mFilter = this.state.filter;
                
        
                if(e.target.name=='from')
                {
                    mFilter[e.target.name] = e.target.value;
                    localStorage.setItem("rfrom",e.target.value);
                }else if(e.target.name=='to'){
        
                    mFilter[e.target.name] = e.target.value;
                    localStorage.setItem("rto",e.target.value);
                }
        
        
                this.setState({ filter: mFilter });
        
            }
        

    onDownloadReports(e) {
        e.preventDefault();
        this.onDownload(this.state.reports);
    }

    getProgress() {
        return (
            <div className='box no-border'>
                <div className='overlay'>
                    <i className='fa fa-refresh fa-spin' />
                </div>
            </div>
        )
    }

    componentDidMount(){

        let cachFrom=localStorage.getItem("rfrom");
        let cachTo=localStorage.getItem("rto");
      //  console.log("From Data",cachFrom,"to Date",cachTo);
        let filter=this.state.filter
        filter.from =cachFrom===null?"":cachFrom;
        filter.to = cachTo===null?"":cachTo;

        this.setState({filter:filter});

    }

    render() {
        let progress = this.state.progressStatus ? this.getProgress() : "";
        return (
            <div>

                <div className='row'>
                    {this.renderFilter()}
                    <div className="col-md-12">
                <br/>
                <button className='btn btn-info' onClick={(e) => this.getReports(e)} >Get Reports</button>
                    {this.state.reports.length > 0 ?
                        <button style={{ marginLeft: '5px' }} className='btn btn-warning' onClick={(e) => this.onDownloadReports(e)} >Download</button>
                        : ""
                    }
                
                </div>
                </div>
                <div className='row'>
             

                </div>
                {progress}
                <div className='row'>
                    <br />
                    {this.state.reports.length > 0 ?
                        this.renderTable()
                        :
                        ""
                    }
                </div>
            </div>
        )
    }

    renderFilter() {


        let searchBy = _SEARCH_BY.map((item, index) => {
            return (
                <option value={item.id} key={index}>{item.name}</option>
            )
        })

        
        return (
            <div className='col-md-12'>
                <div className='form-inline'>
                    <div className='form-group'>
                        <label>Search</label>
                        <input className='form-control' style={{ textTransform: 'uppercase', marginLeft: '10px', marginRight: '10px', width: '200px', fontSize: '13px', padding: '1px' }} name='name' placeholder='' onChange={(e) => { this.onHandleChange(e) }} />
                    </div>
                    <div className='form-group'>
                        <label>By</label>
                        <select className='form-control' style={{ marginLeft: '10px', marginRight: '10px' }} name='search_by' onChange={(e) => { this.onHandleChange(e) }}>
                            {searchBy}
                        </select>
                    </div>
                   
                    <div className='form-group'>
                        <label>From</label>
                        <input type='date' className='form-control' value={this.state.filter.from} style={{ marginLeft: '10px', marginRight: '10px' }} name='from' placeholder='from' onChange={(e) => { this.onDateChange(e) }} />
                    </div>
                    <div className='form-group'>
                        <label>To</label>
                        <input type='date' className='form-control' value={this.state.filter.to} style={{ marginLeft: '10px', marginRight: '10px' }} name='to' placeholder='to' onChange={(e) => { this.onDateChange(e) }} />
                    </div>
                   
                    {/* <div className='form-group'>
                        <label>Item</label>
                        <select className='form-control' style={{ marginLeft: '10px', marginRight: '10px' }} name='item_status' onChange={(e) => { this.onHandleChange(e) }}>
                            <option value='' >All</option>
                            <option value='15' >Completed</option>
                            {mOrderItems}
                        </select>
                    </div> */}
                    
                
                </div>
               
            </div>
        )
    }

    renderTable() {

        let items = this.state.reports.map((item, index) => {
            return (
                <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.date}</td>
                    <td>{item.product_name}</td>
                   

                    {/* <td>{item.arrival_status}</td> */}

                    
                </tr>
            )
        })
        return (
            <div className='box no-border'>
                <div className='box-body table-responsive no-padding'>
                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr className={"table-header-color"} >
                                <th>S.No</th>
                                <th>Date</th>
                                <th>Product</th>
                            </tr>
                        </thead>
                        <tbody>{items}</tbody>
                    </table>
                </div>

            </div>
        )
    }

    processRow(row) {

        
        var finalVal = '';
        for (var j = 0; j < row.length; j++) {
            var innerValue = row[j] === null || typeof row[j]==='undefined' ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            };
            var result = innerValue.replace(/"/g, '""');
            if (result.search(/("|,|\n)/g) >= 0)
                result = '"' + result + '"';
            if (j > 0)
                finalVal += ',';
            finalVal += result;
        }
        return finalVal + '\n';

    }

    exportToCsv(filename, rows) {

        var csvFile = '';
        for (var i = 0; i < rows.length; i++) {
            csvFile += this.processRow(rows[i]);
        }

        var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }

    onDownload(mReports) {

        if (mReports.length > 0) {
            let reports = [];
            let name = [];
            let date = [];
            let title = [];

            for (let i = 0; i < mReports.length; i++) {
                let output = [];

                if (i === 0) {
                    name.push("", "", "", "", "Wanted Product Report", "", "", "", "", "");
                }

                Object.keys(mReports[i]).forEach((key) => {

                    if (key === 'product_id' || key === 'product_code' || key === 'product_name' || key ==='date') {
                        if (key === 'date') { output[0] = mReports[i][key]; }
                        
                      
                        if (key === 'product_name') { output[2] = mReports[i][key]; }
                        if (key === 'product_code') { output[1] = mReports[i][key]; }
                        

                    }
                })
                console.log('---------------------------------------------------');
                reports.push(output);
            }

            let temp = [name, date, title, ['Date','product_name'], ...reports];
            this.exportToCsv("wanted Product Report" + this.state.filter.from + " - " + this.state.filter.from + ".csv", temp);
        }
    }

    getEmpty() {
        return ["", "", "", "", "", "", "", "", ""];
    }

    getDate() {
        let mDate = new Date();
        return mDate.getDate() + "-" + (mDate.getMonth() + 1) + "-" + mDate.getFullYear();
    }


}