import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';

import { buildURL, CUSTOMER_REPORTS } from './../../constants/api';

export default class CustomerReport extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            reports: [],
            progressStatus: true,
        }
    }

    componentWillMount() {
        this.getReports();
    }

    getReports() {
        Axios.get(buildURL(CUSTOMER_REPORTS))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ reports: mData.data, progressStatus: false });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server error');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    onDownloadReports(e) {
        e.preventDefault();
        this.onDownload(this.state.reports);
    }

    getProgress() {
        return (
            <div className='box no-border'>
                <div className='overlay'>
                    <i className='fa fa-refresh fa-spin' />
                </div>
            </div>
        )
    }

    render() {

        return (
            <div>

                <div className='row'>
                    {this.state.reports.length > 0 ?
                        <div className='col-md-12' >
                            <div className='col-md-1 col-md-offset-9 '>
                                <button style={{ marginLeft: '5px' }} className='btn btn-warning pull-right' onClick={(e) => this.onDownloadReports(e)} >Download</button>
                            </div>
                        </div>
                        : ""
                    }
                </div>
                <div className='col-md-8 col-md-offset-2'>
                    <br />
                    {this.renderTable()}
                </div>
            </div>
        )
    }

    renderTable() {

        let progress = this.state.progressStatus ? this.getProgress() : "";

        let items = this.state.reports.map((item, index) => {
            return (
                <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.mobile_number}</td>

                </tr>
            )
        })
        return (
            <div className='box no-border'>
                <div className='box-body table-responsive no-padding'>
                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr className={"table-header-color"} >
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Mobile No</th>
                            </tr>
                        </thead>
                        <tbody>{items}</tbody>
                    </table>
                </div>
                {progress}
            </div>
        )
    }



    processRow(row) {

        var finalVal = '';
        for (var j = 0; j < row.length; j++) {
            var innerValue = row[j] === null ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            };
            var result = innerValue.replace(/"/g, '""');
            if (result.search(/("|,|\n)/g) >= 0)
                result = '"' + result + '"';
            if (j > 0)
                finalVal += ',';
            finalVal += result;
        }
        return finalVal + '\n';

    }

    exportToCsv(filename, rows) {

        var csvFile = '';
        for (var i = 0; i < rows.length; i++) {
            csvFile += this.processRow(rows[i]);
        }

        var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }

    onDownload(mReports) {

        if (mReports.length > 0) {
            let reports = [];
            let name = [];
            let date = [];
            let title = [];

            for (let i = 0; i < mReports.length; i++) {
                let output = [];

                if (i === 0) {
                    name.push("Customer Report");
                }

                Object.keys(mReports[i]).forEach((key) => {

                    if (key === 'name') {
                        output[0] = mReports[i][key];
                    } else {
                        output[1] = mReports[i][key];
                    }

                })
                reports.push(output);
            }

            let temp = [name, date, title, ['Name', 'Mobile Number'], ...reports];
            this.exportToCsv("Customer Report" + ".csv", temp);
        }
    }

    getEmpty() {
        return ["", "", "", "", "", "", "", "", ""];
    }

    getDate() {
        let mDate = new Date();
        return mDate.getDate() + "-" + (mDate.getMonth() + 1) + "-" + mDate.getFullYear();
    }





}