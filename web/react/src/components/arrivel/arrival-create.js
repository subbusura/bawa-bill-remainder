import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';
import Select, { Async } from 'react-select';

import { buildURL, PRODUCT_SEARCH, ARRIVEL_LIST } from './../../constants/api';

export default class ArrivalCreate extends React.Component {

    constructor(props) {
        super(props);
        this.shortKey = this.shortKey.bind(this);
        this.state = {
            itemError: {
                product_name: [],
                qty: [],
                supplier_name: [],
            },
            item: {
                product_code: '',
                product_name: '',
                required_qty: '',
                available_qty: '',
            },

            itemList: [],
            isUpdate: false,
        }
    }

    componentDidMount() {
        this.inputSelect.focus();
        document.addEventListener("keydown", this.shortKey, false)
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.shortKey, false)
    }

    shortKey(e) {

        if (e.altKey && e.which === 70) {
            if (this.state.itemList.length > 0) {
                this.onNotificationSend(e)
            }
        }
    }

    loadProducts(value, callback) {
        if (value === "") { callback(null, []) }
        Axios.get(buildURL(ARRIVEL_LIST, { query: value }))
            .then((response) => {
                let product = response.data.data;
                let mProduct = [];
                for (let i = 0; i < product.length; i++) {
                    let item = {
                        label: product[i].product_name,
                        value: product[i].product_name,
                        name: product[i].product_code,
                        required_qty: product[i].required_qty,
                    }
                    mProduct.push(item);
                }
                //console.log('mProduct',mProduct);
                let data = { options: mProduct }
                callback(null, data);
            })
            .catch((error) => {
                callback(null, [])
            })
    }

    onSelectDataChange(name, value) {
        if (name === 'product_name') {
            let mItem = this.state.item;
            if (value !== null) {
                mItem.product_code = value.name;
                mItem.product_name = value.label;
                mItem.product_value = value.label;
                mItem.required_qty = value.required_qty
            } else {
                mItem.product_name = '';
                mItem.product_code = '';
                mItem.required_qty = '';
            }
            this.setState({ item: mItem, selected_product: value });
        }
    }

    onSave(e) {
        e.preventDefault();

        let mItem = this.state.item;
        let itemList = this.state.itemList;

        for (let i = 0; i < itemList.length; i++) {
            if (mItem.product_code === itemList[i].product_code) {
                window.alert("Item already exist")
                return false;
            }
        }

        if (mItem.product_name === "" || mItem.qty === "") {
            return false;
        }

        let item_temp = {
            product_code: '',
            product_name: '',
            required_qty: '',
            available_qty: '',
        };
        Object.keys(item_temp).forEach((key) => {
            item_temp[key] = mItem[key];
        });
        itemList.push(item_temp)
        this.setState({ itemList: itemList }, () => this.resetItem());
    }

    onUpdate(e) {
        e.preventDefault();
        let mItem = this.state.item;
        let itemList = this.state.itemList;

        for (let i = 0; i < itemList.length; i++) {
            if (this.state.update_item !== mItem.product_code && mItem.product_code === itemList[i].product_code) {
                this.inputSelect.focus();
                window.alert("Item already exist");
                return false;
            }
        }

        let item_temp = {
            product_code: '',
            product_name: '',
            required_qty: '',
            available_qty: '',
        };
        Object.keys(item_temp).forEach((key) => {
            item_temp[key] = mItem[key];
        });

        itemList[this.state.item_index] = item_temp;

        this.setState({ itemList: itemList, isUpdate: false }, () => this.resetItem());

    }


    resetItem() {
        this.inputSelect.focus();
        let mItem = this.state.item;
        Object.keys(mItem).forEach((key) => {
            mItem[key] = ""
        });
        this.setState({ item: mItem, selected_product: '' });
    }

    onHandleChange(e) {
        let mItem = this.state.item;
        mItem[e.target.name] = e.target.value;
        if (e.target.name === 'product_code') {
            mItem.product_name = e.target.options[e.target.selectedIndex].getAttribute('data-name');
        }
        if (e.target.name === 'supplier_code') {
            mItem.supplier_name = e.target.options[e.target.selectedIndex].getAttribute('data-name');
        }
        this.setState({ item: mItem });
    }

    onItemEdit(e, item, index) {
        e.preventDefault();
        let mSelectedProduct = {
            label: item.product_name,
            value: item.product_name,
            name: item.product_code,
            required_qty: item.required_qty,
            available_qty: item.available_qty,
        }

        const mItem = item;
        let mNew = {};
        Object.keys(mItem).forEach((key) => {
            mNew[key] = mItem[key];
        })

        this.setState({
            item: mNew,
            item_index: index,
            update_item: item.product_code,
            isUpdate: true,
            selected_product: mSelectedProduct,
        });
    }

    onItemDelete(e, index) {
        e.preventDefault();
        let mOrder = this.state.itemList;

        let mFilter = mOrder.filter((item, mIndex) => {
            if (index !== mIndex) {
                return item;
            }
        });
        mOrder = mFilter;
        this.setState({ itemList: mOrder, isUpdate: false }, () => this.resetItem());
    }

    onNotificationSend(e) {
        e.preventDefault();
        if (window.confirm('Do you want to send SMS?')) {

            Axios.post(buildURL("/arrival/send"),this.state.itemList)
                .then((response) => {
                    let mData = response.data;
                    if (mData.statusCode === 200) {
                        //this.getData();
                        this.setState({itemList:[]},()=>this.resetItem());
                    } else if (mData.statusCode === 422) {
                        window.alert('Validation Error')
                    } else if (mData.statusCode === 500) {
                        window.alert('Internal Server Error')
                    } else if (mData.statusCode === 401) {
                        window.alert('Unauthorized access')
                    } else if (mData.statusCode === 403) {
                        window.alert('Unauthorized access')
                    } else if (mData.statusCode === 404) {
                        window.alert('Page not found')
                    }
                })
                .catch((error) => {
                    window.alert(error)
                })
        }
    }


    render() {
        return (
            <div>
                <div className='row'>
                    {this.renderItemsForm()}
                </div>
                <div className='row'>
                    {
                        this.state.itemList.length > 0 ?
                            this.renderItems() : ""
                    }
                </div>
            </div>
        )
    }

    renderItemsForm() {

        return (

            <div className='box no-border'>
                <div className='box-body'>
                    <div className='col-md-4'>
                        <div className={this.state.itemError.product_name.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Product Name</label>
                            {/* <input className='form-control' disabled name='product_name' value={this.state.item.product_name} onChange={(e) => this.onHandleChange(e)} /> */}
                            <Select.Async
                                ref={(node) => this.inputSelect = node}
                                name="product_name"
                                value={this.state.selected_product}
                                onChange={(data) => this.onSelectDataChange("product_name", data)}
                                loadOptions={this.loadProducts}
                                defaultOptions
                                cacheOptions
                            />
                            <div className='help-block'>{this.state.itemError.product_name.length > 0 ? this.state.itemError.product_name : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.itemError.qty.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Required Quantity</label>
                            <input className='form-control' disabled type='number' name='required_qty' value={this.state.item.required_qty} onChange={(e) => this.onHandleChange(e)} />
                            <div className='help-block'>{this.state.itemError.qty.length > 0 ? this.state.itemError.qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className={this.state.itemError.qty.length > 0 ? 'form-group has-error' : 'form-group'}>
                            <label>Available Quantity</label>
                            <input className='form-control' type='number' name='available_qty' value={this.state.item.available_qty} onChange={(e) => this.onHandleChange(e)} />
                            <div className='help-block'>{this.state.itemError.qty.length > 0 ? this.state.itemError.qty : ""}</div>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        {
                            this.state.isUpdate ?
                                <button className='btn btn-primary pull-right col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onUpdate(e)}>Update</button>
                                :
                                <button className='btn btn-success pull-right col-md-12' style={{ marginTop: '25px' }} onClick={(e) => this.onSave(e)}>Add</button>
                        }

                    </div>
                </div>
            </div>
        )
    }

    renderItems() {

        let items = this.state.itemList.map((item, index) => {
            return (
                <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.product_name}</td>
                    <td>{item.required_qty}</td>
                    <td>{item.available_qty}</td>
                    <td>
                        {
                            <div>
                                <a href="" onClick={(e) => this.onItemEdit(e, item, index)} ><span className='fa fa-pencil'></span> </a>
                                <a href="" onClick={(e) => this.onItemDelete(e, index)} style={{ marginLeft: '10px' }} ><span className='fa fa-trash'></span> </a>
                            </div>
                        }
                    </td>
                </tr>
            )
        })

        return (
            <div className='box no-border'>
                <div className='box-header with-border'>
                    <h3 className='box-title'>Items</h3>
                    <button className='btn btn-success pull-right col-md-2' disabled={this.state.isAdd} onClick={(e) => this.onNotificationSend(e)} >{'Send Sms (Alt + F)'}</button>
                </div>
                <div className='box-body table-responsive no-padding'>
                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Product Name</th>
                                <th>Required Quantity</th>
                                <th>Available Quantity</th>
                                <th>Acition</th>
                            </tr>
                        </thead>
                        <tbody>{items}</tbody>
                    </table>
                </div>
            </div>
        )
    }

}