import React from 'react';
import Axios from 'axios';
import Pagination from 'react-js-pagination';
import { buildURL, ARRIVEL_LIST } from './../../constants/api';

import { Link } from 'react-router-dom';

export default class ArrivelList extends React.Component {

    constructor(props) {
        super(props);
        this.shortKey = this.shortKey.bind(this);
        this.state = {
            selected: {},
            selectAll: 0,
            data: [],
            pagination: {
                activePage: 1,
                itemsCountPerPage: 10,
                totalItemsCount: 3,
                pageRangeDisplayed: 3,
            },
            filter: []
        };
    }

    componentDidMount() {
        document.addEventListener("keydown", this.shortKey, false)
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.shortKey, false)
    }

    componentWillMount() {
        this.getData();
    }

    shortKey(e) {

        if (e.altKey && e.which === 70) {
            if (Object.keys(this.state.selected).length > 0) {
                this.sendNotification(e)
            }
        }
    }

    setPagination(data) {
        let activePage = this.state.pagination.activePage;
        let perPage = this.state.pagination.itemsCountPerPage;
        let offset = (activePage - 1) * perPage;
        let fData = data.slice(offset).slice(0, perPage);

        let pagination = this.state.pagination;
        pagination['totalItemsCount'] = data.length;
        this.setState({ pagination: pagination });

        return fData;
    }

    onPageChange(page) {

        let lpagination = this.state.pagination;
        lpagination.activePage = page;
        let perPage = this.state.pagination.itemsCountPerPage;
        let activePage = page;
        let offset = (activePage - 1) * perPage;
        let filterData = this.state.data.slice(offset).slice(0, perPage);
        this.setState({ pagination: lpagination, filter: filterData });
    }

    getData() {

        Axios.get(buildURL(ARRIVEL_LIST))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    let filter = this.setPagination(mData.data);
                    this.setState({ data: mData.data, filter: filter, progressStatus: false });
                } else if (mData.statusCode === 500) {
                    window.alert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    window.alert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    window.alert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    window.alert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                window.alert(error);
                this.setState({ progressStatus: false });
            });

    }

    toggleRow(product_code) {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[product_code] = !this.state.selected[product_code];

        this.setState({
            selected: newSelected,
            selectAll: 2
        }, () => {
            //console.log(this.state.selected);
        });
    }

    toggleSelectAll() {
        let newSelected = {};

        if (this.state.selectAll === 0) {
            this.state.data.forEach(x => {
                newSelected[x.product_code] = true;
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: this.state.selectAll === 0 ? 1 : 0
        }, () => {
            //console.log(this.state.selected);
        });
    }

    sendNotification(e) {
        e.preventDefault();

        let updated_list = [];

        Object.keys(this.state.selected).forEach((key, index) => {
            if (this.state.selected[key]) {
                updated_list.push(key)
            }
        })

        //console.log("final value", updated_list);


        if (window.confirm('Do you want to send SMS?')) {

            Axios.post(buildURL("/arrival/send"), { product_code: updated_list })
                .then((response) => {
                    let mData = response.data;
                    if (mData.statusCode === 200) {
                        this.getData();
                    } else if (mData.statusCode === 422) {
                        window.alert('Validation Error')
                    } else if (mData.statusCode === 500) {
                        window.alert('Internal Server Error')
                    } else if (mData.statusCode === 401) {
                        window.alert('Unauthorized access')
                    } else if (mData.statusCode === 403) {
                        window.alert('Unauthorized access')
                    } else if (mData.statusCode === 404) {
                        window.alert('Page not found')
                    }
                })
                .catch((error) => {
                    window.alert(error)
                })
        }


    }
    onHandleChange(e) {
        this.searchInput.value = e.target.value;
    }

    onFilter() {

        //console.log(this.searchInput.value);

        if (this.searchInput.value === "") {
            let filter = this.setPagination(this.state.data);
            this.setState({ filter: filter })
            //console.log("onSearch Empty");

        } else {
            let mFilter = [];
            for (let i = 0; i < this.state.data.length; i++) {
                if (this.state.data[i].product_name.toLowerCase().search(this.searchInput.value.toLocaleLowerCase()) >= 0 || this.state.data[i].supplier_name.toLowerCase().search(this.searchInput.value.toLocaleLowerCase()) >= 0) {
                    mFilter.push(this.state.data[i])
                }
            }
            let filter = this.setPagination(mFilter);
            //console.log("onSearch With Data", filter);
            this.setState({ filter: filter })
        }


    }

    onSearch(e) {

        e.preventDefault();
        if (this.state.searchText === "") {
            let filter = this.setPagination(this.state.data);
            this.setState({ filter: filter })
            //console.log("onSearch Empty");

        } else {
            let mFilter = [];
            for (let i = 0; i < this.state.data.length; i++) {
                if (this.state.data[i].name.toLowerCase().search(this.state.searchText.toLocaleLowerCase()) >= 0) {
                    mFilter.push(this.state.data[i])
                }
            }
            let filter = this.setPagination(mFilter);
            //console.log("onSearch With Data", filter);
            this.setState({ filter: filter })
        }

    }

    renderFilter() {

        return (
            <div>
                <div className='col-md-2'>
                    <Link to='/arrival/create' className='btn btn-success' >Create</Link>
                </div>
                <button className='btn btn-info col-md-2 pull-right' onClick={(e) => this.onFilter(e)} >Search</button>
                <div className='form-inline pull-right'>
                    <div className='form-group'>
                        <label>Search</label>
                        <input ref={(input) => this.searchInput = input} className='form-control' style={{ textTransform: 'uppercase', marginLeft: '10px', marginRight: '10px', width: '250px', fontSize: '13px', padding: '5px' }} name='name' placeholder='Product Name, Supplier Name' onChange={(e) => { this.onHandleChange(e) }} />
                    </div>
                </div>
                {/* <div className='form-group'>
                        <input ref={(input) => this.searchInput = input} className='form-control' name='name' placeholder='Search..' />
                    </div> */}

            </div>
        )
    }

    render() {

        let btn_notification = "";
        if (Object.keys(this.state.selected).length > 0) {
            btn_notification =<button className='btn btn-success' disabled={this.state.selected ? false : true} onClick={(e) => this.sendNotification(e)} >{'Send Sms (Alt+F)'}</button>
        }

        return (
            <div>
                <div className='row'>
                    {this.renderFilter()}
                </div>

                <div className='row'>
                    <br />
                    {btn_notification}
                    {this.renderTable()}

                </div>
            </div>
        )
    }

    renderTable() {

        const rows = this.state.filter.map((item, index) => {

            return (
                <tr key={index}>

                    <td>{index + 1}</td>
                    <td>{item.product_name}</td>
                    <td>{item.supplier_name}</td>
                    <td>{item.required_qty}</td>
                    <td>
                        {
                            <input
                                type="checkbox"
                                className="checkbox"
                                checked={this.state.selected[item.product_code] === true}
                                onChange={() => this.toggleRow(item.product_code)}
                            />
                        }
                    </td>
                </tr>)

        })
        return (
            <div className='box no-border'>
                <div className='box-body no-padding'>

                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr className={"table-header-color"} >

                                <th>S.No</th>
                                <th>Product Name</th>
                                <th>Supplier Name</th>
                                <th>Required Qty</th>
                                <th><input
                                    type="checkbox"
                                    className="checkbox"
                                    checked={this.state.selectAll === 1}
                                    ref={input => {
                                        if (input) {
                                            input.indeterminate = this.state.selectAll === 2;
                                        }
                                    }}
                                    onChange={() => this.toggleSelectAll()}
                                /></th>
                            </tr>
                        </thead>
                        <tbody>{rows}</tbody>
                    </table>
                    {this.state.data.length > 0 ?
                        <Pagination
                            activePage={this.state.pagination.activePage}
                            itemsCountPerPage={this.state.pagination.itemsCountPerPage}
                            totalItemsCount={this.state.pagination.totalItemsCount}
                            pageRangeDisplayed={this.state.pagination.pageRangeDisplayed}
                            onChange={(e, p) => { this.onPageChange(e, p) }}
                        />
                        : ""}
                </div>
            </div>
        )
    }


}    