import React from 'react';
import Axios from 'axios';
import Loader from './../ui/loader';
import Pagination from 'react-js-pagination';
import Alert from 'react-s-alert';
import { Link } from 'react-router-dom';

import { SUPPLIER_LIST, buildURL, SUPPLIER_DELETE } from './../../constants/api';

export default class SupplierList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

            supplier_list: [],
            progressStatus: true,
            page: {
                totalCount: '1',
                pageCount: '1',
                currentPage: '1',
                perPage: '100'
            },
            filter: {
                supplier_code: '',
                supplier_name: '',
            }
        }
    }

    componentWillMount() {
        this.getSupplierList(this.state.page.currentPage);
    }

    getSupplierList(page) {
        Axios.get(buildURL(SUPPLIER_LIST, { page: page, 'per-page': this.state.page.perPage }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ supplier_list: mData.data.items, progressStatus: false, page: mData.data._meta });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    onPageChange(page) {

        let mPage = this.state.page;
        mPage.currentPage = page;
        this.getSupplierList(page);
        this.setState({ page: mPage });
    }

    errorAlert(message){
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message){
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    onDeleteItem(e, item) {
        e.preventDefault();
        this.setState({ progressStatus: true });
        if (window.confirm('Do you want to remove this supplier?')) {
            Axios.delete(buildURL(SUPPLIER_DELETE, { id: item.id }))
                .then((response) => {
                    let mData = response.data;
                    if (response.status === 204) {
                        this.successAlert("Deleted successfully");
                        this.getSupplierList(this.state.page.currentPage);
                    } else if (mData.statusCode === 500) {
                        this.errorAlert('Internal Server Error')
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 401) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 403) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 404) {
                        this.errorAlert('Page not found');
                        this.setState({ progressStatus: false });
                    }
                })
                .catch((error) => {
                    this.errorAlert(error);
                    this.setState({ progressStatus: false });
                });
        }
    }

    onFilterChange(e) {
        let mFilter = this.state.filter;
        let mPage = this.state.page;

        if (e.target.name === 'perPage') {
            mPage[e.target.name] = e.target.value;
        } else {
            mFilter[e.target.name] = e.target.value;
        }

        this.setState({ filter: mFilter, page:mPage });

    }

    onFilter(e) {
        e.preventDefault();
        Axios.post(buildURL(SUPPLIER_LIST, { page: this.state.page.currentPage, 'per-page': this.state.page.perPage }),this.state.filter)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ supplier_list: mData.data.items, progressStatus: false, page: mData.data._meta });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    render() {
        return (
            <div>
                <div className='row'>
                    <div className='col-md-2'>
                        <a className='btn btn-success' href='/supplier/create'>Add Supplier</a>
                    </div>
                </div>

                <div className='row'>
                    <br />
                    <div className='col-md-3'>
                        {this.renderFilter()}
                    </div>
                    <div className='col-md-9'>

                        {
                            this.state.progressStatus ? <Loader /> : this.renderTable()
                        }
                        {
                            this.state.supplier_list.length > 0 ?
                                <Pagination
                                    activePage={this.state.page.currentPage}
                                    itemsCountPerPage={this.state.page.perPage}
                                    totalItemsCount={this.state.page.totalCount}
                                    pageRangeDisplayed={this.state.page.pageCount}
                                    onChange={(e, p) => { this.onPageChange(e, p) }}
                                />
                                : ""
                        }
                    </div>
                </div>

            </div>
        )
    }

    renderTable() {

        let supplier = this.state.supplier_list.map((item, index) => {
            return (
                <tr key={index}>

                    <td>{((this.state.page.currentPage - 1) * this.state.page.perPage) + (index + 1)}</td>
                    <td>{item.supplier_code}</td>
                    <td>{item.supplier_name}</td>
                    <td>
                        {
                            <div>
                                <Link to={'/supplier/update/' + item.id}><span className='fa  fa-pencil'></span></Link>
                                <a href='#'><span className='fa  fa-trash-o' style={{ marginLeft: '10px' }} onClick={(e) => this.onDeleteItem(e, item)} ></span></a>
                            </div>
                        }
                    </td>
                </tr>
            )
        })

        return (
            <div className='box no-border'>
                <div className='box-body no-padding'>

                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr className={"table-header-color"} >
                                <th>S.No</th>
                                <th>Supplier Code</th>
                                <th>Supplier Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>{supplier}</tbody>
                    </table>
                </div>
            </div>
        )
    }

    renderFilter() {
        return (
            <div className='box box-info'>
                <div className='box-header with-border'>
                    <h4 className='box-title'>Search</h4>
                </div>
                <div className='box-body'>
                    <div className='form-group'>
                        <label>Supplier Per Page</label>
                        <input  style={{ textTransform:'uppercase'}} type='number' className='form-control' name='perPage' value={this.state.page.perPage} onChange={(e) => this.onFilterChange(e)} />
                    </div>
                    <div className='form-group'>
                        <label>Supplier Code</label>
                        <input  style={{ textTransform:'uppercase'}} className='form-control' name='supplier_code' value={this.state.filter.supplier_code} onChange={(e) => this.onFilterChange(e)} />
                    </div>
                    <div className='form-group'>
                        <label>Supplier Name</label>
                        <input  style={{ textTransform:'uppercase'}} className='form-control' name='supplier_name' value={this.state.filter.supplier_name} onChange={(e) => this.onFilterChange(e)} />
                    </div>
                </div>

                <div className='box-footer'>
                    <button className='btn btn-info pull-right' onClick={(e)=>this.onFilter(e)} >Search</button>
                </div>

            </div>
        )
    }

}