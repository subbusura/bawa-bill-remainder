import React from 'react';
import Axios from 'axios';
import Loader from './../ui/loader';
import Alert from 'react-s-alert';
import { Link } from 'react-router-dom';

import { SUPPLIER_CREATE, buildURL, SUPPLIER_VIEW, SUPPLIER_UPDATE } from './../../constants/api';

export default class SupplierCreate extends React.Component {

    constructor(props) {
        super(props);

        let params = this.props.match.params;
        this.state = {
            supplier: {
                supplier_code: "",
                supplier_name: "",
                division_code: ""
            },
            error: {
                supplier_code: [],
                supplier_name: [],
            },
            id: params.id,
            update_status: false,
            isDisabled:true,
        }
    }

    componentDidMount() {
        if (typeof this.state.id !== 'undefined') {
            this.getSupplier();
            this.setState({ update_status: true });
        } else {
            this.setState({ update_status: false });
        }
    }

    getSupplier() {
        Axios.get(buildURL(SUPPLIER_VIEW, { id: this.state.id }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setSupplier(mData.data);
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error);
                } else if (mData.statusCode === 500) {
                    window.alert('Internal Server Error')
                } else if (mData.statusCode === 401) {
                    window.alert('Unauthorized access')
                } else if (mData.statusCode === 403) {
                    window.alert('Unauthorized access')
                } else if (mData.statusCode === 404) {
                    window.alert('Page not found')
                }
            })
            .catch((error) => {

            })
    }

    setSupplier(supplier) {
        let mSupplier = this.state.supplier;

        Object.keys(mSupplier).forEach((key) => {
            mSupplier[key] = supplier[key]
        });
        this.setState({ supplier: mSupplier });
    }

    reserSupplier() {
        let mSupplier = this.state.supplier;

        Object.keys(mSupplier).forEach((key) => {
            mSupplier[key] = ""
        });
        this.setState({ supplier: mSupplier });
    }

    onHandleChange(e) {
        let mSupplier = this.state.supplier;

        mSupplier[e.target.name] = e.target.value.toUpperCase();

        this.setState({ supplier: mSupplier, isDisabled:false });
    }

    onAdd(e) {
        e.preventDefault();
        this.resetError();
        this.setState({isDisabled:true});
        Axios.post(buildURL(SUPPLIER_CREATE), this.state.supplier)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 201) {
                    this.successAlert('Addedd successully');
                    this.reserSupplier();
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({isDisabled:false});
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access')
                    this.setState({isDisabled:false});
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access')
                    this.setState({isDisabled:false});
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found')
                    this.setState({isDisabled:false});
                }
            })
            .catch((error) => {
                window.alert(error)
                this.setState({isDisabled:false});
            })
    }

    errorAlert(message){
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message){
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }
    onUpdate(e) {
        e.preventDefault();
        this.resetError();
        this.setState({isDisabled:true});
        Axios.put(buildURL(SUPPLIER_UPDATE, { id: this.state.id }), this.state.supplier)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 201) {
                    this.successAlert('Updated successully');
                    this.props.history.push('/supplier');
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error);
                    this.setState({isDisabled:false});
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error');
                    this.setState({isDisabled:false});
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({isDisabled:false});
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({isDisabled:false});
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({isDisabled:false});
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({isDisabled:false});
            })
    }

    setError(error) {
        let mError = this.state.error;

        Object.keys(error).forEach((key) => {
            mError[key] = error[key]
        })

        this.setState({ error: mError });
    }

    resetError() {
        let mError = this.state.error;

        Object.keys(mError).forEach((key) => {
            mError[key] = []
        })

        this.setState({ error: mError });
    }

    render() {
        return (
            <div>
                <div className='col-md-6 col-md-offset-3'>
                    {this.renderForm()}
                </div>
            </div>
        )
    }

    renderForm() {
        return (
            <div className='box box-info'>
                <div className='box-body'>

                    <div className='box-header with-border'>
                        <Link to='/supplier'><i className='fa fa-arrow-left'/> </Link> 
                        <h4 className='box-title' style={{marginLeft:'20px'}} >{this.state.update_status ? 'Update Supplier' : 'Add Supplier'}</h4>
                    </div>

                    <div className={this.state.error.supplier_code.length > 0 ? 'form-group has-error' : 'form-group'} >
                        <br />
                        <label>Supplier Code</label>
                        <input  style={{ textTransform:'uppercase'}} className='form-control' name='supplier_code' value={this.state.supplier.supplier_code} onChange={(e) => this.onHandleChange(e)} />
                        <div className='help-block'>{this.state.error.supplier_code.length > 0 ? this.state.error.supplier_code : ""}</div>
                    </div>
                    <div className={this.state.error.supplier_name.length > 0 ? 'form-group has-error' : 'form-group'} >
                        <label>Supplier Name</label>
                        <input  style={{ textTransform:'uppercase'}} className='form-control' name='supplier_name' value={this.state.supplier.supplier_name} onChange={(e) => this.onHandleChange(e)} />
                        <div className='help-block'>{this.state.error.supplier_name.length > 0 ? this.state.error.supplier_name : ""}</div>
                    </div>
                </div>

                <div className='box-footer with-border'>
                    {
                        this.state.update_status ?
                            <button className='btn btn-primary col-md-2 pull-right' disabled={this.state.isDisabled} onClick={(e) => this.onUpdate(e)} >Update</button>
                            :
                            <button className='btn btn-primary col-md-2 pull-right' disabled={this.state.isDisabled} onClick={(e) => this.onAdd(e)} >Add</button>
                    }

                </div>

            </div>
        )
    }
}