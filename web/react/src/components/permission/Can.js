import React from 'react';
import Axios from 'axios';
import {buildURL} from './../../constants/api';

export default class Can extends React.PureComponent  {

    constructor(props){
        super(props)
        this.state={
            status:false
        }
    }

    setPermission(key,value){

        sessionStorage.setItem(key,value)
        return true;
    }

    getPermission(key){

          if(sessionStorage.getItem(key) ===null)
          {
              return false;
          }
         return sessionStorage.getItem(key)
    }

    componentWillMount(){

        let currentStatus = this.getPermission(this.props.permission)
        if(currentStatus)
        {
            this.setState({status:currentStatus})
            return false;
        }

        Axios.get(buildURL('/permission/check',{permission:this.props.permission})).then((response)=>{

               let mData=response.data;
              if (mData.statusCode === 200) {
                //console.log("PureComponent",'Can','Success');
                this.setPermission(this.props.permission,true);
                this.setState({status:true})
            } else if (mData.statusCode === 500) {
                this.setState({status:false})
            } else if (mData.statusCode === 401) {
                this.errorAlert('Unauthorized access');
                this.setState({status:false})
            } else if (mData.statusCode === 403) {
                this.errorAlert('Unauthorized access');
                this.setState({status:false})
            } else if (mData.statusCode === 404) {
                this.errorAlert('Page not found');
                this.setState({status:false})
            }
              

        })

    }

        render(){
              let {status} =this.state;

               if(status)
               {
                   return this.props.children;
               }else{
                  return null;
               }

        }
}