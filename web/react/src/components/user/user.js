import React, { Component } from 'react';
import Axios from 'axios';
import UserModel from './usermodel'
import Pagination from "react-js-pagination";
import { withRouter, Link} from 'react-router-dom';
import { buildURL, USER_LIST, ROLE_LIST } from './../../constants/api';
import Alert from 'react-s-alert';

class User extends Component {

    constructor() {
        super();

        this.state = {

            add_status: false,
            user_index: "",
            progress: false,
            user_add_update: "add",
            users: {},
            data: [],
            data_filter: [],
            roles: [],
            retailer_group: [],
            pagination: {
                activePage: 1,
                itemsCountPerPage: 10,
                totalItemsCount: 100,
                pageRangeDisplayed: 3,
            },
            filteredData: [],
            status_list: [
                {
                    id: 10,
                    name: "Active"
                },
                {
                    id: 0,
                    name: "Inactive"
                }
            ],
            status: "",
            search: "",

        }
    }


    componentWillMount() {

        this.getInitialData();
        this.getRoles();

    }


    getInitialData() {

        this.setState({ progress: true });
        Axios.get(buildURL(USER_LIST))
        .then((response) => {
            let mData = response.data;
                if (mData.statusCode === 200) {
                    let mFilter = this.setPagination(response.data.data)
                    this.setState({ data: response.data.data, data_filter: response.data.data, filteredData: mFilter, progress: false })
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progress: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progress: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progress: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progress: false });
                }
        }).catch((error) => {
            this.errorAlert(error.response)
            this.setState({ progress: false });
        });
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    setPagination(data) {
        let perPage = this.state.pagination.itemsCountPerPage;
        let activePage = this.state.pagination.activePage;
        let offset = (activePage - 1) * perPage;
        let filterData = data.slice(offset).slice(0, perPage);

        let pagination1 = this.state.pagination;
        pagination1["totalItemsCount"] = data.length;

        this.setState({ pagination: pagination1 });

        return filterData;

    }

    onPageChange(page) {

        let lpagination = this.state.pagination;
        lpagination.activePage = page;

        let perPage = this.state.pagination.itemsCountPerPage;
        let activePage = page;
        let offset = (activePage - 1) * perPage;
        let filterData = this.state.data_filter.slice(offset).slice(0, perPage);

        this.setState({ pagination: lpagination, filteredData: filterData });

    }

    getRoles() {
        Axios.all([
            Axios.get(buildURL(ROLE_LIST)),
        ])
            .then(Axios.spread((response) => {
                this.setState({ roles: response.data.data })

            }))
            .catch(() => {
            });
    }


    onStatusChange(e) {
        this.setState({ status: e.target.value })
    }

    onReset(e) {
        e.preventDefault();

        let mFilter = this.setPagination(this.state.data);

        this.setState({

            data_filter: this.state.data,
            filteredData: mFilter,
            status: ""
        });

    }

    onFilter(e) {

        e.preventDefault();

        if (this.state.status != "") {

            let filter = [];

            for (let i = 0; i < this.state.data.length; i++) {

                if (this.state.status == this.state.data[i].status) {
                    filter.push(this.state.data[i])
                }
            }

            let mFilter = this.setPagination(filter)
            this.setState({ filteredData: mFilter, data_filter: mFilter })

        } else {
            let mFilter = this.setPagination(this.state.data)
            this.setState({ filteredData: mFilter, data_filter: this.state.data })
        }

    }

    onSearchChange(e) {
        this.setState({ search: e.target.value })
    }

    onSearch(e) {
        if (this.state.search != "") {

            let filter = [];

            for (let i = 0; i < this.state.data.length; i++) {

                if (this.state.data[i].username.toLowerCase().search(this.state.search.toLowerCase()) >= 0 ||
                    this.state.data[i].firstname.toLowerCase().search(this.state.search.toLowerCase()) >= 0 ||
                    this.state.data[i].lastname.toLowerCase().search(this.state.search.toLowerCase()) >= 0) {
                    filter.push(this.state.data[i])
                }

            }

            let mFilter = this.setPagination(filter)
            this.setState({ filteredData: mFilter, data_filter: mFilter })

        } else {
            let mFilter = this.setPagination(this.state.data)
            this.setState({ filteredData: mFilter, data_filter: this.state.data })
        }
    }


    onAddUser(e) {

        this.setState({ add_status: true, users: {}, user_add_update: "Add" })
    }

    onUserUpdate(e, value, index) {
        this.setState({ add_status: true, user_add_update: "Update", users: value, user_index: index })
    }

    onModelClose() {
        this.setState({ add_status: false })
    }

    onAddResult(result) {
        this.getInitialData();
        this.setState({add_status: false })
        // let mResult = this.state.data;
        // mResult.push(result)
        // let mFilter = this.setPagination(mResult);
        // this.setState({ filteredData: mFilter, data: mResult, data_filter: mResult, add_status: false })

    }

    onUpdateResult(result, index) {
        this.getInitialData();
        this.setState({add_status: false })
        //this.getInitialData();
        // let mResult = this.state.data;
        // mResult[((this.state.pagination.activePage - 1) * 10) + index] = result;
        // let mFilter = this.setPagination(mResult);
        // this.setState({ filteredData: mFilter, data: mResult, data_filter: mResult, add_status: false })
    }

    onDelete(index) {
        this.getInitialData();
        this.setState({add_status: false })
        // let filter = [];
        // let mFilter = [];
        // for (let i = 0; i < this.state.data.length; i++) {
        //     if (i != (((this.state.pagination.activePage - 1) * 10) + index)) {
        //         mFilter.push(this.state.data[i])
        //     }
        // }
        // let mFilteredData = this.setPagination(mFilter)
        // this.setState({ filteredData: mFilteredData, data: mFilter, data_filter: mFilter, add_status: false })

    }



    render() {

        let status = this.state.status_list.map((value, index) => {
            return (<option key={index} value={value.id}>{value.name}</option>);
        })

        return (

            <div>

                <div className="row">

                    <div className="col-md-3">
                        <p><a className="btn btn-success" onClick={(e) => this.onAddUser(e)} >Create User</a></p>
                    </div>

                    <div className="col-md-offset-6 col-md-3">
                        <div className="input-group">
                            <input  style={{ textTransform:'uppercase'}} className="form-control" placeholder="name or username" name={"search"} onChange={(e) => this.onSearchChange(e)} />
                            <span className="input-group-btn">
                                <button className="btn btn-primary btn-flat" onClick={(e) => this.onSearch(e)} >Search</button>
                            </span>
                        </div>
                    </div>
                </div>

                <br />

                <div className="row">

                    <div className="col-md-3">

                        <div className="box no-border">

                            <div className="box-header with-border">
                                <h3 className="box-title">Search by</h3>
                            </div>

                            <div className="box-body">

                                <div className="form-group">

                                    <label>
                                        User Status
                                    </label>
                                    <select className="form-control" name={"status"} value={this.state.status} onChange={(e) => this.onStatusChange(e)}>
                                        <option value="">--Select status--</option>
                                        {
                                            status
                                        }
                                    </select>

                                </div>
                                <br />

                                <div className="row">

                                    <div className="col-md-12">
                                        <button className="btn btn-primary" onClick={(e) => this.onFilter(e)} >Search</button>
                                        <button className="btn btn-info pull-right" onClick={(e) => this.onReset(e)} >Reset</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>


                    <div className="col-md-9">

                        <div className="box no-border">

                            <div className="box-body table-responsive no-padding">

                                <table className="table table-striped table-bordered">

                                    <thead>

                                        <tr className={"table-header-color"} >
                                            <th style={{ width: "10px" }} >#</th>
                                            <th style={{ width: "120px" }} >Name</th>
                                            <th style={{ width: "100px" }} >Username</th>
                                            <th style={{ width: "100px" }} >Mobile No</th>
                                            <th style={{ width: "170px" }} >Email</th>
                                            <th style={{ width: "90px" }} >Role</th>
                                            <th style={{ width: "60px" }} >Status</th>
                                        </tr>

                                    </thead>

                                    <tbody>

                                        {
                                            this.state.data.map((value, index) => {

                                                return (<tr key={index}  >

                                                    <td><div>{((this.state.pagination.activePage - 1) * 10) + (index + 1)}</div></td>
                                                    <td><a href='#' onClick={(e)=>this.onUserUpdate(e,value,index)} >{value.firstname} {value.lastname}</a></td>
                                                    <td><div>{value.username}</div></td>
                                                    <td><div>{(value.mobile_number != null) ? value.mobile_number : "not set"}</div></td>
                                                    <td><div>{(value.email != null) ? value.email : "not set"}</div></td>
                                                    <td><div>{value.roles}</div></td>
                                                    <td><div>
                                                        {(value.status == 10) ? <span className="label label-success">Active</span> : <span className="label label-danger">Inactive</span>}
                                                    </div></td>

                                                </tr>);

                                            })
                                        }



                                    </tbody>

                                </table>

                                <Pagination
                                    activePage={this.state.pagination.activePage}
                                    itemsCountPerPage={this.state.pagination.itemsCountPerPage}
                                    totalItemsCount={this.state.pagination.totalItemsCount}
                                    pageRangeDisplayed={this.state.pagination.pageRangeDisplayed}
                                    onChange={(e, p) => { this.onPageChange(e, p) }}
                                />

                            </div>

                            {this.state.progress == true ?
                                <div className="overlay">
                                    <i className="fa fa-refresh fa-spin"></i>
                                </div> : ""}
                        </div>

                    </div>

                </div>


                {this.state.add_status == true ? <UserModel user={this.state.users} roles={this.state.roles} userIndex={this.state.user_index} userAction={this.state.user_add_update} footer={true} onAddResult={(result) => this.onAddResult(result)} onUpdateResult={(data, index) => this.onUpdateResult(data, index)} onDelete={(index) => this.onDelete(index)} onClose={(e) => this.onModelClose(e)} ></UserModel> : ""}

            </div>

        );

    }

}

export default withRouter(User);