import React, { Component } from 'react';

class UserAdd extends Component{

    constructor(props){
        super(props);

        this.state={
            data:{
                first_name:"",
                last_name:"",
                mobile_no:"",
                email:"",
                username:"",
                password:"",
                confirm_password:"",
                status:"",
                role:""
            },

            error:{
                first_name:[],
                last_name:[],
                mobile_no:[],
                email:[],
                username:[],
                password:[],
                confirm_password:[],
                status:[],
                role:[]
            },

            empty:{
                first_name:[],
                last_name:[],
                mobile_no:[],
                email:[],
                username:[],
                password:[],
                confirm_password:[],
                status:[],
                role:[]
            },

            roles:[
                {
                    id:1,
                    name:"Admin",
                },
                {
                    id:2,
                    name:"Stock Manager",
                },
                {
                    id:3,
                    name:"Sales Man",
                },
            ]
        }
    }


    handleChange(e){
        this.state.data[e.target.name]=e.target.value;
    }

    onSubmit(e){

        e.preventDefault();
        let status = this.validate(this.state.data);
        if(status!=this.state.empty){
            this.setState({error:status})
        }else{
        }
    }

    validate(object, index){

        let rules={
            
            first_name:[
                {
                    type:"required",
                    message:"First name cannot be empty"
                },
                {
                    type:"string",
                    message:"First name must be string"
                }
            ],
            last_name:[                
                {
                    type:"string",
                    message:"Last name must be string"
                }
            ],
            mobile_no:[
                {
                    type:"required",
                    message:"Mobile number cannot be empty"
                },
                {
                    type:"number",
                    message:"Mobile number must be number"
                }
            ],
            email:[
                {
                    type:"required",
                    message:"Email cannot be empty"
                },
                {
                    type:"email",
                    message:"Enter valid email id"
                }
            ],
            username:[
                {
                    type:"required",
                    message:"Username cannot be empty"
                },
                {
                    type:"string",
                    message:"Username must be string"
                }
            ],
            password:[
                {
                    type:"required",
                    message:"Password cannot be empty"
                },
                {
                    type:"stringNumber",
                    message:"Password must be string"
                }
            ],
            confirm_password:[
                {
                    type:"required",
                    message:"Password cannot be empty"
                },
                {
                    type:"stringNumber",
                    message:"Password must be string"
                }
            ],
            status:[],
           
            role:[
                {
                    type:"required",
                    message:"Role cannot be empty"
                }                        
            ]             
            
        };

        let mresult= this.validateObject(rules,object)

        return mresult;

    }

    validateObject(rules,object){


        let error=[];

        Object.keys(object).forEach((value)=>{
            error[value]=[]
        });

        Object.keys(object).forEach((value)=>{

            if(Array.isArray(rules[value])){

                if(rules[value].length>0){

                    for(let i=0; i<rules[value].length; i++){

                        if(this.checking(rules[value][i], object[value])){
                        }else{
                            error[value].push(rules[value][i].message)
                        }

                    }
                }
            }

        });

        return error;

    }

    checking(rules,object){
        switch(rules.type){
            case "required":
            if(object != ""){
                return true;
            }else{
                return false;
            }
            break;

            case "number":
            let pattern = /^[0-9]+$/;
            if(object.match(pattern)){
                return true;
            }else{
                return false;
            }
            break;

            case "email":
            let epattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if(object.match(epattern)){
                return true;
            }else{
                return false;
            }
            break;

            case "numberDecimal":
            let npattern = /^[0-9.]+$/;
            if(object.match(npattern)){
                return true;
            }else{
                return false;
            }
            break;

            case "string":
            let spattern = /^[A-Za-z]+$/;
            if(object.match(spattern)){
                return true;
            }else{
                return false;
            }
            break;

            case "stringpassword":
            let ppattern = /^[A-Za-z!@#$%&*()_+=|<>?{}\\[\\]~-]+$/;
            if(object.match(ppattern)){
                return true;
            }else{
                return false;
            }
            break;

            case "stringNumber":
            let snpattern = /^[A-Za-z0-9]+$/;
            if(object.match(snpattern)){
                return true;
            }else{
                return false;
            }
        }
    }
   
    render(){
       
        return(
            
            <div>

                <div className="col-md-1"/>

                <div className="col-md-5">
                
                    <div className="box no-border">

                        <div className="box-header with-border">
                            <h3 className="box-title">Personal Details</h3>
                        </div>
                    
                        <div className="box-body">

                            <div className="row">
                                <div className="col-md-6">
                                    <div className={(this.state.error.first_name.length>0)?"form-group has-error":"form-group"}>
                                        <label>First name</label>                            
                                        <input  style={{ textTransform:'uppercase'}} className="form-control" name="first_name" onChange={(e)=>this.handleChange(e)}/>
                                        <div className="help-block">{(this.state.error.first_name.length>0)?this.state.error.first_name[0]:""}</div>
                                    </div>  
                                </div> 
                                <div className="col-md-6">                         
                                <div className={(this.state.error.last_name.length>0)?"form-group has-error":"form-group"}>
                                        <label>Last name</label>                            
                                        <input  style={{ textTransform:'uppercase'}} className="form-control" name="last_name" onChange={(e)=>this.handleChange(e)}/>
                                        <div className="help-block">{(this.state.error.last_name.length>0)?this.state.error.last_name[0]:""}</div>
                                    </div>
                                </div>
                            </div>                        
                            
                            <div className={(this.state.error.mobile_no.length>0)?"form-group has-error":"form-group"}>
                                <label>Mobile No</label>                            
                                <input className="form-control" type="number" name="mobile_no" onChange={(e)=>this.handleChange(e)}/>
                                <div className="help-block">{(this.state.error.mobile_no.length>0)?this.state.error.mobile_no[0]:""}</div>
                            </div>
                            <div className={(this.state.error.email.length>0)?"form-group has-error":"form-group"}>
                                <label>Email</label>                            
                                <input className="form-control" name="email" onChange={(e)=>this.handleChange(e)}/>
                                <div className="help-block">{(this.state.error.email.length>0)?this.state.error.email[0]:""}</div>
                            </div>
                            <div className={(this.state.error.role.length>0)?"form-group has-error":"form-group"}>
                                <label>Role</label>                            
                                <select className="form-control" name="role" onChange={(e)=>this.handleChange(e)}>
                                <option value="">Select Role</option>
                                {this.state.roles.map((d, index)=>{
                                    return(<option key={index} value={d.id}>{d.name}</option>)
                                })}
                                </select>
                                <div className="help-block">{(this.state.error.role.length>0)?this.state.error.role[0]:""}</div>
                            
                            </div>

                        </div>

                    </div>

                </div>

                

                <div className="col-md-5">
                
                    <div className="box no-border">

                        <div className="box-header with-border">
                            <h3 className="box-title">Login Details</h3>
                        </div>
                    
                        <div className="box-body">
                        
                        <div className={(this.state.error.username.length>0)?"form-group has-error":"form-group"}>
                                <label>Username</label>                            
                                <input  style={{ textTransform:'uppercase'}} className="form-control" name="username" onChange={(e)=>this.handleChange(e)}/>
                                <div className="help-block">{(this.state.error.username.length>0)?this.state.error.username[0]:""}</div>
                            
                            </div>                            
                            <div className={(this.state.error.password.length>0)?"form-group has-error":"form-group"}>
                                <label>Password</label>                            
                                <input className="form-control" name="password" onChange={(e)=>this.handleChange(e)}/>
                                <div className="help-block">{(this.state.error.password.length>0)?this.state.error.password[0]:""}</div>
                            
                            </div>
                            
                            <div className="row">
                            
                                <div className="col-md-12">
                                
                                    <button className="btn btn-primary pull-right" onClick={(e)=>this.onSubmit(e)}>ADD USER</button>

                                </div>

                            </div>
                            
                        </div>

                    </div>

                </div>

                <div className="col-md-1"/>

            </div>

        );

    }
}

export default UserAdd;