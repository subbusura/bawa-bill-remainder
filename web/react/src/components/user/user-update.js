import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';

import {buildURL, ROLE_LIST, USER_CREATE, USER_VIEW } from './../../constants/api';

export default class UserUpdate extends React.Component {

    constructor(props) {
        super(props);
        let params = this.props.match.params;
        this.state = {
            data: {
                first_name: "",
                last_name: "",
                mobile_no: "",
                email: "",
                username: "",
                password: "",
                confirm_password: "",
                status: 10,
                role: ""
            },
            user_id:params.id,
            error: {
                first_name: [],
                last_name: [],
                mobile_no: [],
                email: [],
                username: [],
                password: [],
                confirm_password: [],
                status: [],
                role: []
            },

            roles: [],
            status: [
                { id: 10, name: 'Active' },
                { id: 0, name: 'Inactive' },
            ]
        }
    }

    componentWillMount(){
        this.getInitialData();
        this.getUserData();
    }

    getUserData(){
        Axios.get(buildURL(USER_VIEW,{id:this.state.user_id}))
        .then((response) => {
            let mData = response.data;
                if (mData.statusCode === 200) {
                    //this.setState({ roles: response.data.data, progress: false })
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
        }).catch((error) => {
            this.errorAlert(error.response)
            this.setState({ progress: false });
        });
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message){
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    getInitialData(){
        Axios.get(buildURL(ROLE_LIST))
        .then((response) => {
            let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ roles: response.data.data, progress: false })
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
        }).catch((error) => {
            this.errorAlert(error.response)
            this.setState({ progress: false });
        });
    }

    handleChange(e){
        let mData = this.state.data;
        mData[e.target.name] = e.target.value;
        this.setState({data:mData});
    }

    onUpdate(e){
        Axios.post(buildURL(USER_CREATE),this.state.data)
        .then((response) => {

            let mData = response.data;
                if (mData.statusCode === 200) {
                    this.successAlert('Added successfully')
                    //this.setState({ data: response.data.data, data_filter: response.data.data, filteredData: mFilter, progress: false })
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
        }).catch((error) => {
            this.errorAlert(error.response)
            this.setState({ progress: false });
        });
    }

    render() {
        return (
            <div>
                <div className='row' >
                    <div className="col-md-6">
                        {this.renderPersonalDetails()}
                    </div>

                    <div className="col-md-6">
                        {this.renderLoginDetails()}
                    </div>
                </div >
                <div className='row' >
                    <div className='box no-border'>
                        <div className='box-body'>
                            <div className='col-md-12'>
                                <button className='btn btn-primary col-md-1 pull-right' onClick={(e)=>this.onUpdate(e)} >Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }

    renderPersonalDetails() {
        return (
            <div className="box no-border">

                <div className="box-header with-border">
                    <h3 className="box-title">Personal Details</h3>
                </div>

                <div className="box-body">

                    <div className="row">
                        <div className="col-md-6">
                            <div className={(this.state.error.first_name.length > 0) ? "form-group has-error" : "form-group"}>
                                <label>First name</label>
                                <input className="form-control" name="first_name" value={this.state.data.first_name} onChange={(e) => this.handleChange(e)} />
                                <div className="help-block">{(this.state.error.first_name.length > 0) ? this.state.error.first_name[0] : ""}</div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className={(this.state.error.last_name.length > 0) ? "form-group has-error" : "form-group"}>
                                <label>Last name</label>
                                <input className="form-control" name="last_name" value={this.state.data.last_name} onChange={(e) => this.handleChange(e)} />
                                <div className="help-block">{(this.state.error.last_name.length > 0) ? this.state.error.last_name[0] : ""}</div>
                            </div>
                        </div>
                    </div>

                    <div className={(this.state.error.mobile_no.length > 0) ? "form-group has-error" : "form-group"}>
                        <label>Mobile No</label>
                        <input className="form-control" type="number" name="mobile_no"  value={this.state.data.mobile_no} onChange={(e) => this.handleChange(e)} />
                        <div className="help-block">{(this.state.error.mobile_no.length > 0) ? this.state.error.mobile_no[0] : ""}</div>
                    </div>
                    <div className={(this.state.error.email.length > 0) ? "form-group has-error" : "form-group"}>
                        <label>Email</label>
                        <input className="form-control" name="email"  value={this.state.data.email} onChange={(e) => this.handleChange(e)} />
                        <div className="help-block">{(this.state.error.email.length > 0) ? this.state.error.email[0] : ""}</div>
                    </div>
                    <div className={(this.state.error.role.length > 0) ? "form-group has-error" : "form-group"}>
                        <label>Role</label>
                        <select className="form-control" name="role"  value={this.state.data.role} onChange={(e) => this.handleChange(e)}>
                            <option value="">Select Role</option>
                            {this.state.roles.map((d, index) => {
                                return (<option key={index} value={d}>{d}</option>)
                            })}
                        </select>
                        <div className="help-block">{(this.state.error.role.length > 0) ? this.state.error.role[0] : ""}</div>

                    </div>

                </div>
            </div>

        )
    }

    renderLoginDetails() {

        let status = this.state.status.map((item, index) => {
            return (<option key={index} value={item.id}>{item.name}</option>)
        })

        return (
            <div className="box no-border">

                <div className="box-header with-border">
                    <h3 className="box-title">Login Details</h3>
                </div>

                <div className="box-body">

                    <div className={(this.state.error.username.length > 0) ? "form-group has-error" : "form-group"}>
                        <label>Username</label>
                        <input className="form-control" name="username" value={this.state.data.username} onChange={(e) => this.handleChange(e)} />
                        <div className="help-block">{(this.state.error.username.length > 0) ? this.state.error.username[0] : ""}</div>

                    </div>
                    <div className={(this.state.error.password.length > 0) ? "form-group has-error" : "form-group"}>
                        <label>Password</label>
                        <input className="form-control" name="password" value={this.state.data.password} onChange={(e) => this.handleChange(e)} />
                        <div className="help-block">{(this.state.error.password.length > 0) ? this.state.error.password[0] : ""}</div>

                    </div>
                    <div className={(this.state.error.status.length > 0) ? "form-group has-error" : "form-group"}>
                        <label>Status</label>
                        <select className="form-control" name="status" value={this.state.data.status} onChange={(e) => this.handleChange(e)}>
                            {status}
                        </select>
                        <div className="help-block">{(this.state.error.status.length > 0) ? this.state.error.status[0] : ""}</div>

                    </div>
                </div>
            </div>
        )
    }

}