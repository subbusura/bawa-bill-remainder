import React from 'react';
import Axios from 'axios';
import { buildURL, USER_CREATE, USER_UPDATE, USER_DELETE } from '../../constants/api';
import Alert from 'react-s-alert';

export default class UserModel extends React.Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            progress: false,
            users: {},
            result: {
            },
            error: [],
            stock_count: {
            },
            isShow: true,
            status: [
                { id: 10, name: "Active" },
                { id: 0, name: "Inactive" }
            ],
            userData: {
                firstname: "",
                lastname: "",
                mobile_number: "",
                email: "",
                username: "",
                password: "",
                confirm_password: "",
                role: "",
                customer_group_id: "",
                customer_group_type: "all",
                status: 10,
                printer_id:'',
            },
            error: {
                firstname: [],
                lastname: [],
                mobile_number: [],
                email: [],
                username: [],
                password: [],
                confirm_password: [],
                status: [],
                customer_group_id: [],
                customer_group_type: [],
                role: [],
                printer_id:[],
            },
            roles: [],
            printers:[],
            retailer_group: [],
        };
        console.log(this.props.user);
    }

    componentDidMount() {

        
        this.setState({ users: this.props.user })
        let printers=window.__PRELOADED_STATE__.printers;

        if (this.props.userAction == "Update") {
            this.setState({
                printers:printers,
                userData: {
                    firstname: this.props.user.firstname,
                    lastname: this.props.user.lastname,
                    mobile_number: this.props.user.mobile_number,
                    email: this.props.user.email,
                    username: this.props.user.username,
                    status: this.props.user.status,
                    role: this.props.user.roles.length > 0 ? this.props.user.roles[0] : "",
                    customer_group_type: this.props.user.customer_group_type,
                    customer_group_id: (this.props.user.customer_group_id != null && this.props.user.customer_group_id != 0) ? this.props.user.customer_group_id.id : "",
                    status: this.props.user.status,
                    printer_id:this.props.user.printer_id
                }
            })
        }else{

             this.setState({
                 printers:printers
             });
        }
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    onHandleChange(e) {
        let user = this.state.userData;
        if (e.target.name == 'customer_group_type' && e.target.value == 'all') {
            user["customer_group_id"] = ""
        } else if (e.target.name === 'email') {
            user[e.target.name] = e.target.value;
        }else if (e.target.name === 'confirm_password' || e.target.name === 'password' ) {
            user[e.target.name] = e.target.value;
        } else {
            user[e.target.name] = e.target.value.toUpperCase();
        }
        this.setState({ userData: user, isDisabled: false })
    }

    onSubmit(e) {

        e.preventDefault();

        this.setState({
            error: {

                firstname: [],
                lastname: [],
                mobile_number: [],
                email: [],
                username: [],
                password: [],
                confirm_password: [],
                status: [],
                customer_group_id: [],
                customer_group_type: [],
                role: []
            }, isDisabled: true
        })

        if (this.state.userData.password == this.state.userData.confirm_password) {

            if (this.props.userAction == "Add") {

                this.onResetError();

                Axios.post(buildURL(USER_CREATE), this.state.userData).
                    then((response) => {
                        let mData = response.data;
                        if (mData.statusCode === 200) {
                            this.setState({ isShow: false, isDisabled: false });
                            this.successAlert("Added successfully");
                            this.props.onAddResult(response.data.data)
                        } else if (mData.statusCode === 500) {
                            this.errorAlert('Internal Server Error')
                            this.setState({ progressStatus: false, isDisabled: false });
                        } else if (mData.statusCode === 401) {
                            this.errorAlert('Unauthorized access');
                            this.setState({ progressStatus: false, isDisabled: false });
                        } else if (mData.statusCode === 403) {
                            this.errorAlert('Unauthorized access');
                            this.setState({ progressStatus: false, isDisabled: false });
                        } else if (mData.statusCode === 404) {
                            this.errorAlert('Page not found');
                            this.setState({ progressStatus: false, isDisabled: false });
                        } else if (mData.statusCode === 422) {
                            this.setError(mData.error);
                            this.setState({ progressStatus: false, isDisabled: false });
                        }
                    }).catch((error) => {
                        this.setState({ progressStatus: false, isDisabled: false });
                    });


            } else {
                this.onResetError();

                Axios.post(buildURL(USER_UPDATE, { id: this.props.user.id }), this.state.userData).
                    then((response) => {
                        let mData = response.data;
                        if (mData.statusCode === 200) {
                            this.setState({ isShow: false, isDisabled: false });
                            this.successAlert("Updated successfully");
                            this.props.onAddResult(response.data.data)
                        } else if (mData.statusCode === 500) {
                            this.errorAlert('Internal Server Error')
                            this.setState({ progressStatus: false, isDisabled: false });
                        } else if (mData.statusCode === 401) {
                            this.errorAlert('Unauthorized access');
                            this.setState({ progressStatus: false, isDisabled: false });
                        } else if (mData.statusCode === 403) {
                            this.errorAlert('Unauthorized access');
                            this.setState({ progressStatus: false, isDisabled: false });
                        } else if (mData.statusCode === 404) {
                            this.errorAlert('Page not found');
                            this.setState({ progressStatus: false, isDisabled: false });
                        } else if (mData.statusCode === 422) {
                            this.setError(mData.error);
                            this.setState({ progressStatus: false, isDisabled: false });
                        }

                    }).catch((error) => {
                        this.setState({ progressStatus: false, isDisabled: false });
                    });

            }
        } else {

            this.setState({
                error: {
                    firstname: [],
                    lastname: [],
                    mobile_number: [],
                    email: [],
                    username: [],
                    password: ["Password is not match"],
                    confirm_password: ["Password is not match"],
                    status: [],
                    role: [],
                    customer_group_id: [],
                    customer_group_type: [],

                },
                isDisabled: false
            })
        }
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    onResetError() {
        let mError = this.state.error;
        Object.keys(mError).forEach((value) => {
            mError[value] = []
        });
        this.setState({ error: mError });
    }

    setError(error) {
        let mError = this.state.error;
        Object.keys(error).forEach((value) => {
            mError[value] = error[value]
        });
        this.setState({ error: mError });
    }

    onDelete(e) {

        let mdelete = window.confirm("Confirm..! \n Do you want to delete this user");
        if (mdelete == true) {

            this.setState({ isDisabled: true });

            Axios.post(buildURL(USER_DELETE, { id: this.props.user.id })).
                then((response) => {

                    let mData = response.data;
                    if (mData.statusCode === 200) {
                        this.successAlert("Deleted successfully");
                        this.setState({ isShow: false, isDisabled: false });
                        this.props.onDelete(this.props.userIndex, );
                    } else if (mData.statusCode === 500) {
                        this.errorAlert('Internal Server Error')
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 401) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 403) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 404) {
                        this.errorAlert('Page not found');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 422) {
                        this.setError(mData.error);
                    }

                }).catch(() => {

                    this.setState({ isDisabled: true });

                });

        }
    }


    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }


    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    onClose(e) {

        this.setState({ isShow: false })
        this.props.onClose();

    }

    render() {

           
        let lPrinters=this.state.printers.map((d, index) => {
            return (<option key={index} value={d.id}>{d.name}</option>)
        })

        return (
            <div className={this.state.isShow ? 'modal fade in' : 'modal fade'} style={this.state.isShow ? { display: 'block', paddingRight: '16px' } : { display: 'none' }} >

                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">

                            <button type="button" className="close" onClick={(e) => this.onClose(e)} data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>

                        </div>
                        <div className="modal-body">

                            <div className="row">

                                <div className="col-md-6" >

                                    <div >

                                        <div className="box-header no-border">
                                            <h3 className="box-title">Personal Details</h3>
                                        </div>

                                        <div className="box-body">

                                            <div className="row">
                                                <div className="col-md-6">
                                                    <div className={(this.state.error.firstname.length > 0) ? "form-group has-error" : "form-group"}>
                                                        <label>First name</label>
                                                        <input style={{ textTransform: 'uppercase' }} className="form-control" value={this.state.userData.firstname} name="firstname" onChange={(e) => this.onHandleChange(e)} />
                                                        <div className="help-block">{(this.state.error.firstname.length > 0) ? this.state.error.firstname[0] : ""}</div>
                                                    </div>

                                                </div>

                                                <div className="col-md-6">
                                                    <div className={(this.state.error.lastname.length > 0) ? "form-group has-error" : "form-group"}>
                                                        <label>Last name</label>
                                                        <input style={{ textTransform: 'uppercase' }} className="form-control" defaultValue={(this.props.user != null) ? this.props.user.lastname : ""} name="lastname" onChange={(e) => this.onHandleChange(e)} />
                                                        <div className="help-block">{(this.state.error.lastname.length > 0) ? this.state.error.lastname[0] : ""}</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className={(this.state.error.mobile_number.length > 0) ? "form-group has-error" : "form-group"}>
                                                <label>Mobile No</label>
                                                <input className="form-control" type="number" defaultValue={(this.props.user != null) ? this.props.user.mobile_number : ""} name="mobile_number" onChange={(e) => this.onHandleChange(e)} />
                                                <div className="help-block">{(this.state.error.mobile_number.length > 0) ? this.state.error.mobile_number[0] : ""}</div>
                                            </div>

                                            <div className={(this.state.error.email.length > 0) ? "form-group has-error" : "form-group"}>
                                                <label>Email</label>
                                                <input className="form-control" name="email" defaultValue={(this.props.user != null) ? this.props.user.email : ""} onChange={(e) => this.onHandleChange(e)} />
                                                <div className="help-block">{(this.state.error.email.length > 0) ? this.state.error.email[0] : ""}</div>
                                            </div>

                                            <div className={(this.state.error.role.length > 0) ? "form-group has-error" : "form-group"}>
                                                <label>Role</label>
                                                <select className="form-control" name="role" defaultValue={this.props.user.roles} onChange={(e) => this.onHandleChange(e)}>
                                                    <option value="">Select Role</option>
                                                    {this.props.roles.map((d, index) => {
                                                        return (<option key={d} value={d}>{d}</option>)
                                                    })}
                                                </select>
                                                <div className="help-block">{(this.state.error.role.length > 0) ? this.state.error.role[0] : ""}</div>
                                            </div>
                                            <div className={"form-group"}>
                                                <label>Default Printer</label>
                                                <select className="form-control" name="printer_id" value={this.props.user.printer_id} onChange={(e) => this.onHandleChange(e)}>
                                                    <option value="">Select Printer</option>
                                                   {lPrinters}
                                                </select>
                                               
                                            </div>

                                        </div>

                                    </div>

                                </div>


                                <div className="col-md-6">

                                    <div>

                                        <div className="box-header no-border">
                                            <h3 className="box-title">Login Details</h3>
                                        </div>

                                        <div className="box-body">

                                            <div className={(this.state.error.username.length > 0) ? "form-group has-error" : "form-group"}>
                                                <label>Username</label>
                                                <input style={{ textTransform: 'uppercase' }} className="form-control" name="username" defaultValue={(this.props.user != null) ? this.props.user.username : ""} onChange={(e) => this.onHandleChange(e)} />
                                                <div className="help-block">{(this.state.error.username.length > 0) ? this.state.error.username[0] : ""}</div>

                                            </div>

                                            <div className={(this.state.error.password.length > 0) ? "form-group has-error" : "form-group"}>
                                                <label>Password</label>
                                                <input className="form-control" name="password" onChange={(e) => this.onHandleChange(e)} />
                                                <div className="help-block">{(this.state.error.password.length > 0) ? this.state.error.password[0] : ""}</div>

                                            </div>

                                            <div className={(this.state.error.confirm_password.length > 0) ? "form-group has-error" : "form-group"}>
                                                <label>Confirm Password</label>
                                                <input className="form-control" name="confirm_password" onChange={(e) => this.onHandleChange(e)} />
                                                <div className="help-block">{(this.state.error.confirm_password.length > 0) ? this.state.error.confirm_password[0] : ""}</div>

                                            </div>

                                            <div className={(this.state.error.status.length > 0) ? "form-group has-error" : "form-group"}>
                                                <label>Status</label>
                                                <select className="form-control" name="status" defaultValue={(this.props.user != null) ? this.props.user.status : 0} onChange={(e) => this.onHandleChange(e)}>
                                                    {this.state.status.map((d, index) => {
                                                        return (<option key={index} value={d.id}>{d.name}</option>)
                                                    })}
                                                </select>
                                                <div className="help-block">{(this.state.error.status.length > 0) ? this.state.error.status[0] : ""}</div>

                                            </div>
                                            {
                                                (this.state.userData.customer_group_type == "all" || this.state.userData.customer_group_type == null || this.state.userData.customer_group_type == "") ? "" :
                                                    <div className={(this.state.error.customer_group_id.length > 0) ? "form-group has-error" : "form-group"}>
                                                        <label>Retailer Group</label>
                                                        <select className="form-control" name="customer_group_id" defaultValue={this.state.userData.customer_group_id} onChange={(e) => this.onHandleChange(e)}>
                                                            <option value="">Select Group</option>
                                                            {this.props.retailerGroup.map((d, index) => {
                                                                return (<option key={index} value={d.id}>{d.name}</option>)
                                                            })}
                                                        </select>
                                                        <div className="help-block">{(this.state.error.customer_group_id.length > 0) ? this.state.error.customer_group_id[0] : ""}</div>
                                                    </div>
                                            }



                                        </div>

                                    </div>

                                </div>

                            </div>
                            {

                                (this.props.footer == true) ? <div className="modal-footer">
                                    {this.props.userAction == "Update" ?
                                        <button className="btn btn-danger pull-left" onClick={(e) => this.onDelete(e)} disabled={this.state.isDisabled}>Delete</button>
                                        : ""}
                                    <button className="btn btn-primary" onClick={(e) => this.onSubmit(e)} disabled={this.state.isDisabled}>{this.props.userAction}</button>
                                </div> : ''

                            }

                        </div>

                    </div>

                </div>

            </div>
        );
    }

}