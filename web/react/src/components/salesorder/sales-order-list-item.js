import React from 'react';
import Axios from 'axios';
import Alert from 'react-s-alert';

import { buildURL, SALES_ORDER_DELETE } from './../../constants/api';

export default class SalesOrdersListItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            order: {
                name: '',
                mobile_number: '',
                advance_payment: '',
                items: [],
                image: '',
            },
        }
    }

    componentDidMount() {
        this.setState({ order: this.props.item })
    }

    onDelete(e) {
        e.preventDefault();
        if (window.confirm('Do you want to remove this order?')) {

            Axios.delete(buildURL(SALES_ORDER_DELETE, { order_id: this.state.order.id }))
                .then((response) => {
                    let mData = response.data;
                    if (mData.statusCode === 200) {
                        this.props.onDeleteSuccess();
                    } else if (mData.statusCode === 500) {
                        this.errorAlert('Internal Server Error')
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 401) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 403) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 404) {
                        this.errorAlert('Page not found');
                        this.setState({ progressStatus: false });
                    }
                })
                .catch((error) => {
                    this.errorAlert(error);
                });
        }
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    render() {
        return (

            <div className='box'>
                <div className="box-header with-border">
                    
                    <div className='row'>
                        <h3 className='col-md-6 ' style={{ fontSize: '14px', marginTop: '-3px', marginBottom: '-3px' }} >Order No : {this.state.order.id}</h3>
                        <h3 className='col-md-6 ' style={{ fontSize: '14px', marginTop: '-3px', marginBottom: '-3px' }} >Date : {this.state.order.created_at}</h3>
                    </div>
                </div>
                <div className='box-body' >
                    <div className='row'>

                        <h3 className='col-md-12' style={{ fontSize: '12px', marginTop: '-3px' }} >Name : {this.state.order.name}</h3>
                        <h3 className='col-md-12' style={{ fontSize: '12px', marginTop: '-3px' }} >Phone No : {this.state.order.mobile_number}</h3>
                        <h3 className='col-md-12' style={{ fontSize: '12px', marginTop: '-3px' }} >Advance : {this.state.order.advance_payment}</h3>
                    </div>
                    <div className="row">

                        
                        
                    </div>
                    <div className='row'>
                        <div className='col-md-12' style={{maxHeight: '180px' }}>
                            {this.renderTable()}
                        </div>
                    </div>
                </div>
                <div className='box-footer'>
                    <div className='row'>
                        <h3 className='col-md-5' style={{ fontSize: '14px', marginTop: '-3px', marginBottom: '-3px' }} ><a href={'/order/update/' + this.state.order.id} className='pull-right'><i className='fa fa-pencil' /> Edit</a></h3>
                    </div>
                </div>
            </div>

        )
    }

    renderTable() {
        let items = this.state.order.items.map((item, index) => {
            return (
                <tr key={index} >
                    <td style={{ fontSize: '12px' }} >{item.product_name}</td>
                    <td style={{ fontSize: '12px' }} >{item.qty}</td>
                </tr>
            )
        })
        return (
            <table className='table' style={{ maxHeight: '10px' }}  >
                <thead>
                    <tr>
                        <th style={{ fontSize: '12px' }} >Product Name</th>
                        <th style={{ fontSize: '12px' }} >Quantity</th>
                    </tr>
                </thead>
                <tbody>{items}</tbody>
            </table>
        )
    }

}