import React from 'react';
import Axios from 'axios';
import Loader from './../ui/loader';
import Pagination from 'react-js-pagination';
import Alert from 'react-s-alert';
import { Link } from 'react-router-dom';

import { PRINTER_LIST, buildURL, PRINTER_DELETE } from './../../constants/api';

export default class PrinterList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            printer_list: [],
            progressStatus: true,
            page: {
                totalCount: '1',
                pageCount: '1',
                currentPage: '1',
                perPage: '50'
            },
            filter: {
                name: '',
                printer_queue: '',
                status: '',

            }
        }
    }

    componentWillMount() {
        this.getPrinterList(this.state.page.currentPage);
    }

    getPrinterList(page) {
        Axios.get(buildURL(PRINTER_LIST, { page: page, 'per-page': this.state.page.perPage }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ printer_list: mData.data.items, progressStatus: false, page: mData.data._meta });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    onPageChange(page) {

        let mPage = this.state.page;
        mPage.currentPage = page;
        this.getPrinterList(page);
        this.setState({ page: mPage });
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    onDeleteItem(e, item) {
        e.preventDefault();
        
        if (window.confirm('Do you want to delete this Printer?')) {
            Axios.delete(buildURL(PRINTER_DELETE, { id: item.id }))
                .then((response) => {
                    let mData = response.data;
                    if (mData.statusCode === 204) {
                        this.successAlert("Deleted successfully");
                        this.getPrinterList(this.state.page.currentPage);
                    } else if (mData.statusCode === 500) {
                        this.errorAlert('Internal Server Error')
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 401) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 403) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 404) {
                        this.errorAlert('Page not found');
                        this.setState({ progressStatus: false });
                    }
                })
                .catch((error) => {
                    this.errorAlert(error);
                    this.setState({ progressStatus: false });
                });
        }

        return false;
    }

    onFilterChange(e) {
        let mFilter = this.state.filter;
        let mPage = this.state.page;

        if (e.target.name === 'perPage') {
            mPage[e.target.name] = e.target.value;
        } else {
            mFilter[e.target.name] = e.target.value;
        }

        this.setState({ filter: mFilter, page: mPage });

    }

    onFilter(e) {
        e.preventDefault();
        Axios.post(buildURL(PRINTER_LIST, { page: this.state.page.currentPage, 'per-page': this.state.page.perPage }), this.state.filter)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ product_list: mData.data.items, progressStatus: false, page: mData.data._meta });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    getProgress() {
        return (
            <div className='box no-border' style={{opacity:'1',position:'relative', height:'100%', width:'100%'}} >
                <div className='overlay' style={{position:'absolute', left:'50%', top:'50%', right:'50%' }} >
                    <Loader />
                </div>
            </div>
        )
    }

    render() {
        return (
            <div>
                <div className='row'>
                    <div className='col-md-2'>
                        <Link className='btn btn-success' to='/printer/create'>Add New Printer</Link>
                    </div>
                </div>

                <div className='row'>
                    <br />
                    <div className='col-md-3'>
                        {this.renderFilter()}
                    </div>
                    <div className='col-md-9'>

                        {
                             this.state.progressStatus ? this.getProgress() : this.renderTable()
                        }
                        {
                            this.state.printer_list.length > 0 ?
                                <Pagination
                                    activePage={this.state.page.currentPage}
                                    itemsCountPerPage={this.state.page.perPage}
                                    totalItemsCount={this.state.page.totalCount}
                                    pageRangeDisplayed={parseInt(this.state.page.pageCount) <=10 ? this.state.page.pageCount : 10}
                                    onChange={(e, p) => { this.onPageChange(e, p) }}
                                />
                                : ""
                        }
                    </div>
                </div>

            </div>
        )
    }

    renderTable() {

        let printers = this.state.printer_list.map((item, index) => {
            return (
                <tr key={index}>

                    <td>{((this.state.page.currentPage - 1) * this.state.page.perPage) + (index + 1)}</td>
                    <td>{item.name}</td>
                    <td>{item.printer_queue}</td>
                    <td>
                        {
                            <div>
                               
                                <a href='#'><span className='fa  fa-trash-o' style={{ marginLeft: '10px' }} onClick={(e) => this.onDeleteItem(e, item)} ></span></a>
                            </div>
                        }
                    </td>
                </tr>
            )
        })

        return (
            <div className='box no-border'>
                <div className='box-body no-padding'>

                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr className={"table-header-color"} >
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Printer ID</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>{printers}</tbody>
                    </table>
                </div>
            </div>
        )
    }

    renderFilter() {
        return (
            <div className='box box-info'>
                <div className='box-header with-border'>
                    <h4 className='box-title'>Search</h4>
                </div>
                <div className='box-body'>
                    <div className='form-group'>
                        <label>Printer Per Page</label>
                        <input className='form-control' type='number'  style={{ textTransform:'uppercase'}} name='perPage' value={this.state.page.perPage} onChange={(e) => this.onFilterChange(e)} />
                    </div>
                    <div className='form-group'>
                        <label>Printer Name</label>
                        <input className='form-control' style={{ textTransform:'uppercase'}} name='name' value={this.state.filter.name} onChange={(e) => this.onFilterChange(e)} />
                    </div>
                    <div className='form-group'>
                        <label>Product ID</label>
                        <input className='form-control' style={{ textTransform:'uppercase'}} name='printer_queue' value={this.state.filter.printer_queue} onChange={(e) => this.onFilterChange(e)} />
                    </div>
                </div>

                <div className='box-footer'>
                    <button className='btn btn-info pull-right' onClick={(e) => this.onFilter(e)} >Search</button>
                </div>

            </div>
        )
    }

}