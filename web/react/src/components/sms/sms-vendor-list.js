import React from 'react';
import Axios from 'axios';
import Loader from './../ui/loader';
import Alert from 'react-s-alert';
import { Link } from 'react-router-dom';

import { buildURL, SMS_VENDOR_LIST, SMS_VENDOR_DELETE } from './../../constants/api';

export default class SMSVendorList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

            product_list: [],
            progressStatus: true,
            filter: {
                product_code: '',
                product_name: '',
                division_code: '',

            }
        }
    }

    componentWillMount() {
        this.getProductList();
    }

    getProductList() {
        Axios.get(buildURL(SMS_VENDOR_LIST))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ product_list: mData.data, progressStatus: false });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    onDeleteItem(e, item) {
        e.preventDefault();

        if (window.confirm('Do you want to delete this SMS Vendor?')) {
            Axios.delete(buildURL(SMS_VENDOR_DELETE, { id: item.id }))
                .then((response) => {
                    let mData = response.data;
                    if (response.status === 200) {
                        this.successAlert("Deleted successfully");
                        this.getProductList();
                    } else if (mData.statusCode === 500) {
                        this.errorAlert('Internal Server Error')
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 401) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 403) {
                        this.errorAlert('Unauthorized access');
                        this.setState({ progressStatus: false });
                    } else if (mData.statusCode === 404) {
                        this.errorAlert('Page not found');
                        this.setState({ progressStatus: false });
                    }
                })
                .catch((error) => {
                    this.errorAlert(error);
                    this.setState({ progressStatus: false });
                });
        }

        return false;
    }

    onFilterChange(e) {
        let mFilter = this.state.filter;
        let mPage = this.state.page;

        if (e.target.name === 'perPage') {
            mPage[e.target.name] = e.target.value;
        } else {
            mFilter[e.target.name] = e.target.value;
        }

        this.setState({ filter: mFilter, page: mPage });

    }

    onFilter(e) {
        e.preventDefault();
        Axios.post(buildURL(SMS_VENDOR_LIST), this.state.filter)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ product_list: mData.data, progressStatus: false });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ progressStatus: false });
            });
    }

    getProgress() {
        return (
            <div className='box no-border' style={{ opacity: '1', position: 'relative', height: '100%', width: '100%' }} >
                <div className='overlay' style={{ position: 'absolute', left: '50%', top: '50%', right: '50%' }} >
                    <Loader />
                </div>
            </div>
        )
    }

    render() {
        return (
            <div>
                <div className='container'>
                    <div className='col-md-10 col-md-offset-1'>
                        <div className='row'>
                            <div className='col-md-12'>
                                <Link className='btn btn-success' to='/sms/vendor/create'>Add SMS Vendor</Link>
                            </div>
                            <div className='col-md-12'>
                                {
                                    this.state.progressStatus ? this.getProgress() : this.renderTable()
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderTable() {

        let product = this.state.product_list.map((item, index) => {
            return (
                <tr key={index}>

                    <td>{index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.url}</td>
                    <td>{parseInt(item.is_default)===1 ? <span className='label label-success'>Active</span> : <span className='label label-danger'>Inactive</span>}</td>
                    <td>
                        {
                            <div>
                                <Link to={'/sms/vendor/update/' + item.id}><span className='fa  fa-pencil'></span></Link>
                                <a href='#'><span className='fa  fa-trash-o' style={{ marginLeft: '10px' }} onClick={(e) => this.onDeleteItem(e, item)} ></span></a>
                            </div>
                        }
                    </td>
                </tr>
            )
        })

        return (
            <div className='box no-border'>
                <div className='box-body no-padding'>

                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr className={"table-header-color"} >
                                <th>S.No</th>
                                <th>Name</th>
                                <th>URL</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>{product}</tbody>
                    </table>
                </div>
            </div>
        )
    }

    renderFilter() {
        return (
            <div className='box box-info'>
                <div className='box-header with-border'>
                    <h4 className='box-title'>Search</h4>
                </div>
                <div className='box-body'>
                    <div className='form-group'>
                        <label>Product Per Page</label>
                        <input className='form-control' name='perPage' value={this.state.page.perPage} onChange={(e) => this.onFilterChange(e)} />
                    </div>
                    <div className='form-group'>
                        <label>Product Code</label>
                        <input className='form-control' name='product_code' value={this.state.filter.product_code} onChange={(e) => this.onFilterChange(e)} />
                    </div>
                    <div className='form-group'>
                        <label>Product Name</label>
                        <input className='form-control' name='product_name' value={this.state.filter.product_name} onChange={(e) => this.onFilterChange(e)} />
                    </div>
                    <div className='form-group'>
                        <label>Division Code</label>
                        <input className='form-control' name='division_code' value={this.state.filter.division_code} onChange={(e) => this.onFilterChange(e)} />
                    </div>
                </div>

                <div className='box-footer'>
                    <button className='btn btn-info pull-right' onClick={(e) => this.onFilter(e)} >Search</button>
                </div>

            </div>
        )
    }

}