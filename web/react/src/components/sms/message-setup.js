import React from 'react';
import { Link } from 'react-router-dom';

import SMSMessage from './sms-message';
import MessageList from './message-list';

export default class MessageSetup extends React.Component {

    constructor(props) {
        super(props);

        let type = this.props.match.params.type;

        if (typeof type === 'undefined') {
            type = 'sms';
        }

        this.state = {
            activeMenu: type,
            alert_view: [],
            alertType: 'NEW_ORDER',
            variables: [
                '{ORDER_NUMBER}', '{CUSTOMER_NAME}','{ADVANCE_PAYMENT}', '{ORDERED_PRODUCTS}'
            ]
        }
    }

    componentWillReceiveProps(nextPros) {

        let { match } = nextPros;
        if (match.path === "/configuration/notification/email") {
            this.setState({ activeMenu: "email" });
        } else {
            this.setState({ activeMenu: match.params.type });
        }
    }

    setMessageView(result) {
        this.setState({ alert_view: result })
    }
    setType(result) {
        this.setState({ alertType: result })
    }

    render() {
        return (
            <div>
                <div className='row'>
                </div>
                <div className='row'>
                    <div className='col-md-3'>
                        <MessageList message={(result) => this.setMessageView(result)} type={(result) => this.setType(result)} />
                    </div>
                    <div className='col-md-6'>

                        {this.state.activeMenu === "sms" ? <SMSMessage message={this.state.alert_view} type={this.state.alertType} /> : ""}

                    </div>
                    <div className='col-md-3'>{this.renderInfo()}</div>
                </div>
            </div >
        )
    }

    renderInfo() {
        return (
            <div className='box no-border'>
                <div className='box-header'>
                    <h4>Templates Variables</h4>
                </div>
                <div className='box-body'>
                    {
                        this.state.variables.map((item, index) => {
                            return (
                                <li key={index} >{item}</li>
                            )
                        })
                    }

                </div>
            </div>
        )
    }

}