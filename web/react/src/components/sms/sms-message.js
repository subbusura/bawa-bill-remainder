import React from 'react';
import Axios from 'axios';
import { buildURL, MESSAGE_ALERT_UPDATE } from './../../constants/api';

export default class SMSMessage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            alert_view: {
                id: "",
                sms_type: "",
                content: "",
            },
            alert_type: '',
            isDisabled: true,
        }
    }

    componentDidMount() {
        if (this.props.message !== null) {
            this.setupMessage(this.props.message, this.props.type);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.message !== null) {
            this.setupMessage(nextProps.message, nextProps.type);
        }
    }

    setupMessage(message, alertType) {
        let alertMsg = this.state.alert_view;

        Object.keys(alertMsg).forEach((key) => {
            if (key === 'sms_type') {
                alertMsg[key] = alertType;
            } else {
                alertMsg[key] = message[key];
            }

        });
        if (message.length <= 0) {
            Object.keys(alertMsg).forEach((key) => {
                if (key === 'sms_type') {
                    alertMsg[key] = alertType;
                } else {
                    alertMsg[key] = "";
                }
            });
        }

        // for (let i = 0; i < message.length; i++) {
        //     Object.keys(alertMsg).forEach((key) => {
        //         alertMsg[key] = message[i][key];
        //     })
        // }
        this.setState({ alert_view: alertMsg, alert_type: alertType, isDisabled: true });
    }

    onHandleChange(e) {
        e.preventDefault();
        let alertMsg = this.state.alert_view;
        alertMsg[e.target.name] = e.target.value.toUpperCase();
        this.setState({ alert_view: alertMsg, isDisabled: false });
    }

    onClear(e) {
        e.preventDefault();
        let alertMsg = this.state.alert_view;
        Object.keys(alertMsg).forEach((key) => {
            alertMsg[key] = '';
        });
        this.setState({ alert_view: alertMsg, isDisabled: true });
    }

    onUpdate(e) {
        e.preventDefault();
        this.setState({ isDisabled: true });
        let data = {
            type: this.state.alert_type,
            temp_type: 'sms'
        }
        Axios.put(buildURL(MESSAGE_ALERT_UPDATE, data), this.state.alert_view)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    alert("Updated successfully");
                } else if (mData.statusCode === 500) {
                    alert("Server error");
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 401) {
                    alert("Unauthorized access");
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 403) {
                    alert("Unauthorized access");
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 404) {
                    alert("Page not found");
                    this.setState({ progressStatus: false, isDisabled: false });
                }
            })
            .catch((error) => {
                alert(error);
                this.setState({ progressStatus: false, isDisabled: false });
            })
    }

    render() {
        return (
            <div>
                <div className='row'>
                    <div className='col-md-12'>
                        {this.renderForm()}
                    </div>
                </div>
            </div>
        )
    }

    renderForm() {
        return (
            <div>
                <div className='box no-border'>
                    <div className='box-header'></div>
                    <div className='box-body'>
                        <div className='form-horizontal'>
                            <div className='form-group'>
                                <label className='col-sm-3 control-label' >Content : </label>
                                <div className='col-sm-9'>
                                    <textarea
                                        name='content'
                                        className='form-control'
                                        style={{ height: "150px", resize: 'none', textTransform:'uppercase' }}
                                        value={this.state.alert_view.content}
                                        onChange={(e) => this.onHandleChange(e)}
                                        maxLength='250'
                                    />
                                </div>
                                <div className='help-block'>
                                    <div className='col-md-12'> <h5 style={{textAlign:'end'}}>  Max 250 Characters </h5></div>
                                </div>
                            </div>

                        </div>
                        <br />
                        <div >
                            <div className='col-md-3 pull-right'>
                                <button className='btn btn-success col-md-12' onClick={(e) => this.onUpdate(e)} disabled={this.state.isDisabled} >Update</button>
                            </div>
                            <div className='col-md-3 pull-right '>
                                <button className='btn btn-default col-md-12' onClick={(e) => this.onClear(e)}>Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}