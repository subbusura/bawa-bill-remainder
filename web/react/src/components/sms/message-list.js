import React from 'react';
import Axios from 'axios';
import { buildURL, MESSAGE_ALERT_TYPES, MESSAGE_ALERT_VIEW } from './../../constants/api';
import index from 'axios';

export default class MessageList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            alert_type: [],
            progressStatus: true,
            alert_view: [],
            type: 'NEW_ORDER',
        }
    }

    componentWillMount() {
        this.getInitialData();
        this.getNotificationView(this.state.type);
    }

    getInitialData() {
        Axios.get(buildURL(MESSAGE_ALERT_TYPES))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ alert_type: mData.data, progressStatus: false });
                } else if (mData.statusCode === 500) {
                    alert("Server error");
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    alert("Unauthorized access");
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    alert("Unauthorized access");
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    alert("Page not found");
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                alert(error);
                this.setState({ progressStatus: false });
            })
    }

    onGetData(e, type) {
        e.preventDefault();
        this.props.type(type);
        this.props.message([])
        this.setState({ progressStatus: true, type: type });
        this.getNotificationView(type);
        
    }

    getNotificationView(type){
        let filter = {
            type: type
        }
        Axios.get(buildURL(MESSAGE_ALERT_VIEW, filter))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.props.message(mData.data)
                    this.setState({ alert_view: mData.data, progressStatus: false });
                } else if (mData.statusCode === 500) {
                    alert("Server error");
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 401) {
                    alert("Unauthorized access");
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 403) {
                    alert("Unauthorized access");
                    this.setState({ progressStatus: false });
                } else if (mData.statusCode === 404) {
                    alert("Page not found");
                    this.setState({ progressStatus: false });
                }
            })
            .catch((error) => {
                alert(error);
                this.setState({ progressStatus: false });
            })

    }

    getProgress() {
        return (
            <div className='overlay'>
                <i className='fa fa-refresh fa-spin' />
            </div>
        )
    }

    render() {

        let progress = this.state.progressStatus ? this.getProgress() : "";

        return (
            <div>
                <div className='box no-border'>
                    <div className='box-header'>
                        <h3 style={{ fontSize: "15px" }} ><strong>Alert Types</strong></h3>
                    </div>
                    <div className='box-body'>
                        {
                            this.state.alert_type.map((item, index) => {
                                return (
                                    <div key={index} className='box no-border' style={{ background: item.value === this.state.type ? "#00a65a" : "#bdbdbd" }} >
                                        <a href='#' onClick={(e) => this.onGetData(e, item.value)} ><div className='box-body'>
                                            <h4 style={{ fontSize: "15px", color:"#ffffff" }} >{item.name}</h4>
                                        </div>
                                        </a>
                                    </div>
                                )
                            })
                        }
                    </div>
                    {progress}
                </div>
            </div>
        )
    }
}