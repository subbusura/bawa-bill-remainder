import React from 'react';
import Axios from 'axios';
import Loader from './../ui/loader';
import Alert from 'react-s-alert';
import { Link } from 'react-router-dom';

import { PRODUCT_CREATE, buildURL, PRODUCT_VIEW, PRODUCT_UPDATE, SMS_VENDOR_CREATE, SMS_VENDOR_UPDATE, SMS_VENDOR_VIEW } from './../../constants/api';

export default class SMSVendorCreate extends React.Component {

    constructor(props) {
        super(props);

        let params = this.props.match.params;
        this.state = {
            product: {
                name: "",
                url: "",
                is_default: 0
            },
            error: {
                name: [],
                url: [],
                is_default: []
            },
            id: params.id,
            update_status: false,
            isDisabled: true,
            status: [
                { id: 0, name: 'Inactive' },
                { id: 1, name: 'Active' },
            ]
        }
    }

    componentDidMount() {
        if (typeof this.state.id !== 'undefined') {
            this.getProduct();
            this.setState({ update_status: true });
        } else {
            this.setState({ update_status: false });
        }
    }

    getProduct() {
        Axios.get(buildURL(SMS_VENDOR_VIEW, { id: this.state.id }))
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setProduct(mData.data);
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error);
                } else if (mData.statusCode === 500) {
                    window.alert('Internal Server Error')
                } else if (mData.statusCode === 401) {
                    window.alert('Unauthorized access')
                } else if (mData.statusCode === 403) {
                    window.alert('Unauthorized access')
                } else if (mData.statusCode === 404) {
                    window.alert('Page not found')
                }
            })
            .catch((error) => {

            })
    }

    setProduct(product) {
        let mProduct = this.state.product;

        Object.keys(mProduct).forEach((key) => {
            mProduct[key] = product[key]
        });
        this.setState({ product: mProduct });
    }

    reserProduct() {
        let mProduct = this.state.product;

        Object.keys(mProduct).forEach((key) => {
            mProduct[key] = ""
        });
        this.setState({ product: mProduct });
    }

    onHandleChange(e) {
        let mProduct = this.state.product;

        if (e.target.name === 'name') {
            mProduct[e.target.name] = e.target.value.toUpperCase();
        } else {
            mProduct[e.target.name] = e.target.value;
        }

        this.setState({ product: mProduct, isDisabled: false });
    }
    onAdd(e) {
        e.preventDefault();
        this.resetError();
        this.setState({ isDisabled: true });
        Axios.post(buildURL(SMS_VENDOR_CREATE), this.state.product)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 201) {
                    this.successAlert('Addedd successully');
                    this.reserProduct();
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error);
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ isDisabled: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access')
                    this.setState({ isDisabled: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access')
                    this.setState({ isDisabled: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found')
                    this.setState({ isDisabled: false });
                }
            })
            .catch((error) => {
                window.alert(error)
                this.setState({ isDisabled: false });
            })
    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }

    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }
    onUpdate(e) {
        e.preventDefault();
        this.resetError();
        this.setState({ isDisabled: true });
        Axios.put(buildURL(SMS_VENDOR_UPDATE, { id: this.state.id }), this.state.product)
            .then((response) => {
                let mData = response.data;
                if (mData.statusCode === 201) {
                    this.successAlert('Updated successully');
                    this.props.history.push('/sms/vendor');
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error);
                    this.setState({ isDisabled: false });
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error');
                    this.setState({ isDisabled: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ isDisabled: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ isDisabled: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ isDisabled: false });
                }
            })
            .catch((error) => {
                this.errorAlert(error);
                this.setState({ isDisabled: false });
            })
    }

    setError(error) {
        let mError = this.state.error;

        Object.keys(error).forEach((key) => {
            mError[key] = error[key]
        })

        this.setState({ error: mError });
    }

    resetError() {
        let mError = this.state.error;

        Object.keys(mError).forEach((key) => {
            mError[key] = []
        })

        this.setState({ error: mError });
    }

    render() {
        return (
            <div>
                <div className='col-md-6 col-md-offset-3'>
                    {this.renderForm()}
                </div>
            </div>
        )
    }

    renderForm() {
        let status = this.state.status.map((item, index) => {
            return (<option value={item.id} key={index}>{item.name}</option>)
        })
        return (
            <div className='box box-info'>
                <div className='box-body'>

                    <div className='box-header with-border'>
                        <Link to='/sms/vendor'><i className='fa fa-arrow-left' /> </Link>
                        <h4 className='box-title' style={{ marginLeft: '20px' }} >{this.state.update_status ? 'Update SMS Vendor' : 'Add SMS Vendor'}</h4>
                    </div>

                    <div className={this.state.error.name.length > 0 ? 'form-group has-error' : 'form-group'} >
                        <br />
                        <label>Name</label>
                        <input style={{ textTransform: 'uppercase' }} className='form-control' name='name' value={this.state.product.name} onChange={(e) => this.onHandleChange(e)} />
                        <div className='help-block'>{this.state.error.name.length > 0 ? this.state.error.name : ""}</div>
                    </div>
                    <div className={this.state.error.url.length > 0 ? 'form-group has-error' : 'form-group'} >
                        <label>URL</label>
                        <textarea style={{ height: '50px' }} className='form-control' name='url' value={this.state.product.url} onChange={(e) => this.onHandleChange(e)} />
                        <div className='help-block'>{this.state.error.url.length > 0 ? this.state.error.url : ""}</div>
                    </div>
                    <div className={this.state.error.is_default.length > 0 ? 'form-group has-error' : 'form-group'} >
                        <label>Status</label>
                        <select className='form-control' name='is_default' value={this.state.product.is_default} onChange={(e) => this.onHandleChange(e)}>
                            <option value=''>--Select--</option>
                            {status}
                        </select>
                        <div className='help-block'>{this.state.error.is_default.length > 0 ? this.state.error.is_default : ""}</div>
                    </div>

                </div>

                <div className='box-footer with-border'>
                    {
                        this.state.update_status ?
                            <button className='btn btn-primary col-md-2 pull-right' disabled={this.state.isDisabled} onClick={(e) => this.onUpdate(e)} >Update</button>
                            :
                            <button className='btn btn-primary col-md-2 pull-right' disabled={this.state.isDisabled} onClick={(e) => this.onAdd(e)} >Add</button>
                    }

                </div>

            </div>
        )
    }
}