import React from 'react';
import Permissions from './permissions';
//import { BeatLoader } from 'react-spinners';
import { buildURL, ROLE_LIST } from './../../constants/api';

export default class Role extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            permissions_view: false,
            add_status: false,
        }
    }

    onItemClick(e) {
        this.setState({ permissions_view: true });
    }

    onAddRole(e) {
        e.preventDefault();
        this.setState({ add_status: true });
    }

    onModalClose(e) {
        this.setState({ add_status: false });
    }

    render() {
        return (
            <div>
                <div className='row'>
                    {this.renderButton()}
                </div>

                <br />

                <div className='row'>
                    {this.renderTable()}
                 
                            <div className='col-md-6'>
                            {
                                (this.state.permissions_view) ?
                                    <Permissions />
                                    : ""
                            }
                        </div>
                </div >
            </div>
        )
    }

    renderButton() {
        return (
            <div className='col-md-12'>
                <a href='/role/create' className='btn btn-success'>Add Role</a>
            </div>
        )
    }

    renderTable() {
        return (
            <div className='col-md-6'>
                <div className='box no-border'>
                    <div className='box-body  table-responsive no-padding'>
                        <table className='table table-striped table-bordered'>
                            <thead>
                                <tr style={{background:'#03a9f4', color:'#ffffff'}}  >
                                    <th>S.No</th>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr onClick={(e) => this.onItemClick(e)} >
                                    <td>1</td>
                                    <td>Admin</td>
                                </tr>
                                <tr onClick={(e) => this.onItemClick(e)} >
                                    <td>2</td>
                                    <td>Super User</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}