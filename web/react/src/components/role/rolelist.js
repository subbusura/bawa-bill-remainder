import React from 'react';
import Axios from 'axios';
import RoleDetail from './roledetail';
import Modal from './model';
import { buildURL, ROLE_LIST } from './../../constants/api';
import Alert from 'react-s-alert';

export default class RoleList extends React.Component {

        constructor(props) {
                super(props);
                this.state = {
                        is_show_model: false,
                        roles: [],
                        activeItem: "",
                        activeIndex: null,
                };
        }

        componentDidMount() {

                this.getRoles();
        }

        getRoles() {
                Axios.get(buildURL(ROLE_LIST))
                        .then((response) => {
                                let mData = response.data;
                                if (mData.statusCode === 200) {
                                        let data = mData.data.map((role, index) => {
                                                return { role: role, is_active: false }
                                        })
                                        this.setState({ roles: data, progress: false })
                                } else if (mData.statusCode === 500) {
                                        this.errorAlert('Internal Server Error')
                                        this.setState({ progress: false });
                                } else if (mData.statusCode === 401) {
                                        this.errorAlert('Unauthorized access');
                                        this.setState({ progress: false });
                                } else if (mData.statusCode === 403) {
                                        this.errorAlert('Unauthorized access');
                                        this.setState({ progress: false });
                                } else if (mData.statusCode === 404) {
                                        this.errorAlert('Page not found');
                                        this.setState({ progress: false });
                                }

                        })
                        .catch(() => {


                        });


        }

        errorAlert(message) {
                Alert.error(message, {
                        position: 'top-right',
                        timeout: 3000,
                        offset: 100,
                        effect: 'jelly'
                });
        }
        successAlert(message) {
                Alert.success(message, {
                        position: 'top-right',
                        timeout: 3000,
                        offset: 100,
                        effect: 'jelly'
                });
        }


        onRemove(index) {

                let cRoles = this.state.roles.filter((value, lindex) => {

                        if (lindex != index) {
                                return value;
                        }

                });

                this.setState({ roles: cRoles, activeIndex: null, activeItem: "" });
        }

        onRoleClick(e, response, index) {
                e.preventDefault();

                let roles = this.state.roles;

                for (let i = 0; i < roles.length; i++) {
                        if (i == index) {
                                //roles[i].is_active = true;
                                this.state.activeItem = roles[i];
                                this.state.activeIndex = i;
                        } else {
                                roles[i].is_active = false;
                        }
                }
                this.setState({ roles: roles, activeItem: this.state.activeItem, activeIndex: this.state.activeIndex });


        }

        onAdd(e) {

                this.setState({ is_show_model: true })

        }

        onClose(e) {

                this.setState({ is_show_model: false })
        }

        onResult(item) {



                if (typeof item != "undefined") {

                        let list = this.state.roles;
                        list.push({ role: item, is_active: false });

                        this.setState({ roles: list, is_show_model: false });
                }

        }

        render() {

                let rows = this.state.roles.map((response, index) => {
                        let active = response.is_active == true ? "active" : "";
                        return (<tr key={index} className={active} onClick={(e) => { this.onRoleClick(e, response, index) }} >
                                <td>{index + 1}</td>
                                <td>{response.role}</td>
                        </tr>);
                });

                return (<div>

                        <div className="row">

                                <div className="col-md-3">

                                        <p>
                                                <button className="btn btn-success" onClick={(e) => { this.onAdd(e) }}>Add Role</button>
                                        </p>

                                </div>

                        </div>

                        <div className="row">

                                <div className="col-md-6">
                                        <div className="box">

                                                <div className="box-header">


                                                </div>

                                                <div className="box-body no-padding">

                                                        <table className="table">
                                                                <thead>

                                                                        <tr>
                                                                                <th>id</th>
                                                                                <th>Role</th>

                                                                        </tr>
                                                                </thead>
                                                                <tbody>
                                                                        {
                                                                                rows
                                                                        }
                                                                </tbody>

                                                        </table>

                                                </div>

                                                <div className="box-footer clearfix">

                                                </div>
                                        </div>
                                </div>

                                <div className="col-md-6">
                                        {this.state.activeIndex != null ? <RoleDetail role={this.state.activeItem} uuid={this.state.activeIndex} onRemove={(index) => { this.onRemove(index) }} /> : ""}
                                </div>
                        </div>
                        {this.state.is_show_model == true ? <Modal footer={true} saveRole={(item) => this.onResult(item)} onClose={(e) => { this.onClose(e) }} /> : ""}
                </div>);
        }
}