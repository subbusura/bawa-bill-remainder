import React from 'react';
import Axios from 'axios';
import { buildURL, PERMISSION_LIST, ROLE_ADD } from './../../constants/api';
import Alert from 'react-s-alert';

export default class Modal extends React.Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            images: [],
            isShow: true,
            submitMsg: "Save",
            header: "title",
            isDisabled: true,
            error: { role: [] },
            permissions: [],
            data: {
                role: "",
                permission: []
            }
        };
    }

    componentDidMount() {
        this.getPermissions();


    }

    handleSubmit(event) {
        event.preventDefault();

    }

    getPermissions() {

        Axios.get(buildURL(PERMISSION_LIST)).then((response) => {

            let mData = response.data;
            if (mData.statusCode === 200) {
                let result = response.data.data;
                let mresult = [];
                for (let index = 0; index < result.length; index++) {
                    const mlocal = {};
                    mlocal.name = result[index];
                    mlocal.status = false;
                    mresult.push(mlocal);
                }
                this.setState({ permissions: mresult });
            } else if (mData.statusCode === 500) {
                this.errorAlert('Internal Server Error')
                this.setState({ progressStatus: false, isDisabled: false });
            } else if (mData.statusCode === 401) {
                this.errorAlert('Unauthorized access');
                this.setState({ progressStatus: false, isDisabled: false });
            } else if (mData.statusCode === 403) {
                this.errorAlert('Unauthorized access');
                this.setState({ progressStatus: false, isDisabled: false });
            } else if (mData.statusCode === 404) {
                this.errorAlert('Page not found');
                this.setState({ progressStatus: false, isDisabled: false });
            } else if (mData.statusCode === 422) {
                this.setError(mData.error);
                this.setState({ progressStatus: false, isDisabled: false });
            }

        }).catch((error) => {

        });

    }

    onSubmit(e) {
        e.preventDefault();

        this.saveRole();

    }

    saveRole() {

        let data = this.state.data;
        let edata = {
            role: []
        };

        this.setState({ isDisabled: true, error: edata });

        Axios.post(buildURL(ROLE_ADD), data).then((response) => {

            let mData = response.data;
            if (mData.statusCode === 200) {
                this.setState({ isDisabled: false });
                this.successAlert("Added successully");
                this.props.saveRole(data.role);
            } else if (mData.statusCode === 500) {
                this.errorAlert('Internal Server Error')
                this.setState({ progressStatus: false, isDisabled: false });
            } else if (mData.statusCode === 401) {
                this.errorAlert('Unauthorized access');
                this.setState({ progressStatus: false, isDisabled: false });
            } else if (mData.statusCode === 403) {
                this.errorAlert('Unauthorized access');
                this.setState({ progressStatus: false, isDisabled: false });
            } else if (mData.statusCode === 404) {
                this.errorAlert('Page not found');
                this.setState({ progressStatus: false, isDisabled: false });
            } else if (mData.statusCode === 422) {
                this.setError(mData.error);
                this.setState({ progressStatus: false, isDisabled: false });
            }

        }).catch((error) => {

            if (typeof error.response !== 'undefined') {
                let edata = error.response.data;

                this.setState({ error: edata, isDisabled: true });

            }

        });

    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }
    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }


    onChangeHandler(e) {
        e.preventDefault();

        let data = this.state.data;
        data.role = e.target.value.toUpperCase();
        this.setState({ data: data, isDisabled: false });

    }

    onChange(e, value, index) {

        let list = this.state.data.permission;
        let permissions = this.state.permissions;

        if (list.indexOf(value) > -1) {
            list = list.filter((lvalue) => {

                if (lvalue !== value) {
                    return value;
                }

            });

            permissions[index].status = false;

        } else {

            list.push(value);
            permissions[index].status = true;
        }


        this.setState({ data: { role: this.state.data.role, permission: list }, permissions: permissions, updateStatus: false, isDisabled: false });


    }


    render() {

        let rows = this.state.permissions.map((value, index) => {

            return (<div key={index} className="col-sm-4">

                <div className="">

                    <div className="checkbox">

                        <label>

                            <input
                                type="checkbox"
                                className=""
                                value={value.name}
                                onChange={(e) => { this.onChange(e, value.name, index) }}
                                checked={value.status}
                            />
                            {value.name}
                        </label>

                    </div>

                </div>



            </div>);

        })

        return (
            <div className={this.state.isShow ? 'modal fade in' : 'modal fade'} style={this.state.isShow ? { display: 'block', paddingRight: '16px' } : { display: 'none' }} >

                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            Add Role
                                        <button type="button" className="close" onClick={(e) => this.props.onClose()} data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>

                        </div>
                        <div className="modal-body" style={{ height: "450Px", overflowY: "auto" }} >


                            <div className="row">

                                <div className={this.state.error.role.length > 0 ? "form-group has-error" : "form-group"}>

                                    <div className="col-md-6">

                                        <input style={{textTransform:'uppercase'}} type="text" className="form-control" placeholder="Role name" onChange={(e) => this.onChangeHandler(e)} />
                                    </div>

                                    <div className="help-block">{this.state.error.role}</div>

                                </div>

                            </div>

                            <div className="row">
                                <div className="col-md-12">

                                    <h4>Permissions</h4>
                                    <hr />
                                </div>



                            </div>

                            <div className="row">

                                {rows}

                            </div>



                        </div>
                        {

                            (this.props.footer === true) ? <div className="modal-footer">

                                <button className="btn btn-primary" onClick={(e) => this.onSubmit(e)} disabled={this.state.isDisabled}>{this.state.submitMsg}</button>
                            </div> : ''

                        }

                    </div>

                </div>

            </div>
        );
    }

}