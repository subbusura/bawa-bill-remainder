import React from 'react';

export default class Permissions extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            permissions: [
                "Permission1",
                "Permission2",
                "Permission3",
                "Permission4",
                "Permission5",
                "Permission6",
                "Permission7",
                "Permission8",
                "Permission9",
                "Permission10"
            ]
        }
    }

    render() {

        return (
            <div>

                <div className='col-md-12'>
                    <div className='box no-border'>
                        <div className='box-header'>
                            <button className='btn btn-danger btn-sm pull-right'>Delete Role</button>
                        </div>
                        <div className='box-body'>
                            <div className='row'>
                                {
                                    this.state.permissions.map((value, index) =>
                                        <div className='col-md-4'>
                                            <div className='checkbox'>
                                                <label>
                                                    <input type='checkbox' />
                                                    {value}
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                        <div className='box-footer'>
                            <button className='btn btn-success btn-sm pull-right'>Update Permissions</button>
                        </div>
                    </div>
                </div>


            </div>
        );

    }

}