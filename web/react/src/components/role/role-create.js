import React from 'react';

export default class RoleCreate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isShow: true,
            isDisabled: true,
            permissions: [
                "Permission1",
                "Permission2",
                "Permission3",
                "Permission4",
                "Permission5",
                "Permission6",
                "Permission7",
                "Permission8",
                "Permission9",
                "Permission10"
            ],
        }
    }

    onClose(e) {
        e.preventDefault();
        this.setState({ isShow: false });
        this.props.onClose();
    }

    onHandleChange(e) {
        this.setState({ isDisabled: false });
    }

    onAdd(e) {
        e.preventDefault();
        window.location = '/role';
    }

    onCancel(e){
        e.preventDefault();
        window.location = '/role';
    }

    render() {
        return (
            <div>
                <div className='col-md-8 col-md-offset-2'>
                    <div className='box box-info' >
                        <div className='box-body'>
                            <div className='row'>
                                <div className='col-md-10'>

                                    <div className='form-group'>
                                        <label>Add Role</label>
                                        <input style = {{textTransform:'uppercase'}} className='form-control' placeholder='Role' onChange={(e) => this.onHandleChange(e)} />
                                    </div>

                                </div>
                            </div>

                            <br />

                            <h4>Permissions</h4>

                            <div className='row'>
                                <br />
                                {
                                    this.state.permissions.map((value, index) =>
                                        <div className='col-md-3'>
                                            <div className='checkbox'>
                                                <label>
                                                    <input type='checkbox' />
                                                    {value}
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                            </div>

                            <div className='row' >
                                <br />
                                <div className='col-md-12'>
                                    <button className='btn btn-primary pull-right col-md-2' onClick={(e)=>this.onAdd(e)} >Add</button>
                                    <button className='btn btn-default pull-right col-md-2' onClick={(e)=>this.onCancel(e)} style={{ marginRight: '5px' }} >Cancel</button>
                                </div>
                                <br />
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}