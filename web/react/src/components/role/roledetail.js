import React from 'react';
import Axios from 'axios';
import { buildURL, PERMISSION_LIST, GET_PERMISSION, UPDATE_PERMISSION, ROLE_DELETE } from './../../constants/api';
import Alert from 'react-s-alert';

export default class RoleDetails extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            details: {},
            role: this.props.role,
            dpermissions: [],
            rolePermission: [],
            loading: false,
            updateStatus: true,
            headerCheck: {
                status: false,
                message: "check all"
            }

        };
    }

    componentDidMount() {
        this.getDetails(this.props.role.role, this.props.role);
    }

    componentWillReceiveProps(next) {

        if (this.props.role == next.role.role) {

        } else {

            if (next.uuid != null) {
                this.getDetails(next.role.role, next.role);
            }

        }
    }

    getDetails(name, nextrole) {

        this.setState({ loading: true, dpermissions: [], rolePermission: [], role: nextrole })
        Axios.all([
            Axios.get(buildURL(GET_PERMISSION, { role: name })),
            Axios.get(buildURL(PERMISSION_LIST))
        ]).then(Axios.spread((rolePermission, permissionslist) => {

            if (rolePermission.data.statusCode === 200 && permissionslist.data.statusCode === 200) {
                let activeRoles = rolePermission.data.data;
                let result = permissionslist.data.data;
                let mresult = [];
                for (let index = 0; index < result.length; index++) {
                    const mlocal = {};
                    mlocal.name = result[index];
                    if (activeRoles.indexOf(result[index]) > -1) {
                        mlocal.status = true;
                    } else {
                        mlocal.status = false;
                    }
                    mresult.push(mlocal);
                }
                this.setState({ dpermissions: mresult, rolePermission: activeRoles, updateStatus: true, loading: false });
            } else if (rolePermission.data.statusCode === 500 || permissionslist.data.statusCode === 500) {
                this.errorAlert('Internal Server Error')
                this.setState({ progress: false });
            } else if (rolePermission.data.statusCode === 401 || permissionslist.data.statusCode === 401) {
                this.errorAlert('Unauthorized access');
                this.setState({ progress: false });
            } else if (rolePermission.data.statusCode === 403 || permissionslist.data.statusCode === 403) {
                this.errorAlert('Unauthorized access');
                this.setState({ progress: false });
            } else if (rolePermission.data.statusCode === 404 || permissionslist.data.statusCode === 404) {
                this.errorAlert('Page not found');
                this.setState({ progress: false });
            }

        })).catch((e) => {

        });

    }

    errorAlert(message) {
        Alert.error(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }
    successAlert(message) {
        Alert.success(message, {
            position: 'top-right',
            timeout: 3000,
            offset: 100,
            effect: 'jelly'
        });
    }


    onChange(e, value, index) {

        let list = this.state.rolePermission;
        let permissions = this.state.dpermissions;

        if (list.indexOf(value) > -1) {
            list = list.filter((lvalue) => {

                if (lvalue != value) {
                    return value;
                }

            });

            permissions[index].status = false;

        } else {

            list.push(value);
            permissions[index].status = true;
        }



        this.setState({ rolePermission: list, dpermissions: permissions, updateStatus: false });

    }

    onAll(e) {

        let cstate = this.state.headerCheck;
        if (cstate.status) {
            cstate.status = false;
            cstate.message = "check all";
            this.checkAll(cstate);
        } else {

            cstate.status = true;
            cstate.message = "uncheck all";

            // this.uncheckAll(cstate);
        }



    }

    checkAll(cstate) {

        let result = this.state.dpermissions;


        let mresult = [];

        for (let index = 0; index < result.length; index++) {

            const mlocal = {};
            mlocal.name = result[index];
            mlocal.status = true;

            mresult.push(mlocal);
        }


        this.setState({ dpermissions: mresult, headerCheck: cstate });


    }

    uncheckAll(cstate) {

        let result = this.state.dpermissions;
        let mresult = [];

        for (let index = 0; index < result.length; index++) {

            const mlocal = {};
            mlocal.name = result[index];
            mlocal.status = false;

            mresult.push(mlocal);
        }


        this.setState({ dpermissions: mresult, headerCheck: cstate });


    }

    UpdatePermissions(e) {
        e.preventDefault();

        this.setState({ updateStatus: true });
        Axios.post(buildURL(UPDATE_PERMISSION, { role: this.state.role.role }), { permissions: this.state.rolePermission })
            .then((response) => {

                let mData = response.data;
                if (mData.statusCode === 200) {
                    this.setState({ updateStatus: false });
                    this.successAlert("Updated successfully");
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error);
                    this.setState({ progressStatus: false, isDisabled: false });
                }

            }).catch((e) => {
            });

    }
    onDelete(e) {

        let confirm = window.confirm("Do you want remove this item? ");

        if (confirm) {
            Axios.get(buildURL(ROLE_DELETE, { role: this.state.role.role }))
                .then((response) => {
                    let mData = response.data;
                if (mData.statusCode === 200) {
                    this.props.onRemove(this.props.uuid)
                    this.successAlert("Deleted successfully");
                } else if (mData.statusCode === 500) {
                    this.errorAlert('Internal Server Error')
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 401) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 403) {
                    this.errorAlert('Unauthorized access');
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 404) {
                    this.errorAlert('Page not found');
                    this.setState({ progressStatus: false, isDisabled: false });
                } else if (mData.statusCode === 422) {
                    this.setError(mData.error);
                    this.setState({ progressStatus: false, isDisabled: false });
                }                 

                }).catch((error) => {

                });


        }


    }


    render() {


        let rows = this.state.dpermissions.map((value, index) => {

            return (<div key={index} className="col-sm-4">

                <div className="">

                    <div className="checkbox">

                        <label>

                            <input
                                type="checkbox"
                                className=""
                                value={value.name}
                                onChange={(e) => { this.onChange(e, value.name, index) }}
                                checked={value.status}
                            />
                            {value.name}
                        </label>

                    </div>

                </div>



            </div>);

        })


        if (this.state.role === "") {
            return (<div></div>);
        }

        return (<div>{this.state.loading == true ? <div>loading..</div> : <div className={"box"}>

            <div className={"box-header"}>

                <div className="box-tools">
                    <button onClick={(e) => { this.onDelete(e) }} className={"btn btn-sm btn-danger"}>Delete Role</button>
                </div>
            </div>
            <div className={"box-body"}>

                <div className={"row"}>

                    {rows}

                </div>

            </div>
            <div className={"box-footer"}>

                <button disabled={this.state.updateStatus} onClick={(e) => { this.UpdatePermissions(e) }} className={"btn btn-sm btn-success pull-right"}>Update Permissions</button>


            </div>
        </div>} </div>);

    }

}