import React from 'react';
import { Link } from 'react-router-dom';
import Can from './../components/permission/Can';

export default class SidebarMenu extends React.Component {

    render() {
        
        let users=window.__PRELOADED_STATE__.user;

        return (
            <ul className="nav navbar-nav" data-widget="tree">
                <li><Link to="/">Dashboard (Alt+d)</Link></li>

                <Can permission={'Arrival List'}><li><Link to="/arrival">Arrival (Alt+a)</Link></li></Can>
                <Can permission={'Order List'}><li><Link to="/order">Order Status (Alt+o)</Link></li></Can>
                <Can permission={'Sales Order List'}><li><Link to="/sales/order">Sales Order  (Alt+s)</Link></li></Can>
                <Can permission={'Supplier Order List'}><li><Link to="/sorder/order">Supplier Order(Alt+b)</Link></li></Can>
                <Can permission={'Order List'}><li><Link to="/order-items">Place Order (Alt+I)</Link></li></Can>
                <Can permission={'Master View'}>
                    <li className="dropdown">
                        <a href="#" data-toggle="dropdown" aria-expanded="false">Reports <span className="caret"></span></a>
                        <ul className="dropdown-menu">
                        <Can permission={'Product List'}><li><Link to="/reports/order-items">Orders</Link></li></Can>
                        <Can permission={'Product List'}><li><Link to="/reports/customers">Customers</Link></li></Can>
                        <Can permission={'Product List'}><li><Link to="/reports/supplier/order">Supplier Order</Link></li></Can>
                        <Can permission={'Product List'}><li><Link to="/reports/daily/needs">Wanted Product</Link></li></Can>
                        </ul>
                    </li>
                </Can>

                <Can permission={'Master View'}><li className="dropdown">
                    <a href="#" data-toggle="dropdown" aria-expanded="false">Masters <span className="caret"></span></a>
                    <ul className="dropdown-menu">

                        <Can permission={'Product List'}><li><Link to="/product">Product</Link></li></Can>
                        <Can permission={'Supplier List'}><li><Link to="/supplier">Supplier</Link></li></Can>
                        <Can permission={'Sales Order List'}> <li className="divider"></li></Can>
                        <Can permission={'User List'}> <li><Link to="/user">User</Link></li></Can>
                        <Can permission={'Role List'}><li><Link to="/role">Role</Link></li></Can>
                        <Can permission={'Role List'}><li><Link to="/salesman">Sales User</Link></li></Can>
                        <Can permission={'Role List'}><li><Link to="/sms/template">SMS Template</Link></li></Can>
                        <Can permission={'Role List'}><li><Link to="/sms/vendor">SMS Vendor</Link></li></Can>
                        <Can permission={'Role List'}><li><Link to="/printer">Printers</Link></li></Can>

                    </ul>
                </li></Can>

                <li><a href="/logout">Logout({users.username})</a></li>
            </ul>
        )
    }

}