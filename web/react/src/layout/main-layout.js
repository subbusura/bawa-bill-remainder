import React from 'react';
import SidebarMenu from './../menu/sidebar-menu';
export default class MainLayout extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isActive: true,
            height: 0
        };
    }


    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions.bind(this));
    }

    updateWindowDimensions() {
        this.setState({ height: window.innerHeight });
    }
    onBtnClick(event) {
        event.preventDefault();
        this.setState({ isActive: !this.state.isActive });
    }
    render() {

        return (
            <div className={'wrapper'}>

                <div className={'main-header'}>

                    <a className='logo'>
                        <img src='/images/logo.png' alt='' style={{ width: "50px", height: "50px" }} />
                    </a>

                    <nav className="navbar navbar-static-top">

                        <div className={"container"}>

                            <div className={"navbar-header"}>
                            </div>

                            <div className={"collapse navbar-collapse pull-left"} id="navbar-collapse">
                                <SidebarMenu />
                            </div>

                            <div className={"navbar-custom-menu"}>
                            </div>

                        </div>
                        {/* <a href='#' className="sidebar-toggle"  role='button' onClick={(e)=>this.onBtnClick(e)} >
                            <span className='sr-only'>Toggle navigation</span>
                        </a> */}

                    </nav>
                </div>

                <div className={'content-wrapper'} style={{ minHeight: this.state.height + 'px' }}>
                    <div className={'container-fluid'}>
                        <div className="content-header"></div>
                        <div className={"content"}>

                            {this.props.children}

                        </div>

                    </div>

                </div>

            </div>
        );

    }

}  