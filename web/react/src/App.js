import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

import './AdminLTE.min.css';
import './_all-skins.css';

import Alert from 'react-s-alert';
import Axios from 'axios';

import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import 'react-s-alert/dist/s-alert-css-effects/scale.css';
import 'react-s-alert/dist/s-alert-css-effects/flip.css';
import 'react-s-alert/dist/s-alert-css-effects/jelly.css';
import 'react-s-alert/dist/s-alert-css-effects/stackslide.css';
import 'react-s-alert/dist/s-alert-css-effects/genie.css';
import 'react-s-alert/dist/s-alert-css-effects/bouncyflip.css';

import { Switch, Route, withRouter } from 'react-router-dom';

import MainLayout from './layout/main-layout'
import Role from './components/role/role';
import RoleCreate from './components/role/role-create';
import ProductList from './components/product/product-list';
import ProductCreate from './components/product/product-create';
import SupplierList from './components/supplier/supplier-list';
import SupplierCreate from './components/supplier/supplier-create';
import RoleList from './components/role/rolelist';
import OrdersList from './components/orders/orders-list';
import OrderCreate from './components/orders/order-create';
import OrderUpdate from './components/orders/order-update';
import SalesOrdersList from './components/salesorder/sales-orders-list';
import SalesOrderCreate from './components/salesorder/sales-order-create';
import SalesOrderUpdate from './components/salesorder/sales-order-update';
import SupplierOrdersList from './components/supplierorder/supplier-orders-list';
import SupplierOrderCreate from './components/supplierorder/supplier-order-create';
import SupplierOrderUpdate from './components/supplierorder/supplier-order-update';
import ArrivelList from './components/arrivel/arrivel-list';
import Dashboard from './components/dashboard/dashboard';
import User from './components/user/user';
import UserAdd from './components/user/useradd';
import UserCreate from './components/user/user-create';
import UserUpdate from './components/user/user-update';

import SalesmanCreate from './components/salesrep/SalesmanCreate';
import SalesmanList from './components/salesrep/SalesmanList';

import Can from './components/permission/Can';

import MessageSetup from './components/sms/message-setup';
import SMSVendorList from './components/sms/sms-vendor-list';
import SMSVendorCreate from './components/sms/sms-vendor-create';

import OrderItemsList from './components/orderItems/order-items-list';

import ArrivalCreate from './components/arrivel/arrival-create';
import ArrivelListTemp from './components/arrivel/arrivel-list-temp';

import Reports from './components/reports/reports';
import ReportSupplierOrder from './components/reports/reportsupplierorder';
import DailyProductNeed from './components/reports/dailyproductneed';
import CustomerReport from './components/reports/customer-reports';

import PrinterList from './components/printer/PrinterList';

import PrinterCreate from './components/printer/printer-create';

import {KEEPALIVE,buildURL} from './constants/api';

class App extends Component {

    constructor(props) {
        super(props);

        this.escFunction = this.escFunction.bind(this);
    }

    escFunction(e) {

        // console.log(e);
        //Alt+d
        if (e.altKey && e.which === 68) {
            e.preventDefault();

            this.props.history.push('/');
            //console.log("Alt+d");
        }

        //Alt+s
        if (e.altKey && e.which === 83) {
            e.preventDefault();
            this.props.history.push('/sales/order');

        }

        //Alt+p
        if (e.altKey && e.which === 80) {
            e.preventDefault();
            //console.log("Alt+p");

        }
        //Alt+t
        if (e.altKey && e.which === 84) {
            e.preventDefault();

            //console.log("Alt+t");

        }
        //Alt+l
        if (e.altKey && e.which === 76) {
            e.preventDefault();

            //console.log("Alt+l");

        }
        //Alt+o
        if (e.altKey && e.which === 79) {
            e.preventDefault();


            this.props.history.push('/order');


        }
        //Alt+f
        if (e.altKey && e.which === 70) {
            e.preventDefault();

            //console.log("Alt+f");

        }
        //Alt+i
        if (e.altKey && e.which === 73) {
            e.preventDefault();
            this.props.history.push('/order-items');
            //console.log("Alt+i");
        }
        //Alt+r
        if (e.altKey && e.which === 82) {
            e.preventDefault();

            //console.log("Alt+r");

        }
        //Alt+b
        if (e.altKey && e.which === 66) {
            e.preventDefault();

            //console.log("Alt+b");
            this.props.history.push('/sorder/order');

        }
        //Alt+a
        if (e.altKey && e.which === 65) {
            e.preventDefault();

            this.props.history.push('/arrival');

            //console.log("Alt+a");

        }
        //Alt+u
        if (e.altKey && e.which === 85) {
            e.preventDefault();
            this.props.history.push('/sales/order/create');
            //console.log("Alt+u");
        }

        if (e.altKey && e.which === 85) {
            e.preventDefault();
            this.props.history.push('/sales/order/create');
            //console.log("Alt+u");
        }

        // if (e.altKey && e.which === 82) {
        //     e.preventDefault();
        //     this.props.history.push('/Reports');
        //     //console.log("Alt+R");
        // }


    }
    componentDidMount() {
        document.addEventListener("keydown", this.escFunction, false);
        this.timer=setInterval(this.keepAlive, 300000);
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
        clearInterval(this.timer);
    }

    keepAlive()
    {

        

        Axios.get(buildURL(KEEPALIVE))
        .then((response) => {
            console.log("KeepAlive Message");
        })
        .catch((error) => {
           
        })

    }

    render() {
        return (

            <MainLayout>

                <Switch>
                    <Route exact path="/" component={Dashboard} />
                    <Route exact path="/arrival" component={ArrivelListTemp} />
                    <Route exact path="/printer" component={PrinterList} />
                    <Route exact path="/printer/create" component={PrinterCreate} />
                    <Route exact path="/arrival/create" component={ArrivalCreate} />
                    <Route exact path="/order" component={OrdersList} />
                    <Route exact path="/order/create" component={OrderCreate} />
                    <Route exact path="/order/update/:id" component={OrderUpdate} />
                    <Route exact path="/sales/order" component={SalesOrdersList} />
                    <Route exact path="/sales/order/create" component={SalesOrderCreate} />
                    <Route exact path="/sales/order/update/:id" component={SalesOrderUpdate} />
                    <Route exact path="/sorder/order" component={SupplierOrdersList} />
                    <Route exact path="/sorder/order/create" component={SupplierOrderCreate} />
                    <Route exact path="/sorder/order/update/:id" component={SupplierOrderUpdate} />
                    <Route exact path="/role" component={RoleList} />
                    <Route exact path="/role/create" component={RoleCreate} />
                    <Route exact path="/product" component={ProductList} />
                    <Route exact path="/product/create" component={ProductCreate} />
                    <Route exact path="/product/update/:id" component={ProductCreate} />
                    <Route exact path="/salesman" component={SalesmanList} />
                    <Route exact path="/salesman/create" component={SalesmanCreate} />
                    <Route exact path="/salesman/update/:id" component={SalesmanCreate} />
                    <Route exact path="/supplier" component={SupplierList} />
                    <Route exact path="/supplier/create" component={SupplierCreate} />
                    <Route exact path="/supplier/update/:id" component={SupplierCreate} />
                    <Route exact path="/user" component={User} />
                    <Route exact path="/user/create" component={UserCreate} />
                    <Route exact path="/user/create/:id" component={UserUpdate} />
                    <Route exact path="/role" component={RoleList} />
                    <Route exact path="/sms/template" component={MessageSetup} />
                    <Route exact path="/sms/vendor" component={SMSVendorList} />
                    <Route exact path="/sms/vendor/create" component={SMSVendorCreate} />
                    <Route exact path="/sms/vendor/update/:id" component={SMSVendorCreate} />
                    <Route exact path="/order-items" component={OrderItemsList} />
                    <Route exact path="/reports/order-items" component={Reports} />
                    <Route exact path="/reports/customers" component={CustomerReport} />
                    <Route exact path="/reports/supplier/order" component={ReportSupplierOrder} />
                    <Route exact path="/reports/daily/needs" component={DailyProductNeed} />

                </Switch>
                <Alert stack={{ limit: 5 }} />
            </MainLayout>
        );
    }
}

export default withRouter(App);
