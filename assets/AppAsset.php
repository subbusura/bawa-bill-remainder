<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
    		'css/AdminLTE.min.css',
    		'//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
    	'css/skin-green.css',
    	'https://cdn.jsdelivr.net/npm/react-s-alert@1.4.1/dist/s-alert-default.css',
    	'https://cdn.jsdelivr.net/npm/react-s-alert@1.4.1/dist/s-alert-css-effects/bouncyflip.css',
    		'https://cdn.jsdelivr.net/npm/react-s-alert@1.4.1/dist/s-alert-css-effects/flip.css',
    		'https://cdn.jsdelivr.net/npm/react-s-alert@1.4.1/dist/s-alert-css-effects/genie.css',
    		'https://cdn.jsdelivr.net/npm/react-s-alert@1.4.1/dist/s-alert-css-effects/jelly.css',
    		'https://cdn.jsdelivr.net/npm/react-s-alert@1.4.1/dist/s-alert-css-effects/scale.css',
    		'https://cdn.jsdelivr.net/npm/react-s-alert@1.4.1/dist/s-alert-css-effects/slide.css',
    		'https://cdn.jsdelivr.net/npm/react-s-alert@1.4.1/dist/s-alert-css-effects/stackslide.css',
    		'https://unpkg.com/react-select@1.2.1/dist/react-select.css',
        'css/site.css'
    ];
    public $js = [
    		'https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js',
    		'https://npmcdn.com/bootstrap@4.0.0-alpha.5/dist/js/bootstrap.min.js',
    		'https://code.jquery.com/jquery-3.3.1.slim.min.js',
    		'js/adminlte.min.js',
    		'js/main.4fa8e3c0.js'
    ];
    public $depends = [
    		'yii\web\YiiAsset',
    ];
}
