<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class loginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    		'css/AdminLTE.min.css',
    		'//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
    	'css/skins/_all-skins.min.css'
    ];
    public $js = [
    		'//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.3/js/bootstrap.bundle.min.js',
    		'js/adminlte.min.js',
    ];
    public $depends = [
    		'yii\web\YiiAsset',
    		'yii\bootstrap\BootstrapAsset',
    ];
}
