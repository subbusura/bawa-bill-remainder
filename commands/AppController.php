<?php


namespace app\commands;

use yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\AdminUser;

class AppController extends Controller
{
   
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }
    
    public function actionInit(){
    	
    	$auth=Yii::$app->authManager;
    	AdminUser::deleteAll();
    	
    	$this->actionInstall();
    	
    	$adminDevice=new AdminUser();
    	$adminDevice->username="admin";
    	$adminDevice->password=Yii::$app->security->generatePasswordHash("admin@123");
    	$adminDevice->auth_key=Yii::$app->security->generateRandomString();
    	$adminDevice->firstname="Admin";
    	$adminDevice->lastname="Demo";
    	$adminDevice->email="admin@nekhop.com";
    	$adminDevice->status=10;
    	$adminDevice->created_at=time();
    	 
    	if($adminDevice->save(false))
    	{
    		// $this->addDefaultRole();
    		 
    		$superUser=$auth->getRole("super user");
    		$auth->assign($superUser,$adminDevice->id);
    		echo "User Created success... \n";
    	}
    	return 0;
    	
    	
    }
    
    private function addDefaultRole(){
    	$auth=Yii::$app->authManager;
    	$superUser=$auth->getRole("super user");
    	$admin = $auth->getRole("admin");
    	$auth->addChild($superUser, $admin);
    	
    }
    
    
    public function actionInitPermission(){
    
    	$auth=Yii::$app->authManager;
    		
    	$auth->removeAll();
    	
    	
    		
    	$permissions=$this->permissions();
    
    	for ($i = 0; $i < count($permissions); $i++) {
    
    		$manageProduct=$auth->createPermission($permissions[$i]);
    		$auth->add($manageProduct);
    	}
    	
    	echo "Permissions added successfully...".PHP_EOL;
    
    }
    
    private function permissions(){
    
    
    	return [
    			"Product List",
    			"Product Create",
    			"Product Delete",
    			"Product Update",
    			"Supplier List",
    			"Supplier Create",
    			"Supplier Delete",
    			"Supplier Update",
    			"User List",
    			"User Create",
    			"User Delete",
    			"User Update",
    			"Role List",
    			"Role Add",
    			"Role Delete",
    			"Role Permission Update",
    			"Sales Order Create",
    			"Sales Order Update",
    			"Sales Order Delete",
    			"Sales Order List",
    			"Order Create",
    			"Order Update",
    			"Order Delete",
    			"Order List",
    			"Master View",
    			"Arrival List",
    	];
    }
    
    private function defaultRolePermissions(){
    	 
    		
    	return [
    
    			[
    					'role'=>'super user',
    					'permissions'=>[
    							
    
    					]
    			],
    			[
    			'role'=>'admin',
    			'permissions'=>[
    			"Product List",
    			"Product Create",
    			"Product Delete",
    			"Product Update",
    			"Supplier List",
    			"Supplier Create",
    			"Supplier Delete",
    			"Supplier Update",
    			"User List",
    			"User Create",
    			"User Delete",
    			"User Update",
    			"Role List",
    			"Role Add",
    			"Role Delete",
    			"Role Permission Update",
    			"Sales Order Create",
    			"Sales Order Update",
    			"Sales Order Delete",
    			"Sales Order List",
    			"Order Create",
    			"Order Update",
    			"Order Delete",
    			"Order List",
    			"Master View",
    			"Arrival List",
    		]
    			],
    
    	];
    	 
    	 
    }
    
    public function actionInstall(){
    	 
    	$auth = Yii::$app->authManager;
    
    	$this->actionInitPermission();
    
    	$roles=$this->defaultRolePermissions();
    		
    		
    	for ($i = 0; $i < count($roles); $i++) {
    
    		$role=$auth->createRole($roles[$i]['role']);
    			
    		if($auth->add($role)){
    
    			 
    			foreach ($roles[$i]['permissions'] as $permissions)
    			{
    				$pemission=$auth->getPermission($permissions);
    					
    				if(!is_null($pemission))
    				{
    					$auth->addChild($role, $pemission);
    				}
    					
    				//echo $permissions." \n";
    					
    					
    			}
    			 
    			 
    		}

    	}
    	
    	$this->addDefaultRole();
    	
    }
    
}
