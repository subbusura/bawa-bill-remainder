<?php 
	$date = new DateTime();
	$date->setTimestamp($order->created_at);
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta charset="utf-8" />
<style type="text/css">
 body{
  	padding:0px;
  	margin:0px;
  }
@media print {
	thead {
		display: table-header-group;
	}
	tfoot {
		display: table-footer-group;
	}
}

@media screen {
	thead {
		display: block;
	}
	tfoot {
		display: block;
	}
	
	@page :left {
margin: 0.5cm;
}

	@page :right {
	margin: 0.8cm;
	}
}
</style>
</head>
<body>
	<table>
		<thead>
			<tr>
				<td>Order No : <?=$order->id ?></td>
				<td>Order Date : <?= $date->format("Y-m-d") ?></td>
			</tr>
			<tr>
				<td>Name : <?=$order->name ?></td>
			</tr>
			<tr>
				<td>Mobile Number : <?=$order->mobile_number ?></td>
			</tr>
			<tr>
				<td>Particulars</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Description</td>
				<td>Qty</td>
			</tr>
			
			<?php 
			
			  $i=1;
			  foreach ($order->items as $item)
			  {
			  	
			  	echo "<tr>
						<td>{$i}.{$item->product_name}</td>
						<td>{$item->qty}</td>
					</tr>";
			  }
			
			
			?>
			

		</tbody>
		<tfoot align="center">
			<tr>
				<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbspThank You!</td>
			</tr>
		</tfoot>
	</table>

</body>
</html>
