<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-box" >

            <!-- /.login-logo -->
            <div class="login-box-body" style="background:rgba(255, 255, 255, 0.12);color:#fff">
                <p class="login-box-msg">Sign in to start your session</p>
                 <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                    <div class="form-group has-feedback">
                         <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                    </div>
                    <div class="form-group has-feedback">
                       <?= $form->field($model, 'password')->passwordInput() ?>
                    </div>
                     
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                           
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                        </div>
                        <!-- /.col -->
                    </div>
                    
               <?php ActiveForm::end(); ?>

                <!-- /.social-auth-links -->

               
                
               

            </div>
            <!-- /.login-box-body -->
        </div>

