<?php
namespace app\components;

use yii\base\Component;

class SmsComponent extends Component {
	
	public $url="";
	
	public function init(){
		 parent::init();
		 
	}
	
	public function sendSms($phone,$message,$url){

		
		    $content=[
		    		"MOBILE_NUMBER"=>$phone,
		    		"MESSAGE_CONTENT"=>$message
		    ];
		    
		    $convertUrl=$this->stringreplace($content, $url);
		
		   $result=$this->sendToServer($convertUrl);
		   
		   return $result;
		   
	}
	public function stringreplace($variables,$content){
	
		$response = preg_replace_callback('/{(.+?)}/ix',function($match)use($variables){
			return !empty($variables[$match[1]]) ? $variables[$match[1]] : $match[0];
		},$content);
				
			return $response;
	}
    protected function sendToServer($url){
    	
    	$ch = curl_init();	
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		if ($result === FALSE) {
			
			//$result['error']=curl_error($ch);
		}
		curl_close($ch);
		 
		return $result;
		
    	
    }
    
    protected function setConfiguration($config){
    	
    	     foreach ($config as $key=>$value){
    	     	  
    	     	   $this->{$key}=$value;
    	     	 
    	     }
    	    
    	
    }
	 
	
}