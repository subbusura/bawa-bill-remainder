<?php
namespace app\components;

use yii\base\Component;

class BhashsmsComponent extends Component {
	
	public $baseURl="http://tsms.nekhop.com/api/";
	//public $baseURl="http://127.0.0.1/smsApi/";
    public $smsPath="sendmsg.php";
    public $schedulePath="schedulemsg.php";
    public $balancePath="checkbalance.php";
    public $getSenderIDs="getsenderids.php";
    public $deliveryPath="recdlr.php";
    public $username='smstdemo';
    public $password='12345';
    public $senderID='STDEMO';
    public $phoneNumner;
    public $phoneNumbers;
    public $message;
    public $priority='ndnd';
    public $smsType='normal';
    public $time;
    public $messageID;
    public $messageType='ndnd'; //dnd for promo and ndnd for trans
    
    
	public function init(){
		 parent::init();
		 
	}
	
	public function sendSms($phone,$message,$config=[]){
		
		    if(!empty($config))
		    {
		    	$this->setConfiguration($config);	
		    }

		   $result=$this->buildUrl($phone, $message, 'single');
		   
		   return $result;
		
	}
	
	
	public function sendGroup($numbers,$message,$config=[]){
		
		
		if(!empty($config))
		{
			 
			$this->setConfiguration($config);
			 
		}
		
		$result=$this->buildUrl($numbers, $message, 'multiple');
		 
		return $result;
		
		
	}
	
	public function getBalance($config=[]){
		
		if(!empty($config))
		{
			 
			$this->setConfiguration($config);
			 
		}
		
		$result=$this->buildUrl(null, null, 'balance');
			
		return $result;
		
		
	}
	
	public function getDlrStatus($phone_number,$message_id,$message_type="dnd",$config=[]){
		
		if(!empty($config))
		{
			$this->setConfiguration($config);
		}
		
		$this->phoneNumner=$phone_number;
		$this->messageID=$message_id;
		$this->messageType=$message_type;
		
		$result=$this->buildUrl(null, null, 'dlr');
			
		return $result;
		
		
	}

    protected function buildUrl($phone,$message,$type){
    	
    	   $result="";
    	 
    	     switch ($type) {
    	     	case 'single':
    	     		
    	     		 $params=['user'=>$this->username,'pass'=>$this->password,'sender'=>$this->senderID,'phone'=>$phone,'text'=>$message,'priority'=>$this->priority,'stype'=>$this->smsType];
    	     		 $url=$this->baseURl.$this->smsPath."?".http_build_query($params);    		 
    	     		 $result=$this->sendToServer($url);
    	     		
    	     	break;
    	     	case 'multiple':
    	     		
    	     		$params=['user'=>$this->username,'pass'=>$this->password,'sender'=>$this->senderID,'phone'=>$this->arrayToString($phone),'text'=>$message,'priority'=>$this->priority,'stype'=>$this->smsType];
    	     		$url=$this->baseURl.$this->smsPath."?".http_build_query($params);
    	     		$url=urlencode($url);
    	     		$result=$this->sendToServer($url);
    	     	
    	     		break;
    	     	case 'balance':
    	     		
    	     			$params=['user'=>$this->username,'pass'=>$this->password];
    	     			$url=$this->baseURl.$this->balancePath."?".http_build_query($params);
    	     			$result=$this->sendToServer($url);
    	     			 
    	     	 break;
    	     	 case 'dlr':
    	     	 
    	     	 	$params=['user'=>$this->username,'msgid'=>$this->messageID,'phone'=>$this->phoneNumner,'msgtype'=>$this->messageType];
    	     	 	$url=$this->baseURl.$this->deliveryPath."?".http_build_query($params);
    	     	 	$result=$this->sendToServer($url);
    	     	 
    	     	 	break;
    	     	
    	     	default:
    	     		;
    	     	break;
    	     }
    	     
    	   return $result;
    	
    } 
	
	protected function arrayToString($array=[],$separator=",")
	{
		
		 return implode($separator,$array);
		 
	}
	
    protected function stringToArray($string="",$separator=","){
    	
    	return explode($separator,$string);
    	
    	
    }
    
    protected function sendToServer($url){
    	
    	$ch = curl_init();	
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		if ($result === FALSE) {
			
			//$result['error']=curl_error($ch);
		}
		curl_close($ch);
		 
		return $result;
		
    	
    }
    
    protected function setConfiguration($config){
    	
    	     foreach ($config as $key=>$value){
    	     	  
    	     	   $this->{$key}=$value;
    	     	 
    	     }
    	    
    	
    	
    }
	 
	
}