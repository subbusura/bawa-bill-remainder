<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders_items".
 *
 * @property string $id
 * @property string $order_id
 * @property string $product_code
 * @property string $product_name
 * @property string $supplier_code
 * @property string $supplier_name
 * @property int $qty
 * @property int $arrival_status
 * @property int $arrival_datetime
 */
class OrdersItem extends \yii\db\ActiveRecord
{
	
	const STATUS_PENDING = 10;
	const STATUS_COMPLETED = 15;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_code','qty'], 'required','on'=>'create'],
            [['order_id', 'qty', 'arrival_status', 'arrival_datetime'], 'integer'],
            [['product_code', 'product_name', 'supplier_code', 'supplier_name'], 'string', 'max' => 100],
        ];
    }
    
    public function getOrder(){
    	
    	   return $this->hasOne(Order::className(), ["id"=>"order_id"]);
    	
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_code' => 'Product Code',
            'product_name' => 'Product Name',
            'supplier_code' => 'Supplier Code',
            'supplier_name' => 'Supplier Name',
            'qty' => 'Qty',
            'arrival_status' => 'Arrival Status',
            'arrival_datetime' => 'Arrival Datetime',
        ];
    }
}
