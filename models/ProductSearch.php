<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "products".
 *
 * @property string $id
 * @property string $product_code
 * @property string $product_name
 * @property string $DivisionCode
 */
class ProductSearch extends Product
{
  
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_code', 'product_name', 'DivisionCode'], 'string', 'max' => 255],
            [['product_code'], 'unique'],
        ];
    }
    
    public function search($params){
    	
    	   $query = Product::find();
    	   
    	   
    	  $dataProvider=new ActiveDataProvider([
    	  		'query'=>$query,
    	  		'pagination' => [
    	  				'pageSizeLimit' => [0, 50],
    	  		],
    	  ]);
    		
    	  	
    	  $this->load($params,"");
    	  
    	  if(!$this->validate())
    	  {
    	  	
    	  	 return $dataProvider;
    	  }
    	  
    	  
    	  $query->andFilterWhere([
    	  		 "product_code"=>$this->product_code,
    	  ]);
    	  
    	  $query->andFilterWhere(["like","product_name",$this->product_name]);
    	  
    	 return $dataProvider;
    }

}
