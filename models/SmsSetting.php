<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_setting".
 *
 * @property string $id
 * @property string $sms_type
 * @property string $content
 */
class SmsSetting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
        	[['sms_type'], 'unique'],
            [['sms_type'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sms_type' => 'Sms Type',
            'content' => 'Content',
        ];
    }
}
