<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "admin_users".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $mobile_number
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class AdminUser extends \yii\db\ActiveRecord implements IdentityInterface
{
	
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 10;
	const STATUS_SUSPEND = 20;
	const STATUS_ROLE_UPDATE_FILED=30;
	const TYPE_ALL="all";
	const TYPE_CUSTOM="custom";
	public $role;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname','username', 'password','role'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'email'], 'string', 'max' => 100],
        	['username','unique'],
            [['auth_key', 'password', 'firstname', 'lastname','role'], 'string', 'max' => 255],
            [['mobile_number'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password' => 'Password',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'mobile_number' => 'Mobile Number',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
    	return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
    	throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
    	return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
    	return null;
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
    	return $this->getPrimaryKey();
    }
    
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
    	return $this->auth_key;
    }
    
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
    	return $this->getAuthKey() === $authKey;
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
    	return Yii::$app->security->validatePassword($password, $this->password);
    }
    
    
    public static function roles(){
    	 
    	$roles=Yii::$app->authManager->getRoles();
    	$result=[];
    
    	foreach ($roles as $key=>$value)
    	{
    		array_push($result, $key);
    		 
    	}
    	 
    }
    
    public function getRoles(){
    	 
    	$roles=Yii::$app->authManager->getRolesByUser($this->id);
    	 
    	$result=[];
    	 
    	foreach ($roles as $key=>$value)
    	{
    		array_push($result, $key);
    		 
    	}
    	 
    	return $result;
    
    	 
    }
    
    public function getPermissions(){
    	 
    	$permissions=Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->id);
    	$result=[];
    	 
    	foreach ($permissions as $key=>$value)
    	{
    		array_push($result, $key);
    
    	}
    
    	return $permissions;
    
    }
    
    public function fields(){
    	
    	   $fields=parent::fields();
    	   
    	   $fields["roles"]=function($model){
    	   	   
    	   	 return $model->roles;
    	   };
    	 return $fields;
    }
}
