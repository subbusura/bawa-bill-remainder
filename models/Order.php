<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property string $id
 * @property string $name
 * @property string $mobile_number
 * @property string $advance_payment
 * @property string $image_path
 * @property int $user_id
 * @property int $order_status
 * @property int $created_at
 * @property int $updated_at
 */
class Order extends \yii\db\ActiveRecord
{
	
	const STATUS_PENDING = 10;
	const STATUS_PROCESSING=20;
	const STATUS_DELEVERED=25;
	const STATUS_COMPLETED = 15;
	
	public $src;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mobile_number'], 'required'],
            [['advance_payment'], 'number'],
            [['image_path','src','remarks'], 'string'],
            [['user_id', 'order_status', 'created_at', 'updated_at','salesman_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        	[['order_status'],'default','value'=>self::STATUS_PENDING],
            [['mobile_number'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'mobile_number' => 'Mobile Number',
            'advance_payment' => 'Advance Payment',
            'image_path' => 'Image Path',
            'user_id' => 'User ID',
            'order_status' => 'Order Status',
        	'remarks' => 'remarks',
        	'salesman_id'=>'salesman_id',
        	'image_one' => 'image_one',
        	'image_two' => 'image_two',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function getUser(){
    	
    	 return $this->hasOne(AdminUser::className(), ["id"=>"user_id"]);
    	
    }
    
    public function getSalesMan(){
    	 
    	return $this->hasOne(Salesman::className(), ["id"=>"salesman_id"]);
    	 
    }
    
    public function getItems(){
    	
    	 
    	 return $this->hasMany(OrdersItem::className(), ["order_id"=>"id"]);
    	
    }
    

    
    public function afterFind(){
    	
    	

    	   $update_at=new \DateTime();
    	   $update_at->setTimestamp($this->updated_at);
    	
    	   $this->updated_at=$update_at->format("d-m-Y");
    	   
    }
    
    public function extraFields(){
    	
    	 return ['user','items','salesMan'];
    	
    }
    
    public function fields(){
    	
    	
    		$fields=parent::fields();
    		
    		$fields["formatted_date"]=function($model){
    			
    			$created_at=new \DateTime();
    			$created_at->setTimestamp($this->created_at);
    			
    			return $created_at->format("d-m-Y");
    			
    		};
    		
    		return $fields;
    	
    }
    
    
    
}
