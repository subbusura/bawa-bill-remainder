<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Query;

/**
 * This is the model class for table "orders_items".
 *
 * @property string $id
 * @property string $order_id
 * @property string $product_code
 * @property string $product_name
 * @property string $supplier_code
 * @property string $supplier_name
 * @property int $qty
 * @property int $arrival_status
 * @property int $arrival_datetime
 */
class OrdersItemSearch extends OrdersItem
{
	
	const STATUS_PENDING = 10;
	const STATUS_COMPLETED = 15;
	
	public $order_ids;
	public $from;
	public $to;	
	public $product_codes;

	
    public function rules()
    {
        return [
            [['order_id', 'qty', 'arrival_status', 'arrival_datetime'], 'integer'],
        	['arrival_status','default','value'=>self::STATUS_PENDING],
        	[['order_ids'], 'each','rule' => ['integer']],
        	[['product_codes'], 'each','rule' => ['string']],
        	[['product_code'], 'each','rule' => ['integer']],
        	[['supplier_code'], 'each','rule' => ['integer']],
            [['product_code','product_name', 'supplier_code', 'supplier_name'], 'string', 'max' => 100],
        ];
    }
    
    
    public function search($params){
    	 
    	$query= OrdersItem::find();
    
    	$dataProvider=new ActiveDataProvider([
    			"query"=>$query
    	]);
    
    	$this->load($params,"");
    	 
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
    			'arrival_status'=>$this->arrival_status,
    			'mobile_number'=>$this->mobile_number,
    	]);
    	 
    	$query->andFilterWhere(['order_id'=>$this->order_ids]);
    	$query->andFilterWhere(['like', 'name', $this->name]);
    	 
    	return $dataProvider;
    
    }
    
    
    public function searchArrival($params){
    	
             $this->loadDefaultValues();
    		
    		$query1 = new Query();
    		$query1->select(["orders_items.product_code","orders_items.supplier_code","orders_items.supplier_name","orders_items.product_name","arrival_status","SUM(qty) as required_qty"])->from("orders_items")->leftJoin("products","products.product_code=orders_items.product_code")->leftJoin("suppliers","suppliers.supplier_code=orders_items.supplier_code")->andWhere("orders_items.arrival_status=20")->groupBy(["product_code"]);
    		
    		
    		$query1->andFilterWhere([
    				"orders_items.product_code"=>$this->product_code,
    				"orders_items.supplier_name"=>$this->supplier_name
    		]);
    		
      		$dataProvider=new SqlDataProvider([
    				"sql"=>$query1->createCommand()->sql,
    				"pagination"=>false
    		]);
    		
    		
    		$this->load($params,"");
    		
    		if (!$this->validate()) {
    			// uncomment the following line if you do not want to return any records when validation fails
    			// $query->where('0=1');
    			return $dataProvider;
    		}
    		
    		$filter=$dataProvider->getModels();
    		
    	    
    		
    		
    	
    		return $dataProvider;
    }
    
    
    
}
