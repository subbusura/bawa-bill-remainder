<?php

namespace app\models;

use yii;
use yii\base\Model;

class RoleForm extends Model{
	
	   public $role;
	   public $permission;
	
	   public function rules(){
	   	
	   	
	   		return [
	   				[['role'],'required'],
	   				['role','cnique'],
	   				['permission', 'each', 'rule' => ['string']],
	   		];
	   		
	   }
	   
	   public function cnique($attribute){
	   	
	   		 	$auth=Yii::$app->authManager;
	   		 	$roles=$auth->getRole($this->role);
	   		 	
	   		 	if(!is_null($roles))
	   		 	{
	   		 		$this->addError($attribute,"Role {$this->role} has already been taken.");
	   		 	}
	   	
	   }
	
}