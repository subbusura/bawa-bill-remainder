<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_arrived".
 *
 * @property string $id
 * @property string $order_id
 * @property string $line_items
 */
class OrderArrive extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_arrivel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'line_items'], 'required'],
            [['order_id'], 'integer'],
            [['line_items'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'line_items' => 'Line Items',
        ];
    }
}
