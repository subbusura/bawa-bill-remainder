<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "orders".
 *
 * @property string $id
 * @property string $name
 * @property string $mobile_number
 * @property string $advance_payment
 * @property string $image_path
 * @property int $user_id
 * @property int $order_status
 * @property int $created_at
 * @property int $updated_at
 */
class OrderSearch extends Order
{
	
	const STATUS_PENDING = 10;
	const STATUS_COMPLETED = 15;
	
  
    public $order_ids;
    public $from;
    public $to;
    public $name;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        		
            [['advance_payment'], 'number'],
        	[['order_ids'], 'each','rule' => ['integer']],
            [['image_path','src','name'], 'string'],
            [['user_id', 'order_status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        	//[['order_status'],'default','value'=>self::STATUS_PENDING],
            [['mobile_number'], 'string', 'max' => 15],
        	[['from', 'to'], 'default', 'value' => function ($model, $attribute) {
        			
        		   $date=new \DateTime();
        		   $date->modify('today');
        		   
        		   if($attribute==='from')
        		   {
        		   	return $date->format("Y-m-d");
        		   	
        		   }else{
        		   	
        		   	 $date->modify('tomorrow');
        		   	 $date->modify('-1 minute');
        		   	 return $date->format("Y-m-d");
        		   	
        		   }
        	}],
        ];
    }

    
    
    public function search($params){
    	
    	 $query= Order::find();
    	 
    	 $dataProvider=new ActiveDataProvider([
    	 		"query"=>$query,
    	 		'pagination' => [
    	 				'pageSizeLimit' => [0, 50],
    	 		],
    	 		'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
    	 ]);
    	 
    	$this->load($params,"");
    	
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		Yii::trace("Order Search Validation Error");
    		return $dataProvider;
    	}
    	
    	
    	
    	$begin = new \DateTime($this->from);
    	$end = new \DateTime($this->to);
    	$end = $end->modify('tomorrow');
    	$end = $end->modify('-1 minute');
    	
    	$query->orFilterWhere([
    			'order_status'=>$this->order_status,
    	]);
    	
    	
    	$searchname=$this->name==""?"":$this->name;
    	
    	$query->orFilterWhere(['id'=>$this->name])->orFilterWhere(['like', 'name', $searchname])->orFilterWhere(['like', 'mobile_number', $searchname])->andFilterWhere(["between",'created_at',$begin->getTimestamp(),$end->getTimestamp()]);
    	
    	
        return $dataProvider;
    	 
    }
    
    
    
}
