<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "suppliers".
 *
 * @property string $id
 * @property string $supplier_code
 * @property string $supplier_name
 */
class SupplierSearch extends Supplier
{
    
    public function rules()
    {
        return [
            [['supplier_code', 'supplier_name'], 'string', 'max' => 255],
            [['supplier_code'], 'unique'],
        ];
    }

    

    public function search($params){
    	 
    	$query = Supplier::find();
    
    
    	
    	$dataProvider=new ActiveDataProvider([
    			'query'=>$query,
    			'pagination' => [
    					'pageSizeLimit' => [0, 50],
    			],
    	]);
    
    
    	$this->load($params,"");
    	 
    	if(!$this->validate())
    	{
    
    		return $dataProvider;
    	}
    	
    	$query->andFilterWhere([
    			"supplier_code"=>$this->supplier_code,
    	]);
    	 
    	$query->andFilterWhere(["like","supplier_name",$this->supplier_name]);
    	 
    	return $dataProvider;
    }
    
}
