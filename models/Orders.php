<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property string $id
 * @property string $name
 * @property string $mobile_number
 * @property string $advance_payment
 * @property string $image_path
 * @property int $user_id
 * @property int $salesman_id
 * @property int $order_status
 * @property string $remarks
 * @property string $image_one
 * @property string $image_two
 * @property int $created_at
 * @property int $updated_at
 */
class Orders extends \yii\db\ActiveRecord
{
	
	const STATUS_PENDING = 10;
	const STATUS_PROCESSING=20;
	const STATUS_COMPLETED = 15;
	
	public $src;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mobile_number'], 'required'],
            [['advance_payment'], 'number'],
        	[['image_path','src'], 'string'],
            [['image_path','src' ,'remarks', 'image_one', 'image_two'], 'string'],
            [['user_id', 'salesman_id', 'order_status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        	[['order_status'],'default','value'=>self::STATUS_PENDING],
            [['mobile_number'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'mobile_number' => 'Mobile Number',
            'advance_payment' => 'Advance Payment',
            'image_path' => 'Image Path',
            'user_id' => 'User ID',
            'salesman_id' => 'Salesman ID',
            'order_status' => 'Order Status',
            'remarks' => 'Remarks',
            'image_one' => 'Image One',
            'image_two' => 'Image Two',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    
    public function getUser(){
    	 
    	return $this->hasOne(Salesman::className(), ["id"=>"salesman_id"]);
    	 
    }
    
    public function getItems(){
    	 
             
    	return $this->hasMany(OrdersItem::className(), ["order_id"=>"id"]);
    	 
    }
    
    
    public function afterFind(){
    	 
    	 
    	$created_at=new \DateTime();
    	$created_at->setTimestamp($this->created_at);
    
    	$this->created_at=$created_at->format("d-m-Y");
    
    	$update_at=new \DateTime();
    	$update_at->setTimestamp($this->updated_at);
    	 
    	$this->updated_at=$update_at->format("d-m-Y");
    
    }
    
    public function extraFields(){
    	 
    	return ['user','items'];
    	 
    }
    
    
}
