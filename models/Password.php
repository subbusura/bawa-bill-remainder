<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Password extends Model
{
	
	public $user_id;
	public $current_password;
	public $new_password;
	
	private $_user;
	
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
				[['current_password','new_password'], 'required'],
				[['current_password','new_password'], 'string', 'max' => 255],
		];
	}
	
	

}
