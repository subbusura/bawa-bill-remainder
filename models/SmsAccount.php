<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_account".
 *
 * @property string $id
 * @property string $name
 * @property string $url
 * @property string $sender_id
 * @property string $username
 * @property string $password
 * @property string $sms_type
 * @property int $is_default
 * @property int $created_at
 * @property int $updated_at
 */
class SmsAccount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['url', 'username'], 'string'],
        	['is_default','default','value'=>0],
            [['sender_id', 'is_default', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['password', 'sms_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'sender_id' => 'Sender ID',
            'username' => 'Username',
            'password' => 'Password',
            'sms_type' => 'Sms Type',
            'is_default' => 'Is Default',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
